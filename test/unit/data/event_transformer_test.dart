import 'package:flutter_test/flutter_test.dart';
import 'package:mim_mobile/features/events/data/datasources/transformers/event.transformer.dart';

import 'factories/event_dto_factory.dart';
import 'mocks/mock_params_dao.dart';

void main() {
  test('EventTransformer_shouldNotReturnAnNASpeaker', () async {
    final eventsAndSpeakers =
        EventTransformer((await getMockParamsDao()).getParamsSync().eventDate).eventsAndSpeakersFromEventDto(eventsDtoForTest);
    expect(eventsAndSpeakers.speakers, isNot(anyElement(HasName('#'))));
    expect(eventsAndSpeakers.speakers, isNot(anyElement(HasName('#N/A'))));
  });
}

class HasName extends CustomMatcher {
  HasName(Object? valueOrMatcher) : super('', '', valueOrMatcher);

  @override
  Object featureValueOf(dynamic actual) {
    return actual.name;
  }
}
