import 'package:mim_mobile/core/data/datasources/local/database_accessor.dart';
import 'package:mim_mobile/core/data/datasources/local/floor_app_database_dev.dart';
import 'package:mim_mobile/features/events/data/datasources/local/event.dbo.dart';
import 'package:mim_mobile/features/events/data/datasources/local/speaker.dbo.dart';
import 'package:test/test.dart';

import 'factories/database_factory.dart';

void main() {
  late DatabaseAccessor databaseAccessor;
  setUp(() async {
    databaseAccessor = await DatabaseAccessor.create(FloorAppDatabase());
    await prepareDatabase(databaseAccessor);
  });
  test('DatabaseAccessor_ShouldProvideInfoAboutAFlutterTalk', () async {
    final List<EventDbo> eventsDbo = await databaseAccessor.getEventsWithSpeakers().first;
    final flutterEvent = eventsDbo.firstWhere((element) => element.label == 'Où est Charlie : Comment j\'ai vaincu mon fils (ou pas...) grace à mon iPhone.');
    expect(flutterEvent, isNotNull);
    expect(flutterEvent.openFeedbackId, equals("FrWkVWQ3TnZeK4z1HOoc"));
    expect(flutterEvent.speakers[0].name, 'Vincent Blanchet');
  });
  test('DatabaseAccessor_ShouldReturnASingleEventWithSpeakers', () async {
    final EventDbo? eventDbo = await databaseAccessor.getEventWithSpeakersById('2hKL7CtGfDtHJynDKkWf');
    expect(eventDbo, isNotNull);
    expect(eventDbo?.label, 'Où est Charlie : Comment j\'ai vaincu mon fils (ou pas...) grace à mon iPhone.');
    expect(eventDbo?.speakers[0].name, 'Vincent Blanchet');
    expect(eventDbo?.isFavorite, true);
  });
  test('DatabaseAccessor_ShouldSetAnEventFavorite', () async {
    EventDbo? eventDbo = await databaseAccessor.getEventWithSpeakersById('2hKL7CtGfDtHJynDKkWf');
    expect(eventDbo, isNotNull);
    eventDbo!.isFavorite = true;
    await databaseAccessor.updateEvent(eventDbo);
    eventDbo = await databaseAccessor.getEventWithSpeakersById('2hKL7CtGfDtHJynDKkWf');
    expect(eventDbo?.isFavorite, true);
  });
  test('DatabaseAccessor_ShouldReturnASingleSpeakerWithEvents', () async {
    final SpeakerDbo? speakerDbo = await databaseAccessor.getSpeakerWithEventsById(0);
    expect(speakerDbo, isNotNull);
    expect(speakerDbo?.name, 'Vincent Blanchet');
    expect(speakerDbo?.events, isNotNull);
    expect(speakerDbo?.events, isNotEmpty);
    expect(speakerDbo?.events.length, 1);
    expect(speakerDbo?.events[0].label, 'Où est Charlie : Comment j\'ai vaincu mon fils (ou pas...) grace à mon iPhone.');
  });
}
