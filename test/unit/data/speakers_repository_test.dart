import 'package:mim_mobile/core/data/datasources/local/database_accessor.dart';
import 'package:mim_mobile/core/data/datasources/local/floor_app_database_dev.dart';
import 'package:mim_mobile/features/events/data/datasources/local/speaker.dbo.dart';
import 'package:mim_mobile/features/speakers/data/repositories/speakers.repository.dart';
import 'package:mim_mobile/features/speakers/domain/repositories/ispeakers.repository.dart';
import 'package:test/test.dart';

import 'factories/database_factory.dart';

void main() {
  late ISpeakersRepository repository;
  setUp(() async {
    final databaseAccessor = await DatabaseAccessor.create(FloorAppDatabase());
    await prepareDatabase(databaseAccessor);
    repository = SpeakersRepository(databaseAccessor);
  });
  test('speakersRepository_ShouldInstantiate', () {
    expect(repository, isNotNull);
  });
  test('speakersRepository_ShouldGetSpeakers', () async {
    final List<SpeakerDbo> speakers = await repository.getSpeakers();
    expect(speakers, isNotNull);
    expect(speakers, isNotEmpty);
  });
  test('speakersRepository_ShouldGetSpeakerWithEventsById', () async {
    final SpeakerDbo? speaker = await repository.getSpeakerWithEventsById(0);
    expect(speaker, isNotNull);
    expect(speaker?.name, 'Vincent Blanchet');
    expect(speaker?.events, isNotNull);
    expect(speaker?.events, isNotEmpty);
    expect(speaker?.events.length, 1);
    expect(speaker?.events[0].label, 'Où est Charlie : Comment j\'ai vaincu mon fils (ou pas...) grace à mon iPhone.');
  });
}
