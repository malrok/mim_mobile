import 'package:flutter_test/flutter_test.dart';
import 'package:mim_mobile/config/di/dependency_injection.dart';
import 'package:mim_mobile/features/events/data/datasources/local/event.dbo.dart';
import 'package:mim_mobile/features/events/domain/repositories/ievents.repository.dart';
import 'package:mim_mobile/features/params/data/datasources/local/base_params_dao.dart';
import 'package:mim_mobile/features/params/data/datasources/local/params.dbo.dart';
import 'package:mocktail/mocktail.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'mocks/mock_events_repository.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  SharedPreferences.setMockInitialValues({});
  configureDependencies('dev');
  late IEventsRepository repository;
  setUp(() async {
    final mockParamsDao = await getIt.getAsync<BaseParamsDao>();

    when(() => mockParamsDao.getParamsSync()).thenReturn(
      ParamsDbo(
        contactEmail: 'contact@mobilis-in-mobile.io',
        locationAddress1: '41 Boulevard de la Prairie au Duc',
        locationAddress2: '44200 Nantes, FRANCE',
        locationLatitude: 47.20454345368479,
        locationLongitude: -1.560808920249304,
        locationName: 'Mediacampus',
        eventsFileName: 'slots2024.json',
        sponsorsFileName: 'sponsors2024.json',
        openFeedbackUrl: 'https://openfeedback.io/RQSg6MHywwQzjkYgFFoW/2023-06-20/',
        ticketShopUrl: 'https://www.billetweb.fr/mobilis-in-mobile-2024',
        eventDate: DateTime(2024, 6, 18),
      ),
    );

    repository = await getMockIEventsRepository();
  });
  test('EventsRepository_ShouldInstantiate', () {
    expect(repository, isNotNull);
  });
  test('EventsRepository_ShouldGetEvents', () async {
    final List<EventDbo> events = await repository.getEvents().first;
    expect(events, isNotNull);
    expect(events, isNotEmpty);
  });
  test('EventsRepository_ShouldGetEventById', () async {
    final EventDbo? event = await repository.getEventById('2hKL7CtGfDtHJynDKkWf');
    expect(event, isNotNull);
    expect(event?.label, 'Où est Charlie : Comment j\'ai vaincu mon fils (ou pas...) grace à mon iPhone.');
    expect(event?.speakers[0].name, 'Vincent Blanchet');
  });
  test('EventsRepository_ShouldUpdateEventFavorite', () async {
    EventDbo? event = await repository.getEventById('tfU5P1WpCN10JSQVcs8X');
    expect(event, isNotNull);
    expect(event!.isFavorite, false);
    event.isFavorite = true;
    await repository.updateEvent(event);
    event = await repository.getEventById('tfU5P1WpCN10JSQVcs8X');
    expect(event!.isFavorite, true);
  });
  test('EventsRepository_ShouldUpdateAllEventsAndKeepFavorites', () async {
    /// check that favorite is set and true on an event
    List<EventDbo> events = await repository.getEvents().first;
    expect(events, isNotNull);
    var event = events.firstWhere((element) => element.id == '2hKL7CtGfDtHJynDKkWf');
    expect(event.isFavorite, true);

    /// update the database
    events = await repository.getEvents().first;

    /// check that favorite is still false on an event
    event = events.firstWhere((element) => element.id == 'Kxm7nza0bmlC5SMuL0r3');
    expect(event.isFavorite, false);

    /// check that favorite is still set and true on an event
    event = events.firstWhere((element) => element.id == '2hKL7CtGfDtHJynDKkWf');
    expect(event.isFavorite, true);
  });
}
