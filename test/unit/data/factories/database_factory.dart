import 'package:mim_mobile/core/data/datasources/local/database_accessor.dart';
import 'package:mim_mobile/features/events/data/datasources/remote/event.dto.dart';
import 'package:mim_mobile/features/events/data/datasources/transformers/event.transformer.dart';

import '../mocks/mock_params_dao.dart';
import 'event_dto_factory.dart';

Future<void> prepareDatabase(DatabaseAccessor databaseAccessor) async {
  await _dropData(databaseAccessor);
  await _insertData(databaseAccessor);
}

Future<void> _dropData(DatabaseAccessor databaseAccessor) => databaseAccessor.dropData();

Future<void> _insertData(DatabaseAccessor databaseAccessor) async {
  final eventsDto = mockEventsDto.map((e) => EventDto.fromJson(e)).toList();
  final eventsAndSpeakers =
      EventTransformer((await getMockParamsDao()).getParamsSync().eventDate).eventsAndSpeakersFromEventDto(eventsDto);
  eventsAndSpeakers.events.firstWhere((element) => element.id == '2hKL7CtGfDtHJynDKkWf').isFavorite = true;
  await databaseAccessor.insertEventsAndSpeakers(eventsAndSpeakers);
}
