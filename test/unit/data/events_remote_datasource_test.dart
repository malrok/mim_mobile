import 'package:mim_mobile/features/events/data/datasources/remote/event.dto.dart';
import 'package:mim_mobile/features/events/data/datasources/remote/events.remote_datasource.dart';
import 'package:test/test.dart';

import 'mocks/mock_events_client.dart';

void main() {
  final mockEventsClient = getMockEventsClient();

  test('EventsRemoteDatasource_ShouldComplete', () async {
    await EventsRemoteDatasource(mockEventsClient).query();
  });
  test('EventsRemoteDatasource_ShouldReturnANotEmptyDto', () async {
    List<EventDto> dto = await EventsRemoteDatasource(mockEventsClient).query();
    expect(dto, isNotNull);
    expect(dto, isNotEmpty);
    expect(dto[0].title, 'Keynote');
    expect(dto[0].openFeedbackId, isEmpty);
  });
}
