import 'package:mim_mobile/features/events/data/datasources/remote/base_events_client.dart';
import 'package:mim_mobile/features/events/data/datasources/remote/events.client.dev.dart';
import 'package:mocktail/mocktail.dart';

import '../factories/event_dto_factory.dart';

BaseEventsClient getMockEventsClient() {
  final mockEventsClient = MockEventsClient();

  when(() => mockEventsClient.getEvents()).thenAnswer((_) => Future.value(eventsDtoForTest));
  return mockEventsClient;
}
