import 'package:mim_mobile/features/params/data/datasources/local/base_params_dao.dart';
import 'package:mim_mobile/features/params/data/datasources/local/params.dao.dev.dart';
import 'package:mim_mobile/features/params/data/datasources/local/params.dbo.dart';
import 'package:mocktail/mocktail.dart';

Future<BaseParamsDao> getMockParamsDao() async {
  final mockParamsDao = await MockParamsDao.instantiate();

  when(() => mockParamsDao.getParamsSync()).thenAnswer(
    (_) => ParamsDbo(
      contactEmail: 'contact@mobilis-in-mobile.io',
      locationAddress1: '41 Boulevard de la Prairie au Duc',
      locationAddress2: '44200 Nantes, FRANCE',
      locationLatitude: 47.20454345368479,
      locationLongitude: -1.560808920249304,
      locationName: 'Mediacampus',
      eventsFileName: 'slots2024.json',
      sponsorsFileName: 'sponsors2024.json',
      openFeedbackUrl: 'https://openfeedback.io/RQSg6MHywwQzjkYgFFoW/2023-06-20/',
      ticketShopUrl: 'https://www.billetweb.fr/mobilis-in-mobile-2024',
      eventDate: DateTime(2024, 6, 18),
    ),
  );
  return mockParamsDao;
}
