import 'package:mim_mobile/core/data/datasources/local/database_accessor.dart';
import 'package:mim_mobile/core/data/datasources/local/floor_app_database_dev.dart';
import 'package:mim_mobile/features/events/data/datasources/remote/events.remote_datasource.dart';
import 'package:mim_mobile/features/events/data/repositories/events.repository.dart';
import 'package:mim_mobile/features/events/domain/repositories/ievents.repository.dart';

import '../factories/database_factory.dart';
import 'mock_events_client.dart';

Future<IEventsRepository> getMockIEventsRepository() async {
  final databaseAccessor = await DatabaseAccessor.create(FloorAppDatabase());
  await prepareDatabase(databaseAccessor);
  final mockEventsClient = getMockEventsClient();
  return EventsRepository.instantiate(databaseAccessor, EventsRemoteDatasource(mockEventsClient));
}
