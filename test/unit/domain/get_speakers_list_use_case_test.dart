import 'package:mim_mobile/core/data/datasources/local/database_accessor.dart';
import 'package:mim_mobile/core/data/datasources/local/floor_app_database_dev.dart';
import 'package:mim_mobile/features/speakers/data/repositories/speakers.repository.dart';
import 'package:mim_mobile/features/speakers/domain/repositories/ispeakers.repository.dart';
import 'package:mim_mobile/features/speakers/domain/use_cases/get_speakers_list.use_case.dart';
import 'package:test/test.dart';

import '../data/factories/database_factory.dart';

void main() {
  late ISpeakersRepository repository;
  setUp(() async {
    final databaseAccessor = await DatabaseAccessor.create(FloorAppDatabase());
    await prepareDatabase(databaseAccessor);
    repository = SpeakersRepository(databaseAccessor);
  });
  test('speakersListUseCase_ShouldGetSpeakers', () async {
    final useCase = SpeakersListUseCase(repository);
    final res = await useCase.execute();
    expect(res, isNotNull);
    res.fold((l) => expect(l, isNull), (r) => expect(r, isNotEmpty));
  });
}
