import 'package:flutter_test/flutter_test.dart';
import 'package:mim_mobile/config/di/dependency_injection.dart';
import 'package:mim_mobile/features/params/data/datasources/local/base_params_dao.dart';
import 'package:mim_mobile/features/params/data/datasources/local/params.dbo.dart';
import 'package:mim_mobile/features/params/data/datasources/remote/params.client.dev.dart';
import 'package:mim_mobile/features/params/data/datasources/remote/params.dto.dart';
import 'package:mim_mobile/features/params/data/datasources/remote/params.remote_datasource.dart';
import 'package:mim_mobile/features/params/data/repositories/params.repository.dart';
import 'package:mim_mobile/features/params/domain/repository/iparams.repository.dart';
import 'package:mim_mobile/features/params/domain/use_cases/check_event_date.use_case.dart';
import 'package:mocktail/mocktail.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  SharedPreferences.setMockInitialValues({});
  configureDependencies('dev');

  late IParamsRepository repository;
  late BaseParamsDao mockParamsDao;

  ParamsDbo getDbo(DateTime date) => ParamsDbo(
        contactEmail: 'contact@mobilis-in-mobile.io',
        locationAddress1: '41 Boulevard de la Prairie au Duc',
        locationAddress2: '44200 Nantes, FRANCE',
        locationLatitude: 47.20454345368479,
        locationLongitude: -1.560808920249304,
        locationName: 'Mediacampus',
        eventsFileName: 'slots2024.json',
        sponsorsFileName: 'sponsors2024.json',
        openFeedbackUrl: 'https://openfeedback.io/RQSg6MHywwQzjkYgFFoW/2023-06-20/',
        ticketShopUrl: 'https://www.billetweb.fr/mobilis-in-mobile-2024',
        eventDate: date,
      );

  setUp(() async {
    mockParamsDao = await getIt.getAsync<BaseParamsDao>();

    final mockParamsClient = MockParamsClient();

    when(() => mockParamsClient.getParams()).thenAnswer(
      (_) async => ParamsDto(
        contactEmail: 'contact@mobilis-in-mobile.io',
        locationAddress1: '41 Boulevard de la Prairie au Duc',
        locationAddress2: '44200 Nantes, FRANCE',
        locationLatitude: 47.20454345368479,
        locationLongitude: -1.560808920249304,
        locationName: 'Mediacampus',
        eventsFileName: 'slots2024.json',
        sponsorsFileName: 'sponsors2024.json',
        openFeedbackUrl: 'https://openfeedback.io/RQSg6MHywwQzjkYgFFoW/2023-06-20/',
        ticketShopUrl: 'https://www.billetweb.fr/mobilis-in-mobile-2024',
        eventDate: DateTime(2024, 6, 18).toIso8601String(),
      ),
    );

    repository = ParamsRepository(ParamsRemoteDatasource(mockParamsClient), mockParamsDao);
  });

  test('GetEventByIdUseCase_ShouldReturnFalseOnFarPassedEvent', () async {
    when(() => mockParamsDao.getParams()).thenAnswer(
          (_) => Stream.value(getDbo(DateTime.now().subtract(Duration(days: 365)))),
    );

    final useCase = CheckEventDateUseCase(repository);
    final res = await useCase.execute().first;
    expect(res, isNotNull);
    res.fold((l) {
      expect(l, isNull);
    }, (r) {
      expect(r, isNotNull);
      expect(r, equals(false));
    });
  });

  test('GetEventByIdUseCase_ShouldReturnFalseOnPassedEvent', () async {
    when(() => mockParamsDao.getParams()).thenAnswer(
      (_) => Stream.value(getDbo(DateTime.now().subtract(Duration(days: 15)))),
    );

    final useCase = CheckEventDateUseCase(repository);
    final res = await useCase.execute().first;
    expect(res, isNotNull);
    res.fold((l) {
      expect(l, isNull);
    }, (r) {
      expect(r, isNotNull);
      expect(r, equals(false));
    });
  });

  test('GetEventByIdUseCase_ShouldReturnTrueOnNearEvent', () async {
    when(() => mockParamsDao.getParams()).thenAnswer(
      (_) => Stream.value(getDbo(DateTime.now().add(Duration(days: 13)))),
    );

    final useCase = CheckEventDateUseCase(repository);
    final res = await useCase.execute().first;
    expect(res, isNotNull);
    res.fold((l) {
      expect(l, isNull);
    }, (r) {
      expect(r, isNotNull);
      expect(r, equals(true));
    });
  });

  test('GetEventByIdUseCase_ShouldReturnTrueOnFarEvent', () async {
    when(() => mockParamsDao.getParams()).thenAnswer(
          (_) => Stream.value(getDbo(DateTime.now().add(Duration(days: 365)))),
    );

    final useCase = CheckEventDateUseCase(repository);
    final res = await useCase.execute().first;
    expect(res, isNotNull);
    res.fold((l) {
      expect(l, isNull);
    }, (r) {
      expect(r, isNotNull);
      expect(r, equals(true));
    });
  });
}
