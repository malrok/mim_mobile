import 'package:mim_mobile/features/events/domain/entities/event.entity.dart';
import 'package:mim_mobile/features/events/domain/entities/formatted_text.entity.dart';
import 'package:mim_mobile/features/events/domain/entities/speaker.entity.dart';
import 'package:test/test.dart';

import 'factories/event_factory.dart';
import 'factories/formatted_text_factory.dart';
import 'factories/speaker_factory.dart';

void main() {
  late Event event;
  late Speaker speaker;
  late FormattedText formattedText;
  setUp(() {
    event = eventForTests;
    speaker = speakerForTests;
    formattedText = formattedTextForTests;
  });
  group('Testing Events', () {
    test('Events_ShouldInstantiate', () {
      expect(event, isNotNull);
    });
    test('Events_ShouldHaveNonNullRequiredAttributes', () {
      expect(event.label, isNotNull);
      expect(event.startDate, isNotNull);
      expect(event.endDate, isNotNull);
      expect(event.track, isNotNull);
    });
    test('Events_ShouldHaveADurationOf60Minutes', () {
      expect(event.duration.inMinutes, equals(60));
    });
    test('Events_ShouldHaveNonNullSpeakerAttribute', () {
      expect(event.speakers, isNotNull);
    });
    test('Events_ShouldHaveAFormattedTextDescription', () {
      expect(event.description, isA<FormattedText>());
    });
  });
  group('Testing Speakers', () {
    test('Speaker_ShouldInstantiate', () {
      expect(speaker, isNotNull);
    });
    test('Speaker_ShouldHaveNonNullRequiredAttributes', () {
      expect(speaker.name, isNotNull);
    });
    test('Speaker_ShouldHaveAFormattedTextBio', () {
      expect(speaker.bio, isA<FormattedText>());
    });
  });
  group('Testing Formatted Text', () {
    test('FormattedText_ShouldInstantiate', () {
      expect(formattedText, isNotNull);
    });
    test('FormattedText_ShouldHaveAContent', () {
      expect(formattedText.content, isNotNull);
    });
    test('FormattedText_ShouldHaveAType', () {
      expect(formattedText.type, isNotNull);
    });
  });
  group('Testing Formatted Text', () {
    test('FormattedText_ShouldInstantiate', () {
      expect(formattedText, isNotNull);
    });
    test('FormattedText_ShouldHaveAContent', () {
      expect(formattedText.content, isNotNull);
    });
    test('FormattedText_ShouldHaveAType', () {
      expect(formattedText.type, isNotNull);
    });
  });
}
