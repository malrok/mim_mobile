import 'package:mim_mobile/features/events/domain/entities/formatted_text.entity.dart';
import 'package:mim_mobile/features/events/domain/transformers/formatted_text.transformer.dart';
import 'package:mim_mobile/features/events/domain/value_objects/formatted_text_type.value_object.dart';
import 'package:test/test.dart';

import '../data/factories/event_dto_factory.dart';

main() {
  late FormattedText formattedTextMd;
  late FormattedText formattedTextPlain;
  setUp(() {
    formattedTextMd = formattedTextFromDto(mockEventsDto[4]["abstract"]);
    formattedTextPlain = formattedTextFromDto(mockEventsDto[19]["abstract"]);
  });
  test('FormattedTextTransformer_ShouldReturnANonNullValue', () {
    expect(formattedTextMd, isNotNull);
    expect(formattedTextPlain, isNotNull);
  });
  test('FormattedTextTransformer_ShouldReturnAFormattedText', () {
    expect(formattedTextMd, isA<FormattedText>());
    expect(formattedTextPlain, isA<FormattedText>());
  });
  test('FormattedTextTransformer_ShouldReturnAFormattedTextWithCorrectType', () {
    expect(formattedTextMd.type, equals(FormattedTextType.markdown));
    expect(formattedTextPlain.type, equals(FormattedTextType.plain));
  });
}
