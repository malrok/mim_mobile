import 'package:mim_mobile/features/events/data/datasources/transformers/event.transformer.dart';
import 'package:test/test.dart';

import '../data/mocks/mock_params_dao.dart';

void main() async {
  String schedule = '9h10-9h40';
  EventTransformer transformer = EventTransformer((await getMockParamsDao()).getParamsSync().eventDate);
  test('EventTransformer_ShouldHaveProperStartDate', () {
    final startDate = transformer.startDateFromSchedule(schedule);
    expect(startDate, equals(DateTime(2024, 6, 18, 9, 10)));
  });
  test('EventTransformer_ShouldHaveProperEndDate', () {
    final endDate = transformer.endDateFromSchedule(schedule);
    expect(endDate, equals(DateTime(2024, 6, 18, 9, 40)));
  });
}
