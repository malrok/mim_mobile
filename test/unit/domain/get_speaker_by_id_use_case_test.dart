import 'package:mim_mobile/core/data/datasources/local/database_accessor.dart';
import 'package:mim_mobile/core/data/datasources/local/floor_app_database_dev.dart';
import 'package:mim_mobile/features/speakers/data/repositories/speakers.repository.dart';
import 'package:mim_mobile/features/speakers/domain/repositories/ispeakers.repository.dart';
import 'package:mim_mobile/features/speakers/domain/use_cases/get_speaker_by_id.use_case.dart';
import 'package:test/test.dart';

import '../data/factories/database_factory.dart';

void main() {
  late ISpeakersRepository repository;
  setUp(() async {
    final databaseAccessor = await DatabaseAccessor.create(FloorAppDatabase());
    await prepareDatabase(databaseAccessor);
    repository = SpeakersRepository(databaseAccessor);
  });
  test('speakerUseCase_ShouldGetSpeakerById', () async {
    final useCase = SpeakerUseCase(repository);
    final res = await useCase.execute(0);
    expect(res, isNotNull);
    res.fold((l) => expect(l, isNull), (r) {
      expect(r, isNotNull);
      expect(r?.name, 'Vincent Blanchet');
      expect(r?.events, isNotNull);
      expect(r?.events, isNotEmpty);
      expect(r?.events?.length, 1);
      expect(r?.events?[0].label, 'Où est Charlie : Comment j\'ai vaincu mon fils (ou pas...) grace à mon iPhone.');
    });
  });
}
