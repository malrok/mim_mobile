import 'package:mim_mobile/features/events/domain/entities/formatted_text.entity.dart';
import 'package:mim_mobile/features/events/domain/value_objects/formatted_text_type.value_object.dart';

const _content =
    "Dans ce talk, j'aimerais vous partager une solution pour répondre à la problématique suivante :\\n**Comment bien gérer le cycle de release d'une application mobile ?**\\n\\nÀ mesure des projets mobiles sur lesquels j'ai participé, je me suis aperçu qu'aucune équipe n'avait la même manière de distribuer leur application. Souvent la productivité s'en voit freiner à cause d'une opération git à faire *alamano*, d'un build ne ciblant pas le bon environnement ou encore de la difficulté à intégrer un nouveau variant de l'application dans la CI/CD.\\n\\nNous parlerons notamment de :\\n\\n- SemVer\\n- Trunk-based\\n- CI/CD\\n- Dev stores\\n- AppStore (+ TestFlight)\\n- PlayConsole (+ tracks), ...\\n\\n";

final formattedTextForTests = FormattedText(_content, FormattedTextType.markdown);
