import 'package:mim_mobile/features/events/domain/entities/event.entity.dart';
import 'package:mim_mobile/features/events/domain/entities/formatted_text.entity.dart';
import 'package:mim_mobile/features/events/domain/value_objects/formatted_text_type.value_object.dart';
import 'package:mim_mobile/features/events/domain/value_objects/languages.value_object.dart';
import 'package:mim_mobile/features/events/domain/value_objects/talk_categories.value_object.dart';
import 'package:mim_mobile/features/events/domain/value_objects/talk_type.value_object.dart';

import 'speaker_factory.dart';

final eventForTests = Event(
  id: '2hKL7CtGfDtHJynDKkWf',
  openFeedbackId: 'FrWkVWQ3TnZeK4z1HOoc',
  label: 'First event',
  startDate: DateTime.now(),
  endDate: DateTime.now().add(const Duration(hours: 1)),
  track: 'Room 1',
  speakers: [speakerForTests],
  description: FormattedText('', FormattedTextType.plain),
  category: TalkCategories.na,
  type: TalkType.talk,
  language: Languages.french,
  isFavorite: false,
);
