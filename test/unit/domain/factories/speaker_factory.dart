import 'package:mim_mobile/features/events/domain/entities/formatted_text.entity.dart';
import 'package:mim_mobile/features/events/domain/entities/speaker.entity.dart';
import 'package:mim_mobile/features/events/domain/value_objects/formatted_text_type.value_object.dart';

final speakerForTests = Speaker(
  id: -1,
  name: 'John Doe',
  bio: FormattedText('', FormattedTextType.plain),
  pictureUrl: '',
  company: '',
  twitterUrl: '',
  githubUrl: '',
);
