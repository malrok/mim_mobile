import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

void prepareSharedPrefsForTests() {
  WidgetsFlutterBinding.ensureInitialized();
  Map<String, Object> values = <String, Object>{'favorites': false};
  SharedPreferences.setMockInitialValues(values);
}