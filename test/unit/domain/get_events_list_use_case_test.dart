import 'package:flutter_test/flutter_test.dart';
import 'package:mim_mobile/config/di/dependency_injection.dart';
import 'package:mim_mobile/features/events/domain/repositories/ievents.repository.dart';
import 'package:mim_mobile/features/events/domain/use_cases/get_events_list.use_case.dart';
import 'package:mim_mobile/features/params/data/datasources/local/base_params_dao.dart';
import 'package:mim_mobile/features/params/data/datasources/local/params.dbo.dart';
import 'package:mocktail/mocktail.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../data/mocks/mock_events_repository.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  SharedPreferences.setMockInitialValues({});
  configureDependencies('dev');
  late IEventsRepository repository;
  setUp(() async {
    final mockParamsDao = await getIt.getAsync<BaseParamsDao>();

    when(() => mockParamsDao.getParamsSync()).thenReturn(
      ParamsDbo(
        contactEmail: 'contact@mobilis-in-mobile.io',
        locationAddress1: '41 Boulevard de la Prairie au Duc',
        locationAddress2: '44200 Nantes, FRANCE',
        locationLatitude: 47.20454345368479,
        locationLongitude: -1.560808920249304,
        locationName: 'Mediacampus',
        eventsFileName: 'slots2024.json',
        sponsorsFileName: 'sponsors2024.json',
        openFeedbackUrl: 'https://openfeedback.io/RQSg6MHywwQzjkYgFFoW/2023-06-20/',
        ticketShopUrl: 'https://www.billetweb.fr/mobilis-in-mobile-2024',
        eventDate: DateTime(2024, 6, 18),
      ),
    );
    repository = await getMockIEventsRepository();
  });
  test('GetEventsListUseCase_ShouldGetEvents', () async {
    final useCase = GetEventsListUseCase(repository);
    final res = await useCase.execute().first;
    expect(res, isNotNull);
    res.fold((l) => expect(l, isNull), (r) => expect(r, isNotEmpty));
  });
}
