import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mim_mobile/config/di/dependency_injection.dart';
import 'package:mim_mobile/core/data/datasources/local/database_accessor.dart';
import 'package:mim_mobile/core/data/datasources/local/floor_app_database_dev.dart';
import 'package:mim_mobile/features/events/presentation/blocs/favorite/favorite.cubit.dart';
import 'package:mim_mobile/features/events/presentation/blocs/favorite/favorite.states.dart';
import 'package:mim_mobile/features/params/data/datasources/local/base_params_dao.dart';
import 'package:mim_mobile/features/params/data/datasources/local/params.dbo.dart';
import 'package:mocktail/mocktail.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../data/factories/database_factory.dart';
import '../domain/factories/event_factory.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  SharedPreferences.setMockInitialValues({});
  configureDependencies('dev');
  setUp(() async {
    final mockParamsDao = await getIt.getAsync<BaseParamsDao>();

    when(() => mockParamsDao.getParamsSync()).thenReturn(
      ParamsDbo(
        contactEmail: 'contact@mobilis-in-mobile.io',
        locationAddress1: '41 Boulevard de la Prairie au Duc',
        locationAddress2: '44200 Nantes, FRANCE',
        locationLatitude: 47.20454345368479,
        locationLongitude: -1.560808920249304,
        locationName: 'Mediacampus',
        eventsFileName: 'slots2024.json',
        sponsorsFileName: 'sponsors2024.json',
        openFeedbackUrl: 'https://openfeedback.io/RQSg6MHywwQzjkYgFFoW/2023-06-20/',
        ticketShopUrl: 'https://www.billetweb.fr/mobilis-in-mobile-2024',
        eventDate: DateTime(2024, 6, 18),
      ),
    );

    final databaseAccessor = await DatabaseAccessor.create(FloorAppDatabase());
    await prepareDatabase(databaseAccessor);
  });
  blocTest<FavoriteCubit, FavoriteStates>(
    'FavoriteCubit_ShouldLoadData',
    build: () => FavoriteCubit(),
    act: (bloc) => bloc.load('2hKL7CtGfDtHJynDKkWf'),
    expect: () => [
      isA<FavoriteStatesLoading>(),
      isA<FavoriteStatesData>(),
    ],
  );
  blocTest<FavoriteCubit, FavoriteStates>(
    'FavoriteCubit_ShouldUpdateFavorite',
    build: () => FavoriteCubit(),
    act: (bloc) => bloc.updateFavorite(eventForTests),
    expect: () => [
      isA<FavoriteStatesLoading>(),
      isA<FavoriteStatesData>(),
    ],
  );
}
