import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:mim_mobile/config/di/dependency_injection.dart';
import 'package:mim_mobile/features/events/data/datasources/remote/base_events_client.dart';
import 'package:mim_mobile/mim_app.widget.dart';
import 'package:mocktail/mocktail.dart';

import '../test/unit/data/factories/event_dto_factory.dart';
import 'utils/take_screenshot.dart';

void main() async {
  late BaseEventsClient mockEventsClient;

  final IntegrationTestWidgetsFlutterBinding binding = IntegrationTestWidgetsFlutterBinding();
  configureDependencies('dev');

  setUp(() {
    mockEventsClient = getIt.get<BaseEventsClient>();
  });

  testWidgets('EventsPage_ShouldBehaveOverExceptions', (tester) async {
    when(() => mockEventsClient.getEvents()).thenThrow(Exception());

    await tester.pumpWidget(const MimApp());
    await tester.pumpAndSettle();
    await takeScreenshot(tester, binding, 'eventsPageWithException');
    expect(find.text("Réessayer"), findsOneWidget);
  });

  testWidgets('EventsPage_ShouldEventuallyLoadEvents', (tester) async {
    when(() => mockEventsClient.getEvents()).thenAnswer((_) => Future.value(eventsDtoForTest));

    await tester.pumpWidget(const MimApp());
    await tester.pumpAndSettle();
    await takeScreenshot(tester, binding, 'eventsPageLoaded');
    expect(find.text("Keynote d'ouverture"), findsOneWidget);
  });
}
