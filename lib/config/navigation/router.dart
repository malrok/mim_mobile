import 'package:go_router/go_router.dart';
import 'package:mim_mobile/config/di/dependency_injection.dart';
import 'package:mim_mobile/features/events/presentation/pages/event_detail/event_detail.page.dart';
import 'package:mim_mobile/features/events/presentation/pages/feedback/feedback.page.dart';
import 'package:mim_mobile/features/home/presentation/pages/home.widget.dart';
import 'package:mim_mobile/features/params/domain/use_cases/get_params.use_case.dart';
import 'package:mim_mobile/features/speakers/presentation/pages/speaker_detail/speaker_detail.page.dart';
import 'package:mim_mobile/features/sponsors/domain/use_cases/refresh_sponsors_list.use_case.dart';
import 'package:mim_mobile/features/sponsors/presentation/pages/sponsor_detail/sponsor_detail.page.dart';

bool started = false;

final appRouter = GoRouter(
  redirect: (context, state) async {
    if (!started) {
      started = true;
      final getParamsUseCase = await getIt.getAsync<GetParamsUseCase>();
      final result = await getParamsUseCase.execute().first;

      if (result.isRight()) {
        final refreshSponsorsListUseCase = await getIt.getAsync<RefreshSponsorsListUseCase>();
        await refreshSponsorsListUseCase.execute();
      }
    }

    return state.fullPath;
  },
  routes: [
    GoRoute(
      path: '/',
      builder: (context, state) => const HomePage(),
    ),
    GoRoute(
      path: '/event_detail',
      builder: (context, state) => EventDetailPage(id: state.extra as String),
    ),
    GoRoute(
      path: '/speaker_detail',
      builder: (context, state) => SpeakerDetailPage(id: state.extra as int?),
    ),
    GoRoute(
      path: '/sponsor_detail',
      builder: (context, state) => SponsorDetailPage(sponsorKey: state.extra as String),
    ),
    GoRoute(
      path: '/feedback',
      builder: (context, state) => FeedbackPage(openFeedbackUrl: state.extra as String),
    ),
  ],
);
