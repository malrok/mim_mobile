import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mim_mobile/config/constants/colors.constants.dart';

part 'text.constants.dart';

final mimTheme = ThemeData(
  textTheme: textTheme,
  primarySwatch: Colors.blue,
  splashColor: MimColors.primary32,
  scaffoldBackgroundColor: MimColors.background,
  visualDensity: VisualDensity.adaptivePlatformDensity,
  appBarTheme: AppBarTheme(
    backgroundColor: MimColors.background,
    foregroundColor: MimColors.primary,
    shadowColor: Colors.transparent,
    titleSpacing: 0.0,
    systemOverlayStyle: const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        systemStatusBarContrastEnforced: false,
        statusBarIconBrightness: Brightness.dark, // For Android (dark icons)
        statusBarBrightness: Brightness.light // For iOS (dark icons)
        ),
  ),
  tabBarTheme: TabBarTheme(
    indicator: BoxDecoration(
      color: MimColors.primary32,
      border: Border(
        top: BorderSide(color: MimColors.primary, width: 2),
      ),
    ),
    labelColor: MimColors.primary,
    labelStyle: textTheme.labelSmall,
    labelPadding: EdgeInsets.zero,
    unselectedLabelColor: MimColors.greyMedium,
    unselectedLabelStyle: textTheme.labelSmall,
  ),
  pageTransitionsTheme: PageTransitionsTheme(
    builders: Map<TargetPlatform, PageTransitionsBuilder>.fromIterable(
      TargetPlatform.values,
      value: (_) => const FadeForwardsPageTransitionsBuilder(),
    ),
  ),
);
