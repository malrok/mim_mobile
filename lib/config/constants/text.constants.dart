part of 'theme.constants.dart';

final _serif = GoogleFonts.bitter().fontFamily;
final _sansSerif = GoogleFonts.workSans().fontFamily;

final _black = MimColors.black;

// TODO: FontWeight still not working ?

final textTheme = TextTheme(
  /// Titre de page
  headlineLarge: TextStyle(
    fontSize: 26,
    letterSpacing: -0.4,
    fontWeight: FontWeight.w600,
    fontFamily: _serif,
    color: _black,
  ),

  /// Titre secondaire
  headlineMedium: TextStyle(
    fontSize: 16,
    letterSpacing: -0.2,
    fontWeight: FontWeight.w500,
    fontFamily: _serif,
    color: _black,
  ),

  /// Titre de card
  headlineSmall: TextStyle(
    fontSize: 14,
    letterSpacing: -0.2,
    fontWeight: FontWeight.w500,
    fontFamily: _serif,
    color: _black,
  ),

  /// Sous-titre
  titleMedium: TextStyle(
    fontSize: 14,
    letterSpacing: -0.4,
    fontWeight: FontWeight.w400,
    fontFamily: _sansSerif,
    color: MimColors.greyMedium,
  ),

  /// Caps
  titleSmall: TextStyle(
    fontSize: 12,
    letterSpacing: -0.4,
    fontWeight: FontWeight.w500,
    fontFamily: _sansSerif,
  ),

  /// Boutons, actions
  labelLarge: TextStyle(
    fontSize: 14,
    letterSpacing: -0.4,
    fontWeight: FontWeight.w600,
    fontFamily: _sansSerif,
  ),

  /// Tags
  labelMedium: TextStyle(
    fontSize: 14,
    letterSpacing: -0.4,
    fontWeight: FontWeight.w600,
    fontFamily: _sansSerif,
  ),

  /// NavBar
  labelSmall: TextStyle(
    fontSize: 11,
    letterSpacing: -0.4,
    fontWeight: FontWeight.w500,
    fontFamily: _sansSerif,
    color: MimColors.greyMedium,
  ),

  /// Text
  bodyLarge: TextStyle(
    fontSize: 16,
    letterSpacing: -0.4,
    fontWeight: FontWeight.w400,
    fontFamily: _sansSerif,
    color: _black,
  ),

  bodyMedium: TextStyle(
    fontSize: 14,
    letterSpacing: -0.4,
    fontWeight: FontWeight.w400,
    fontFamily: _sansSerif,
    color: _black,
  ),

  /// Hint, astuces,...
  bodySmall: TextStyle(
    fontSize: 14,
    letterSpacing: -0.4,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.italic,
    fontFamily: _sansSerif,
    color: MimColors.greyMedium,
  ),
);
