import 'package:flutter/material.dart';

abstract class MimIcons {
  static const calendar = AssetImage('assets/icons/3D/calendar.png');
  static const contact = AssetImage('assets/icons/3D/contact.png');
  static const location = AssetImage('assets/icons/3D/location.png');
  static const speakers = AssetImage('assets/icons/3D/speakers.png');
  static const googleMaps = AssetImage('assets/icons/2D/google_maps.png');
}

abstract class MimSvg {
  static const twoD = 'assets/icons/2D/2d.svg';
  static const arrowLeft = 'assets/icons/2D/arrow_left.svg';
  static const arrowOut = 'assets/icons/2D/arrow_out.svg';
  static const arrowRight = 'assets/icons/2D/arrow_right.svg';
  static const bookmarkOff = 'assets/icons/2D/bookmark_off.svg';
  static const bookmarkOn = 'assets/icons/2D/bookmark_on.svg';
  static const contact = 'assets/icons/2D/contact.svg';
  static const github = 'assets/icons/2D/github.svg';
  static const search = 'assets/icons/2D/search.svg';
  static const twitter = 'assets/icons/2D/twitter.svg';
  static const user = 'assets/icons/2D/user.svg';
}

abstract class MimCategoryIcons {
  static const hi = '👋';
  static const sandwich = '🥪';

  static const ci = '🔄';
  static const design = '🖊️';
  static const gaming = '🎮';
  static const health = '🏥';
  static const iot = '⌚';
  static const methodology = '🔧';
  static const mobileForGood = '🍃';
  static const multipleFrameworks = '🚀';
  static const nativeFrameworks = '📱';
  static const others = '🎙️';
  static const product = '🛒';
  static const pwa = '🖥️';
  static const security = '🔐';
  static const successAndFailure = '⭐';
}
