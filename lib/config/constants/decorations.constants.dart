import 'package:dotted_decoration/dotted_decoration.dart';
import 'package:flutter/material.dart';
import 'package:mim_mobile/config/constants/colors.constants.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';

class MimDecorations {
  static Decoration dottedBottomLine = DottedDecoration(
    shape: Shape.line,
    linePosition: LinePosition.bottom,
    dash: [MimSizes.xxxsmall.toInt(), MimSizes.xxxsmall.toInt()],
    strokeWidth: MimSizes.xxxsmall,
    color: MimColors.primary32,
  );
  static Decoration dottedTopLine = DottedDecoration(
    shape: Shape.line,
    linePosition: LinePosition.top,
    dash: [MimSizes.xxxsmall.toInt(), MimSizes.xxxsmall.toInt()],
    strokeWidth: MimSizes.xxxsmall,
    color: MimColors.primary32,
  );
  static Decoration dottedBox = DottedDecoration(
    shape: Shape.box,
    borderRadius: BorderRadius.circular(MimRadius.medium),
    dash: [MimSizes.xxxsmall.toInt(), MimSizes.xxxsmall.toInt()],
    strokeWidth: MimSizes.xxxsmall,
    color: MimColors.primary32,
  );
}
