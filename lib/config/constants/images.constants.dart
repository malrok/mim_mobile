import 'package:flutter/material.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';

abstract class MimImagePaths {
  static const error = 'assets/images/error.png';

  static const illustration = 'assets/images/mim.png';

  static const localization = 'assets/images/localization.png';

  static const logo = 'assets/images/logo_full.png';

  static const map = 'assets/images/map.jpg';

  static const noEvent = 'assets/images/noEvent.png';

  static const notFound = 'assets/images/notFound.png';
}

abstract class MimImages {
  static final error = Image.asset(MimImagePaths.error, height: MimSizes.hugeMedium);

  static final illustration = Image.asset(MimImagePaths.illustration, height: MimSizes.hugeLarge);

  static final localization = Image.asset(MimImagePaths.localization, height: MimSizes.hugeLarge);

  static final logo = Image.asset(MimImagePaths.logo, height: MimSizes.xxlarge);

  static final noEvent = Image.asset(MimImagePaths.noEvent, height: MimSizes.hugeLarge);

  static final notFound = Image.asset(MimImagePaths.notFound, height: MimSizes.hugeMedium);
}
