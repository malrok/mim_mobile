class MimSizes {
  static const double zero = 0;

  static const double xxxsmall = 2;
  static const double xxsmall = 4;
  static const double xsmall = 8;
  static const double small = 12;
  static const double regular = 16;
  static const double medium = 24;
  static const double large = 32;
  static const double xlarge = 48;
  static const double xxlarge = 56;
  static const double xxxlarge = 64;

  static const double hugeSmall = 40;
  static const double hugeMedium = 80;
  static const double hugeLarge = 120;
}

class MimRadius {
  static const double small = 6;
  static const double medium = 8;
  static const double large = 12;
  static const double max = 999;
}
