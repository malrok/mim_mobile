import 'package:flutter/material.dart';

class MimColors {
  static Color primary = const Color(0xFF5475FB);
  static Color primaryLight = const Color(0xFFDCE5FF);
  static Color primary32 = const Color.fromARGB(32, 84, 117, 251);
  static Color primary04 = const Color.fromARGB(10, 84, 117, 251);

  static Color red = const Color(0xFFE25C62);

  static Color black = Colors.black;
  static Color black12 = const Color.fromARGB(30, 0, 0, 0);

  static Color white = Colors.white;
  static Color whiteLight = const Color(0xFFF2F6FF);

  /// Grey
  static Color greyDark = const Color(0xFF343A40);
  static Color greyMedium = const Color(0xFF7F8A96);
  static Color greyLight = const Color(0xFFB7C3CE);
  static Color greyLight48 = const Color.fromARGB(122, 183, 195, 206);

  static Color mapBackground = const Color(0xfff7f7f7);

  static Color background = const Color(0xFFEDF2FF);
  static Color divider = const Color(0xFFDEE4F5);
}
