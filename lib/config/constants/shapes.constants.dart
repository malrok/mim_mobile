import 'package:avatars/avatars.dart';
import 'package:flutter/material.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';

class MimShapes {
  static RoundedRectangleBorder buttonShape = RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(MimSizes.small),
  );
  static AvatarShape smallSpeakerShape = AvatarShape.circle(MimSizes.large);
  static AvatarShape normalSpeakerShape = AvatarShape.rectangle(
    MimSizes.hugeMedium,
    MimSizes.hugeMedium,
    const BorderRadius.all(Radius.circular(MimSizes.small)),
  );
  static AvatarShape largeSpeakerShape = AvatarShape.rectangle(
    MimSizes.hugeLarge,
    MimSizes.hugeLarge,
    const BorderRadius.all(Radius.circular(MimSizes.small)),
  );
}
