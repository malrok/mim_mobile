// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:get_it/get_it.dart' as _i174;
import 'package:injectable/injectable.dart' as _i526;
import 'package:mim_mobile/core/data/datasources/local/base_floor_app_database.dart'
    as _i256;
import 'package:mim_mobile/core/data/datasources/local/database_accessor.dart'
    as _i361;
import 'package:mim_mobile/core/data/datasources/local/floor_app_database_dev.dart'
    as _i922;
import 'package:mim_mobile/core/data/datasources/local/floor_app_database_prod.dart'
    as _i507;
import 'package:mim_mobile/features/events/data/datasources/remote/base_events_client.dart'
    as _i13;
import 'package:mim_mobile/features/events/data/datasources/remote/events.client.dart'
    as _i323;
import 'package:mim_mobile/features/events/data/datasources/remote/events.client.dev.dart'
    as _i433;
import 'package:mim_mobile/features/events/data/datasources/remote/events.remote_datasource.dart'
    as _i551;
import 'package:mim_mobile/features/events/data/repositories/events.repository.dart'
    as _i458;
import 'package:mim_mobile/features/events/domain/repositories/ievents.repository.dart'
    as _i39;
import 'package:mim_mobile/features/events/domain/use_cases/get_event_by_id.use_case.dart'
    as _i915;
import 'package:mim_mobile/features/events/domain/use_cases/get_events_list.use_case.dart'
    as _i632;
import 'package:mim_mobile/features/events/domain/use_cases/get_favorite_events_list.use_case.dart'
    as _i391;
import 'package:mim_mobile/features/events/domain/use_cases/refresh_events.use_case.dart'
    as _i958;
import 'package:mim_mobile/features/events/domain/use_cases/update_event_favorite.use_case.dart'
    as _i256;
import 'package:mim_mobile/features/map/data/datasources/local/preferences.dao.dart'
    as _i526;
import 'package:mim_mobile/features/map/data/repository/preferences.repository.dart'
    as _i47;
import 'package:mim_mobile/features/map/domain/repositories/ipreferences.repository.dart'
    as _i1025;
import 'package:mim_mobile/features/map/domain/use_cases/get_three_dimensions_map_setting.use_case.dart'
    as _i1009;
import 'package:mim_mobile/features/map/domain/use_cases/set_three_dimensions_map_setting.use_case.dart'
    as _i699;
import 'package:mim_mobile/features/params/data/datasources/local/base_params_dao.dart'
    as _i878;
import 'package:mim_mobile/features/params/data/datasources/local/params.dao.dart'
    as _i136;
import 'package:mim_mobile/features/params/data/datasources/local/params.dao.dev.dart'
    as _i96;
import 'package:mim_mobile/features/params/data/datasources/remote/base_params_client.dart'
    as _i646;
import 'package:mim_mobile/features/params/data/datasources/remote/params.client.dart'
    as _i827;
import 'package:mim_mobile/features/params/data/datasources/remote/params.client.dev.dart'
    as _i800;
import 'package:mim_mobile/features/params/data/datasources/remote/params.remote_datasource.dart'
    as _i769;
import 'package:mim_mobile/features/params/data/repositories/params.repository.dart'
    as _i523;
import 'package:mim_mobile/features/params/domain/repository/iparams.repository.dart'
    as _i1070;
import 'package:mim_mobile/features/params/domain/use_cases/check_event_date.use_case.dart'
    as _i869;
import 'package:mim_mobile/features/params/domain/use_cases/get_params.use_case.dart'
    as _i701;
import 'package:mim_mobile/features/params/domain/use_cases/refresh_params.use_case.dart'
    as _i495;
import 'package:mim_mobile/features/speakers/data/repositories/speakers.repository.dart'
    as _i32;
import 'package:mim_mobile/features/speakers/domain/repositories/ispeakers.repository.dart'
    as _i127;
import 'package:mim_mobile/features/speakers/domain/use_cases/get_speaker_by_id.use_case.dart'
    as _i205;
import 'package:mim_mobile/features/speakers/domain/use_cases/get_speakers_list.use_case.dart'
    as _i599;
import 'package:mim_mobile/features/sponsors/data/datasources/remote/base_sponsors_client.dart'
    as _i257;
import 'package:mim_mobile/features/sponsors/data/datasources/remote/sponsors.client.dart'
    as _i879;
import 'package:mim_mobile/features/sponsors/data/datasources/remote/sponsors.client.dev.dart'
    as _i791;
import 'package:mim_mobile/features/sponsors/data/datasources/remote/sponsors.remote_datasource.dart'
    as _i584;
import 'package:mim_mobile/features/sponsors/data/repositories/sponsors.repository.dart'
    as _i398;
import 'package:mim_mobile/features/sponsors/domain/repositories/isponsors.repository.dart'
    as _i469;
import 'package:mim_mobile/features/sponsors/domain/use_cases/get_sponsor_details.use_case.dart'
    as _i964;
import 'package:mim_mobile/features/sponsors/domain/use_cases/get_sponsors_list.use_case.dart'
    as _i31;
import 'package:mim_mobile/features/sponsors/domain/use_cases/refresh_sponsors_list.use_case.dart'
    as _i367;

const String _dev = 'dev';
const String _prod = 'prod';

extension GetItInjectableX on _i174.GetIt {
// initializes the registration of main-scope dependencies inside of GetIt
  _i174.GetIt init({
    String? environment,
    _i526.EnvironmentFilter? environmentFilter,
  }) {
    final gh = _i526.GetItHelper(
      this,
      environment,
      environmentFilter,
    );
    gh.singletonAsync<_i526.PreferencesDao>(
        () => _i526.PreferencesDao.create());
    gh.singletonAsync<_i1025.IPreferencesRepository>(() async =>
        _i47.PreferencesRepository(await getAsync<_i526.PreferencesDao>()));
    gh.singletonAsync<_i257.BaseSponsorsClient>(
      () async => _i791.MockSponsorsClient.instantiate(),
      registerFor: {_dev},
    );
    gh.singletonAsync<_i878.BaseParamsDao>(
      () => _i96.MockParamsDao.instantiate(),
      registerFor: {_dev},
    );
    gh.singletonAsync<_i646.BaseParamsClient>(
      () => _i800.MockParamsClient.instantiate(),
      registerFor: {_dev},
    );
    gh.factoryAsync<_i1009.GetThreeDimensionsMapSettingUseCase>(() async =>
        _i1009.GetThreeDimensionsMapSettingUseCase(
            await getAsync<_i1025.IPreferencesRepository>()));
    gh.factoryAsync<_i699.SetThreeDimensionsMapSettingUseCase>(() async =>
        _i699.SetThreeDimensionsMapSettingUseCase(
            await getAsync<_i1025.IPreferencesRepository>()));
    gh.factory<_i256.BaseFloorAppDatabase>(
      () => _i922.FloorAppDatabase(),
      registerFor: {_dev},
    );
    gh.factoryAsync<_i584.SponsorsRemoteDatasource>(() async =>
        _i584.SponsorsRemoteDatasource(
            await getAsync<_i257.BaseSponsorsClient>()));
    gh.singletonAsync<_i878.BaseParamsDao>(
      () => _i136.ParamsDao.instantiate(),
      registerFor: {_prod},
    );
    gh.factory<_i256.BaseFloorAppDatabase>(
      () => _i507.FloorAppDatabase(),
      registerFor: {_prod},
    );
    gh.singletonAsync<_i361.DatabaseAccessor>(
        () => _i361.DatabaseAccessor.create(gh<_i256.BaseFloorAppDatabase>()));
    gh.singletonAsync<_i13.BaseEventsClient>(
      () async => _i433.MockEventsClient.instantiate(
          await getAsync<_i878.BaseParamsDao>()),
      registerFor: {_dev},
    );
    gh.factoryAsync<_i769.ParamsRemoteDatasource>(() async =>
        _i769.ParamsRemoteDatasource(await getAsync<_i646.BaseParamsClient>()));
    gh.singletonAsync<_i646.BaseParamsClient>(
      () => _i827.ParamsClient.instantiate(),
      registerFor: {_prod},
    );
    gh.singletonAsync<_i469.ISponsorsRepository>(
        () async => _i398.SponsorsRepository(
              await getAsync<_i584.SponsorsRemoteDatasource>(),
              await getAsync<_i361.DatabaseAccessor>(),
            ));
    gh.singletonAsync<_i13.BaseEventsClient>(
      () async =>
          _i323.EventsClient.instantiate(await getAsync<_i878.BaseParamsDao>()),
      registerFor: {_prod},
    );
    gh.factoryAsync<_i31.GetSponsorsListUseCase>(() async =>
        _i31.GetSponsorsListUseCase(
            await getAsync<_i469.ISponsorsRepository>()));
    gh.factoryAsync<_i964.GetSponsorDetailsUseCase>(() async =>
        _i964.GetSponsorDetailsUseCase(
            await getAsync<_i469.ISponsorsRepository>()));
    gh.factoryAsync<_i367.RefreshSponsorsListUseCase>(() async =>
        _i367.RefreshSponsorsListUseCase(
            await getAsync<_i469.ISponsorsRepository>()));
    gh.singletonAsync<_i1070.IParamsRepository>(
        () async => _i523.ParamsRepository(
              await getAsync<_i769.ParamsRemoteDatasource>(),
              await getAsync<_i878.BaseParamsDao>(),
            ));
    gh.singletonAsync<_i257.BaseSponsorsClient>(
      () async => _i879.SponsorsClient.instantiate(
          await getAsync<_i878.BaseParamsDao>()),
      registerFor: {_prod},
    );
    gh.factoryAsync<_i127.ISpeakersRepository>(() async =>
        _i32.SpeakersRepository(await getAsync<_i361.DatabaseAccessor>()));
    gh.factoryAsync<_i551.EventsRemoteDatasource>(() async =>
        _i551.EventsRemoteDatasource(await getAsync<_i13.BaseEventsClient>()));
    gh.factoryAsync<_i701.GetParamsUseCase>(() async =>
        _i701.GetParamsUseCase(await getAsync<_i1070.IParamsRepository>()));
    gh.factoryAsync<_i495.RefreshParamsUseCase>(() async =>
        _i495.RefreshParamsUseCase(await getAsync<_i1070.IParamsRepository>()));
    gh.factoryAsync<_i869.CheckEventDateUseCase>(() async =>
        _i869.CheckEventDateUseCase(
            await getAsync<_i1070.IParamsRepository>()));
    gh.factoryAsync<_i599.SpeakersListUseCase>(() async =>
        _i599.SpeakersListUseCase(await getAsync<_i127.ISpeakersRepository>()));
    gh.factoryAsync<_i205.SpeakerUseCase>(() async =>
        _i205.SpeakerUseCase(await getAsync<_i127.ISpeakersRepository>()));
    gh.singletonAsync<_i39.IEventsRepository>(
        () async => _i458.EventsRepository.instantiate(
              await getAsync<_i361.DatabaseAccessor>(),
              await getAsync<_i551.EventsRemoteDatasource>(),
            ));
    gh.factoryAsync<_i632.GetEventsListUseCase>(() async =>
        _i632.GetEventsListUseCase(await getAsync<_i39.IEventsRepository>()));
    gh.factoryAsync<_i915.GetEventByIdUseCase>(() async =>
        _i915.GetEventByIdUseCase(await getAsync<_i39.IEventsRepository>()));
    gh.factoryAsync<_i391.GetFavoriteEventsListUseCase>(() async =>
        _i391.GetFavoriteEventsListUseCase(
            await getAsync<_i39.IEventsRepository>()));
    gh.factoryAsync<_i958.RefreshEventsUseCase>(() async =>
        _i958.RefreshEventsUseCase(await getAsync<_i39.IEventsRepository>()));
    gh.factoryAsync<_i256.UpdateEventFavoriteUseCase>(() async =>
        _i256.UpdateEventFavoriteUseCase(
            await getAsync<_i39.IEventsRepository>()));
    return this;
  }
}
