import 'package:flutter/material.dart';
import 'package:mim_mobile/config/constants/colors.constants.dart';
import 'package:mim_mobile/config/constants/images.constants.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';
import 'package:mim_mobile/config/constants/theme.constants.dart';

class EmptyList extends StatelessWidget {
  final String text;

  const EmptyList({
    super.key,
    required this.text,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(MimSizes.large),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          MimImages.notFound,
          const SizedBox(height: MimSizes.small),
          Text(
            text,
            style: textTheme.headlineLarge!.copyWith(color: MimColors.primary32),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}
