import 'package:flutter/material.dart';
import 'package:mim_mobile/config/constants/colors.constants.dart';
import 'package:mim_mobile/features/events/domain/entities/speaker.entity.dart';

enum SpeakerInfoSize { regular, big }

class SpeakerInfo extends StatelessWidget {
  final Speaker speaker;

  final SpeakerInfoSize size;

  const SpeakerInfo({super.key, required this.speaker, this.size = SpeakerInfoSize.regular});

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    final isBig = size == SpeakerInfoSize.big;

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          speaker.name,
          overflow: TextOverflow.ellipsis,
          style: isBig ? textTheme.headlineLarge : textTheme.headlineMedium,
        ),
        Text(
          '@ ${speaker.company}',
          overflow: TextOverflow.ellipsis,
          style: textTheme.labelLarge!.copyWith(color: MimColors.primary),
        )
      ],
    );
  }
}
