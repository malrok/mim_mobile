import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:mim_mobile/config/constants/colors.constants.dart';
import 'package:mim_mobile/config/constants/images.constants.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';
import 'package:mim_mobile/config/constants/theme.constants.dart';
import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/core/presentation/primary_button.widget.dart';

class ErrorDisplay extends StatefulWidget {
  final AppError? error;
  final Function? onRetry;

  const ErrorDisplay({super.key, this.error, this.onRetry});

  @override
  State<ErrorDisplay> createState() => _ErrorDisplayState();
}

class _ErrorDisplayState extends State<ErrorDisplay> {
  @override
  Widget build(BuildContext context) {
    final l10n = AppLocalizations.of(context)!;
    return Padding(
      padding: const EdgeInsets.all(MimSizes.large),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          MimImages.error,
          const SizedBox(height: MimSizes.medium),
          Text(
            widget.error?.getMessage(l10n) ?? 'an error occurred',
            style: textTheme.headlineMedium!.copyWith(color: MimColors.primary),
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: MimSizes.medium),
          widget.onRetry != null
              ? PrimaryButton(onPressed: () => widget.onRetry!(), label: l10n.actionRetry)
              : const SizedBox(),
        ],
      ),
    );
  }
}
