import 'package:avatars/avatars.dart';
import 'package:flutter/material.dart';
import 'package:mim_mobile/config/constants/colors.constants.dart';
import 'package:mim_mobile/config/constants/shapes.constants.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';
import 'package:mim_mobile/features/events/domain/entities/speaker.entity.dart';

enum SpeakerAvatarSize {
  small,
  normal,
  large,
}

class SpeakerAvatar extends StatelessWidget {
  final Speaker speaker;
  final SpeakerAvatarSize size;

  const SpeakerAvatar({
    super.key,
    required this.speaker,
    this.size = SpeakerAvatarSize.normal,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: _getAvatarSize(),
      height: _getAvatarSize(),
      child: Avatar(
        useCache: true,
        sources: [
          NetworkSource(speaker.pictureUrl),
        ],
        name: speaker.name,
        shape: _getAvatarShape(),
        textStyle: TextStyle(
          fontSize: _getTextSize(),
          color: MimColors.primaryLight,
        ),
      ),
    );
  }

  AvatarShape _getAvatarShape() {
    switch (size) {
      case SpeakerAvatarSize.small:
        return MimShapes.smallSpeakerShape;
      case SpeakerAvatarSize.normal:
        return MimShapes.normalSpeakerShape;
      case SpeakerAvatarSize.large:
        return MimShapes.largeSpeakerShape;
    }
  }

  double _getAvatarSize() {
    switch (size) {
      case SpeakerAvatarSize.small:
        return MimSizes.large;
      case SpeakerAvatarSize.normal:
        return MimSizes.hugeMedium;
      case SpeakerAvatarSize.large:
        return MimSizes.hugeLarge;
    }
  }

  double _getTextSize() {
    switch (size) {
      case SpeakerAvatarSize.small:
        return MimSizes.small;
      case SpeakerAvatarSize.normal:
        return MimSizes.medium;
      case SpeakerAvatarSize.large:
        return MimSizes.xlarge;
    }
  }
}
