import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mim_mobile/config/constants/colors.constants.dart';
import 'package:mim_mobile/config/constants/icons.constants.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';
import 'package:mim_mobile/config/constants/theme.constants.dart';
import 'package:mim_mobile/core/domain/helpers/datetime.helper.dart';

enum EventDurationDirection { vertical, horizontal }

class EventDuration extends StatelessWidget {
  final DateTime startDate;
  final DateTime endDate;
  final EventDurationDirection direction;

  const EventDuration({
    super.key,
    required this.startDate,
    required this.endDate,
    required this.direction,
  });

  @override
  Widget build(BuildContext context) {
    final timeStyle = textTheme.labelMedium!.copyWith(color: MimColors.greyMedium);
    final isHorizontal = direction == EventDurationDirection.horizontal;

    final content = [
      Text(datetimeToTime(startDate), style: timeStyle),
      RotatedBox(
        quarterTurns: isHorizontal ? 0 : 1,
        child: Padding(
          padding: isHorizontal
              ? const EdgeInsets.symmetric(horizontal: MimSizes.xxsmall)
              : const EdgeInsets.symmetric(vertical: MimSizes.xxsmall),
          child: SvgPicture.asset(
            MimSvg.arrowRight,
            width: MimSizes.regular,
            height: MimSizes.regular,
            colorFilter: ColorFilter.mode(MimColors.greyLight, BlendMode.srcIn),
          ),
        ),
      ),
      Text(datetimeToTime(endDate), style: timeStyle)
    ];

    return isHorizontal ? Row(children: content) : Column(children: content);
  }
}
