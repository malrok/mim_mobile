import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mim_mobile/config/constants/colors.constants.dart';
import 'package:mim_mobile/config/constants/icons.constants.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';
import 'package:mim_mobile/features/events/domain/entities/event.entity.dart';
import 'package:mim_mobile/features/events/presentation/blocs/favorite/favorite.cubit.dart';
import 'package:mim_mobile/features/events/presentation/blocs/favorite/favorite.states.dart';

class FavoriteToggle extends StatelessWidget {
  final Event event;
  final double? size;

  const FavoriteToggle({
    super.key,
    required this.event,
    this.size,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FavoriteCubit, FavoriteStates>(
      builder: (context, state) {
        return switch (state) {
          FavoriteStatesData() => GestureDetector(
              onTap: () => BlocProvider.of<FavoriteCubit>(context).updateFavorite(event),
              child: Padding(
                padding: const EdgeInsets.only(
                  left: MimSizes.xsmall,
                  bottom: MimSizes.xsmall,
                ),
                child: SvgPicture.asset(
                  state.data ? MimSvg.bookmarkOn : MimSvg.bookmarkOff,
                  width: size ?? MimSizes.medium,
                  height: size ?? MimSizes.medium,
                  colorFilter: ColorFilter.mode(MimColors.primary, BlendMode.srcIn),
                ),
              ),
            ),
          FavoriteStatesLoading() => _DeactivatedBookmark(size: size),
          FavoriteStatesError() => _DeactivatedBookmark(size: size),
        };
      },
    );
  }
}

class _DeactivatedBookmark extends StatelessWidget {
  final double? size;

  const _DeactivatedBookmark({this.size});

  @override
  Widget build(BuildContext context) {
    return SvgPicture.asset(
      MimSvg.bookmarkOff,
      width: size ?? MimSizes.medium,
      height: size ?? MimSizes.medium,
      colorFilter: ColorFilter.mode(MimColors.greyLight48, BlendMode.srcIn),
    );
  }
}
