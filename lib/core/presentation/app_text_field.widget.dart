import 'package:flutter/material.dart';
import 'package:mim_mobile/config/constants/colors.constants.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';
import 'package:mim_mobile/config/constants/theme.constants.dart';

class AppTextField extends StatefulWidget {
  final void Function(String)? onTextChanged;
  final Widget? suffixIcon;
  final String hintText;

  const AppTextField({
    super.key,
    this.onTextChanged,
    this.suffixIcon,
    required this.hintText,
  });

  @override
  State<AppTextField> createState() => _AppTextFieldState();
}

class _AppTextFieldState extends State<AppTextField> {
  final TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    _controller.addListener(() {
      if (widget.onTextChanged != null) widget.onTextChanged!(_controller.text);
    });
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: _controller,
      style: textTheme.labelLarge!.copyWith(color: MimColors.greyDark),
      cursorColor: MimColors.greyDark,
      decoration: InputDecoration(
          suffixIcon: widget.suffixIcon ?? const SizedBox(),
          filled: true,
          fillColor: MimColors.white,
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: MimColors.white),
            borderRadius: BorderRadius.circular(MimSizes.small),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: MimColors.white),
            borderRadius: BorderRadius.circular(MimSizes.small),
          ),
          hintText: widget.hintText,
          hintStyle: TextStyle(
            color: MimColors.greyMedium,
            fontStyle: FontStyle.italic,
          )),
    );
  }
}
