import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:mim_mobile/config/constants/theme.constants.dart';
import 'package:mim_mobile/core/domain/helpers/url.helper.dart';
import 'package:mim_mobile/features/events/domain/entities/formatted_text.entity.dart' as entity;
import 'package:mim_mobile/features/events/domain/value_objects/formatted_text_type.value_object.dart';

class FormattedText extends StatelessWidget {
  final entity.FormattedText value;

  const FormattedText({
    super.key,
    required this.value,
  });

  @override
  Widget build(BuildContext context) {
    switch (value.type) {
      case FormattedTextType.plain:
        return Text(value.content, style: textTheme.bodyLarge);
      case FormattedTextType.markdown:
        return MarkdownBody(
          data: value.content,
          styleSheet: MarkdownStyleSheet.fromTheme(mimTheme),
          onTapLink: (text, href, title) {
            if (href != null) openUrl(href);
          },
        );
      case FormattedTextType.html:
        return Text(value.content, style: textTheme.bodyLarge);
    }
  }
}
