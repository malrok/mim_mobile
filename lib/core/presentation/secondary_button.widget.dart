import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mim_mobile/config/constants/colors.constants.dart';
import 'package:mim_mobile/config/constants/shapes.constants.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';

class SecondaryButton extends StatelessWidget {
  final void Function()? onPressed;
  final String label;
  final String? icon;

  const SecondaryButton({
    super.key,
    required this.onPressed,
    required this.label,
    this.icon,
  });

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      style: OutlinedButton.styleFrom(
        foregroundColor: MimColors.black,
        shape: MimShapes.buttonShape,
        minimumSize: const Size(double.minPositive, MimSizes.xlarge),
        elevation: MimSizes.zero,
        side: BorderSide(width: 1.0, color: MimColors.black),
      ),
      onPressed: onPressed,
      child: Padding(
        padding: const EdgeInsets.all(MimSizes.small),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Flexible(
              child: Text(
                label,
                overflow: TextOverflow.clip,
              ),
            ),
            if (icon != null) ...[
              const SizedBox(width: MimSizes.xxsmall),
              SvgPicture.asset(
                icon!,
                width: MimSizes.medium,
                height: MimSizes.medium,
              ),
            ]
          ],
        ),
      ),
    );
  }
}
