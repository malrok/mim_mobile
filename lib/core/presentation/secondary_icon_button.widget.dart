import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mim_mobile/config/constants/colors.constants.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';

class SecondaryIconButton extends StatelessWidget {
  final void Function() onPressed;
  final String? svg;
  final AssetImage? png;
  final IconData? icon;

  const SecondaryIconButton({
    super.key,
    required this.onPressed,
    this.svg,
    this.png,
    this.icon,
  });

  @override
  Widget build(BuildContext context) {
    const size = MimSizes.large;

    return InkWell(
      borderRadius: BorderRadius.circular(
        MimRadius.medium,
      ),
      onTap: onPressed,
      child: Container(
        width: MimSizes.xlarge,
        height: MimSizes.xlarge,
        padding: const EdgeInsets.all(MimSizes.xsmall),
        decoration: BoxDecoration(
          color: Colors.transparent,
          border: Border.all(width: 1, color: MimColors.greyMedium),
          borderRadius: BorderRadius.circular(
            MimRadius.medium,
          ),
        ),
        child: _getChild(size),
      ),
    );
  }

  Widget _getChild(double size) {
    if (svg != null) {
      return SvgPicture.asset(svg!, width: size, height: size);
    } else if (png != null) {
      return Image(image: png!, width: size, height: size);
    } else {
      return Icon(icon!);
    }
  }
}
