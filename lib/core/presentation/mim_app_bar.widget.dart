import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';
import 'package:mim_mobile/config/constants/colors.constants.dart';
import 'package:mim_mobile/config/constants/decorations.constants.dart';
import 'package:mim_mobile/config/constants/icons.constants.dart';
import 'package:mim_mobile/config/constants/images.constants.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';

class MimAppBar extends StatefulWidget implements PreferredSizeWidget {
  final bool isMainNav;
  final Widget? trailing;

  const MimAppBar({
    super.key,
    this.isMainNav = true,
    this.trailing,
  });

  @override
  State<MimAppBar> createState() => _MimAppBarState();

  @override
  Size get preferredSize => const Size(double.infinity, MimSizes.hugeMedium);
}

class _MimAppBarState extends State<MimAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      toolbarHeight: MimSizes.hugeMedium,
      automaticallyImplyLeading: false,
      title: Container(
        width: double.infinity,
        padding: EdgeInsets.symmetric(
          horizontal: widget.isMainNav ? MimSizes.small : MimSizes.medium,
          vertical: widget.isMainNav ? MimSizes.small : MimSizes.regular,
        ),
        decoration: MimDecorations.dottedBottomLine,
        child: widget.isMainNav
            ? Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  MimImages.logo,
                ],
              )
            : Row(children: [
                _AppBarReturn(onTap: () => context.pop()),
                if (widget.trailing != null) ...[const Spacer(), widget.trailing!],
              ]),
      ),
    );
  }
}

class _AppBarReturn extends StatelessWidget {
  final VoidCallback onTap;

  const _AppBarReturn({required this.onTap});

  @override
  Widget build(BuildContext context) {
    final l10n = AppLocalizations.of(context)!;
    return InkWell(
      onTap: onTap,
      borderRadius: BorderRadius.circular(MimRadius.small),
      child: Padding(
        padding: const EdgeInsets.only(right: MimSizes.small),
        child: Row(
          children: [
            SvgPicture.asset(MimSvg.arrowLeft, colorFilter: ColorFilter.mode(MimColors.primary, BlendMode.srcIn)),
            const SizedBox(width: MimSizes.xxsmall),
            Text(
              l10n.backAction.toUpperCase(),
              style: Theme.of(context).textTheme.titleSmall!.copyWith(color: MimColors.primary),
            )
          ],
        ),
      ),
    );
  }
}
