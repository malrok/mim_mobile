import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:mim_mobile/config/constants/colors.constants.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';
import 'package:mim_mobile/core/presentation/speaker_avatar.widget.dart';
import 'package:mim_mobile/features/events/domain/entities/speaker.entity.dart';

class SpeakerCardSmall extends StatelessWidget {
  final Speaker speaker;

  const SpeakerCardSmall({
    super.key,
    required this.speaker,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => context.push('/speaker_detail', extra: speaker.id),
      child: Container(
        height: MimSizes.large,
        decoration: BoxDecoration(
          color: MimColors.primaryLight,
          borderRadius: BorderRadius.circular(MimRadius.large),
        ),
        child: Padding(
          padding: const EdgeInsets.only(left: 2, top: 2, right: 16, bottom: 2),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              SpeakerAvatar(
                speaker: speaker,
                size: SpeakerAvatarSize.small,
              ),
              const SizedBox(width: MimSizes.small),
              Text(
                speaker.name,
                style: Theme.of(context).textTheme.labelLarge!.copyWith(color: MimColors.primary),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
