import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mim_mobile/config/constants/colors.constants.dart';
import 'package:mim_mobile/config/constants/icons.constants.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';
import 'package:mim_mobile/core/presentation/speaker_avatar.widget.dart';
import 'package:mim_mobile/core/presentation/speaker_infos.widget.dart';
import 'package:mim_mobile/features/events/domain/entities/speaker.entity.dart';

class SpeakerCard extends StatelessWidget {
  final Speaker speaker;
  final Function onTap;
  final bool hasArrow;

  const SpeakerCard({
    super.key,
    required this.speaker,
    required this.onTap,
    this.hasArrow = false,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.circular(MimRadius.large),
      onTap: () => onTap(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SpeakerAvatar(speaker: speaker),
          const SizedBox(width: MimSizes.medium),
          Expanded(child: SpeakerInfo(speaker: speaker)),
          if (hasArrow)
            SvgPicture.asset(
              MimSvg.arrowRight,
              width: MimSizes.large,
              height: MimSizes.large,
              colorFilter: ColorFilter.mode(MimColors.primary, BlendMode.srcIn),
            )
        ],
      ),
    );
  }
}
