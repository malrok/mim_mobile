import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:mim_mobile/config/constants/icons.constants.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';

class AppNavigationBar extends StatelessWidget {
  const AppNavigationBar({super.key});

  @override
  Widget build(BuildContext context) {
    final l10n = AppLocalizations.of(context)!;

    return Padding(
      padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
      child: TabBar(
        dividerColor: Colors.transparent,
        tabs: [
          _Tab(label: l10n.tabEvents, image: MimIcons.calendar),
          _Tab(label: l10n.tabSpeakers, image: MimIcons.speakers),
          _Tab(label: l10n.tabMap, image: MimIcons.location),
          _Tab(label: l10n.tabInformation, image: MimIcons.contact),
        ],
      ),
    );
  }
}

class _Tab extends StatelessWidget {
  final String label;
  final AssetImage image;

  const _Tab({
    required this.label,
    required this.image,
  });

  @override
  Widget build(BuildContext context) {
    const iconSize = MimSizes.large;
    final double width = MediaQuery.of(context).size.width / 4;

    return SizedBox(
      width: width,
      child: Tab(
        icon: Image(
          image: image,
          width: iconSize,
          height: iconSize,
        ),
        text: label,
      ),
    );
  }
}
