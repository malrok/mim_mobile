import 'package:flutter/material.dart';
import 'package:mim_mobile/config/constants/colors.constants.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';

class WhiteCard extends StatelessWidget {
  final void Function()? onTap;
  final Widget child;

  const WhiteCard({
    super.key,
    required this.onTap,
    required this.child,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.circular(
        MimRadius.medium,
      ),
      color: MimColors.white,
      child: InkWell(
        borderRadius: BorderRadius.circular(
          MimRadius.medium,
        ),
        onTap: onTap,
        child: Container(
          padding: const EdgeInsets.all(MimSizes.regular),
          child: child,
        ),
      ),
    );
  }
}
