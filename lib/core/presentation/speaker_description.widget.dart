import 'package:flutter/material.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';
import 'package:mim_mobile/core/presentation/formatted_text.widget.dart';
import 'package:mim_mobile/features/events/domain/entities/speaker.entity.dart';
import 'package:mim_mobile/features/speakers/presentation/pages/speaker_detail/widgets/speaker_networks.widget.dart';

class SpeakerDescription extends StatelessWidget {
  final Speaker speaker;

  const SpeakerDescription({
    super.key,
    required this.speaker,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (speaker.bio.isNotEmpty) FormattedText(value: speaker.bio),
        const SizedBox(height: MimSizes.medium),
        SpeakerNetworks(
          githubUrl: speaker.githubUrl,
          twitterUrl: speaker.twitterUrl,
        )
      ],
    );
  }
}
