import 'package:flutter/material.dart';
import 'package:mim_mobile/config/constants/colors.constants.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';

enum AppTagSize { regular, big }

class AppTag extends StatelessWidget {
  final String label;
  final String icon;

  final AppTagSize size;

  const AppTag({super.key, required this.label, required this.icon, this.size = AppTagSize.regular});

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    final isBig = size == AppTagSize.big;

    final greyMedium = MimColors.greyMedium;

    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: isBig ? MimSizes.small : MimSizes.xxsmall,
        vertical: isBig ? MimSizes.xsmall : MimSizes.xxsmall,
      ),
      decoration: BoxDecoration(
          border: Border.all(color: MimColors.greyLight48, width: 1),
          borderRadius: BorderRadius.circular(
            isBig ? MimRadius.medium : MimRadius.small,
          )),
      child: Row(children: [
        Text(icon, style: isBig ? textTheme.labelLarge : textTheme.labelMedium),
        SizedBox(width: isBig ? MimSizes.xsmall : MimSizes.xxsmall),
        Text(label,
            style: isBig
                ? textTheme.labelLarge!.copyWith(color: greyMedium)
                : textTheme.labelMedium!.copyWith(color: greyMedium)),
      ]),
    );
  }
}
