import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mim_mobile/config/constants/colors.constants.dart';
import 'package:mim_mobile/config/constants/shapes.constants.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';

class PrimaryButton extends StatelessWidget {
  final void Function()? onPressed;
  final String label;
  final String? icon;

  const PrimaryButton({
    super.key,
    required this.onPressed,
    required this.label,
    this.icon,
  });

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        backgroundColor: MimColors.black,
        shape: MimShapes.buttonShape,
        minimumSize: const Size(double.minPositive, MimSizes.xlarge),
        elevation: MimSizes.zero,
      ),
      onPressed: onPressed,
      child: Padding(
        padding: const EdgeInsets.all(MimSizes.small),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              label,
              style: Theme.of(context).textTheme.labelLarge!.copyWith(color: MimColors.background),
            ),
            if (icon != null) ...[
              const SizedBox(width: MimSizes.xxsmall),
              SvgPicture.asset(
                icon!,
                width: MimSizes.medium,
                height: MimSizes.medium,
                colorFilter: ColorFilter.mode(MimColors.background, BlendMode.srcIn),
              ),
            ]
          ],
        ),
      ),
    );
  }
}
