import 'package:dotted_decoration/dotted_decoration.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mim_mobile/config/constants/colors.constants.dart';
import 'package:mim_mobile/config/constants/decorations.constants.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';
import 'package:mim_mobile/config/constants/theme.constants.dart';
import 'package:mim_mobile/core/domain/helpers/datetime.helper.dart';
import 'package:mim_mobile/core/presentation/app_tag.widget.dart';
import 'package:mim_mobile/core/presentation/favorite_toggle.widget.dart';
import 'package:mim_mobile/core/presentation/size_provider.widget.dart';
import 'package:mim_mobile/core/presentation/white_card.widget.dart';
import 'package:mim_mobile/features/events/domain/entities/event.entity.dart';
import 'package:mim_mobile/features/events/domain/value_objects/talk_categories.value_object.dart';
import 'package:mim_mobile/features/events/presentation/blocs/favorite/favorite.cubit.dart';

class PlanningElement extends StatefulWidget {
  final List<Event> events;
  final DateTime start;
  final DateTime end;
  final void Function(Event)? onTap;

  const PlanningElement({
    super.key,
    required this.events,
    required this.start,
    required this.end,
    this.onTap,
  });

  @override
  State<PlanningElement> createState() => _PlanningElementState();
}

class _PlanningElementState extends State<PlanningElement> {
  double? height;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: MimSizes.xsmall),
      child: SizeProviderWidget(
        onChildSize: (Size size) {
          setState(() {
            height = size.height;
          });
        },
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            _Schedules(
              start: widget.start,
              end: widget.end,
              height: height,
              hasWarning: widget.events.length > 1,
            ),
            const SizedBox(width: MimSizes.medium),
            Expanded(
              child: ListView.separated(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (BuildContext context, int index) {
                  final event = widget.events[index];
                  if (event.category == TalkCategories.na) {
                    return _CardDetailForOther(event: event);
                  } else {
                    return _CardDetailForTalk(
                        event: event, onTap: widget.onTap != null ? () => widget.onTap!(event) : null);
                  }
                },
                separatorBuilder: (BuildContext context, int index) => const SizedBox(height: MimSizes.xsmall),
                itemCount: widget.events.length,
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _Schedules extends StatelessWidget {
  final DateTime start;
  final DateTime end;
  final double? height;
  final bool hasWarning;

  const _Schedules({
    required this.start,
    required this.end,
    required this.height,
    required this.hasWarning,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height ?? 64,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            datetimeToTime(start),
            style: textTheme.labelMedium!.copyWith(color: MimColors.greyMedium),
          ),
          Expanded(
            child: hasWarning
                ? Container(
                    width: 1,
                    decoration: DottedDecoration(
                      shape: Shape.line,
                      linePosition: LinePosition.left,
                      color: hasWarning ? MimColors.red : MimColors.greyLight,
                    ),
                  )
                : Container(width: MimSizes.xxxsmall, color: MimColors.greyLight),
          ),
          Text(
            datetimeToTime(end),
            style: textTheme.labelMedium!.copyWith(color: MimColors.greyMedium),
          ),
        ],
      ),
    );
  }
}

class _CardDetailForOther extends StatelessWidget {
  final Event event;

  const _CardDetailForOther({
    required this.event,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: MimDecorations.dottedBox,
      padding: const EdgeInsets.all(MimSizes.regular),
      child: Column(
        children: [
          Text(
            event.label,
            softWrap: true,
            style: textTheme.headlineSmall!.copyWith(color: MimColors.primary),
          ),
        ],
      ),
    );
  }
}

class _CardDetailForTalk extends StatelessWidget {
  final Event event;
  final void Function()? onTap;

  const _CardDetailForTalk({
    required this.event,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return BlocProvider<FavoriteCubit>(
      create: (_) => FavoriteCubit()..load(event.id),
      child: WhiteCard(
        onTap: onTap,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    event.label,
                    softWrap: true,
                    style: textTheme.headlineSmall!.copyWith(color: MimColors.black),
                  ),
                  const SizedBox(height: MimSizes.xsmall),
                  FittedBox(
                    child: AppTag(
                      label: event.category.desc,
                      icon: event.category.icon,
                    ),
                  )
                ],
              ),
            ),
            FavoriteToggle(event: event),
          ],
        ),
      ),
    );
  }
}
