import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:mim_mobile/core/domain/exceptions/connection.exception.dart';
import 'package:mim_mobile/core/domain/exceptions/no_events.exception.dart';
import 'package:mim_mobile/core/domain/exceptions/no_params.exception.dart';
import 'package:mim_mobile/core/domain/exceptions/no_speakers.exception.dart';
import 'package:mim_mobile/core/domain/exceptions/remote.exception.dart';

class AppError {
  dynamic error;

  AppError(this.error);

  String getMessage(AppLocalizations l10n) {
    if (error is String) {
      return error;
    } else if (error is ConnectionException) {
      return l10n.noConnection;
    } else if (error is NoEventsException) {
      return l10n.noEventsFound;
    } else if (error is NoSpeakersException) {
      return l10n.noSpeakersFound;
    } else if (error is RemoteException) {
      return l10n.unknownServerError;
    } else if (error is NoParamsException) {
      return l10n.noParamsError;
    }
    return error.toString();
  }
}
