import 'package:intl/intl.dart';

final _timeFormat = DateFormat("HH:mm");

String datetimeToTime(DateTime dateTime) => _timeFormat.format(dateTime);
