import 'package:mim_mobile/core/domain/helpers/url.helper.dart';

void mailto(String email, String subject) {
  openUrl('mailto:$email?subject=$subject');
}
