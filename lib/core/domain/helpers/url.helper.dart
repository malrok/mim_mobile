import 'package:url_launcher/url_launcher.dart';

Future<bool> openUrl(String url) async => launchUrl(Uri.parse(url));

String getTwitterUrl(String speakerTwitter) {
  if (speakerTwitter.isEmpty) return '';
  return 'https://twitter.com/${speakerTwitter.substring(1)}';
}

String getGithubUrl(String speakerGithub) {
  if (speakerGithub.isEmpty) return '';
  return 'https://github.com/$speakerGithub';
}
