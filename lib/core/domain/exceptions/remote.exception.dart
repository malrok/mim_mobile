class RemoteException extends Error {
  final dynamic error;

  RemoteException(this.error);
}
