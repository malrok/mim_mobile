import 'package:flutter/material.dart';
import 'package:mim_mobile/core/data/datasources/remote/connection.helper.dart';
import 'package:mim_mobile/core/domain/exceptions/connection.exception.dart';

abstract class BaseRemoteDatasource<T> {
  @protected
  Future<T> execute(Future<T> request) async {
    if (await ConnectionHelper.isConnected()) {
      return request;
    }
    throw ConnectionException();
  }
}
