import 'package:dio/dio.dart';

final options = BaseOptions(
  baseUrl: 'https://thomas-boutin.github.io',
);

final httpClient = Dio(options);
