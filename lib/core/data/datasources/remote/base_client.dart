import 'package:dio/dio.dart';
import 'package:mim_mobile/core/domain/exceptions/no_params.exception.dart';
import 'package:mim_mobile/core/domain/exceptions/remote.exception.dart';

abstract class BaseClient<T> {
  final Dio _dio;

  BaseClient(this._dio);

  Future<T> execute(String path) async {
    late Response<T> response;

    if (path.isEmpty) {
      throw NoParamsException();
    }

    final finalPath = path.startsWith('/') ? path : '/$path';

    try {
      response = await _dio.get<T>(finalPath);
    } on DioException catch (error) {
      throw RemoteException(error);
    } catch (error) {
      throw RemoteException(error);
    }

    if (response.statusCode == 200 && response.data != null) {
      try {
        return response.data as T;
      } catch (error) {
        throw RemoteException(error);
      }
    }

    throw RemoteException(response);
  }
}
