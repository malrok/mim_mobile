// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_database.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

abstract class $AppDatabaseBuilderContract {
  /// Adds migrations to the builder.
  $AppDatabaseBuilderContract addMigrations(List<Migration> migrations);

  /// Adds a database [Callback] to the builder.
  $AppDatabaseBuilderContract addCallback(Callback callback);

  /// Creates the database and initializes it.
  Future<AppDatabase> build();
}

// ignore: avoid_classes_with_only_static_members
class $FloorAppDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static $AppDatabaseBuilderContract databaseBuilder(String name) =>
      _$AppDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static $AppDatabaseBuilderContract inMemoryDatabaseBuilder() =>
      _$AppDatabaseBuilder(null);
}

class _$AppDatabaseBuilder implements $AppDatabaseBuilderContract {
  _$AppDatabaseBuilder(this.name);

  final String? name;

  final List<Migration> _migrations = [];

  Callback? _callback;

  @override
  $AppDatabaseBuilderContract addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  @override
  $AppDatabaseBuilderContract addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  @override
  Future<AppDatabase> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name!)
        : ':memory:';
    final database = _$AppDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$AppDatabase extends AppDatabase {
  _$AppDatabase([StreamController<String>? listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  EventDao? _eventDaoInstance;

  SpeakerDao? _speakerDaoInstance;

  EventSpeakerDao? _eventSpeakerDaoInstance;

  SponsorDao? _sponsorDaoInstance;

  JobOfferDao? _jobOfferDaoInstance;

  Future<sqflite.Database> open(
    String path,
    List<Migration> migrations, [
    Callback? callback,
  ]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 2,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
        await callback?.onConfigure?.call(database);
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `event` (`id` TEXT NOT NULL, `openFeedbackId` TEXT NOT NULL, `label` TEXT NOT NULL, `description` TEXT NOT NULL, `start_date` INTEGER NOT NULL, `end_date` INTEGER NOT NULL, `room` TEXT NOT NULL, `category` TEXT NOT NULL, `type` TEXT NOT NULL, `language` TEXT NOT NULL, `isFavorite` INTEGER NOT NULL, PRIMARY KEY (`id`))');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `speaker` (`id` INTEGER NOT NULL, `name` TEXT NOT NULL, `bio` TEXT NOT NULL, `picture_url` TEXT NOT NULL, `company` TEXT NOT NULL, `twitter_url` TEXT NOT NULL, `github_url` TEXT NOT NULL, PRIMARY KEY (`id`))');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `event_speaker` (`id` INTEGER NOT NULL, `speaker_id` INTEGER NOT NULL, `event_id` TEXT NOT NULL, FOREIGN KEY (`speaker_id`) REFERENCES `speaker` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION, FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION, PRIMARY KEY (`id`))');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `sponsor` (`sponsor_key` TEXT NOT NULL, `name` TEXT NOT NULL, `description_fr` TEXT NOT NULL, `description_en` TEXT NOT NULL, `logo` TEXT NOT NULL, `level` TEXT NOT NULL, `url` TEXT NOT NULL, PRIMARY KEY (`sponsor_key`))');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `sponsor_job_offer` (`id` INTEGER NOT NULL, `title` TEXT NOT NULL, `link` TEXT NOT NULL, `sponsor_key` TEXT NOT NULL, FOREIGN KEY (`sponsor_key`) REFERENCES `sponsor` (`sponsor_key`) ON UPDATE NO ACTION ON DELETE NO ACTION, PRIMARY KEY (`id`))');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  EventDao get eventDao {
    return _eventDaoInstance ??= _$EventDao(database, changeListener);
  }

  @override
  SpeakerDao get speakerDao {
    return _speakerDaoInstance ??= _$SpeakerDao(database, changeListener);
  }

  @override
  EventSpeakerDao get eventSpeakerDao {
    return _eventSpeakerDaoInstance ??=
        _$EventSpeakerDao(database, changeListener);
  }

  @override
  SponsorDao get sponsorDao {
    return _sponsorDaoInstance ??= _$SponsorDao(database, changeListener);
  }

  @override
  JobOfferDao get jobOfferDao {
    return _jobOfferDaoInstance ??= _$JobOfferDao(database, changeListener);
  }
}

class _$EventDao extends EventDao {
  _$EventDao(
    this.database,
    this.changeListener,
  )   : _queryAdapter = QueryAdapter(database, changeListener),
        _eventDboInsertionAdapter = InsertionAdapter(
            database,
            'event',
            (EventDbo item) => <String, Object?>{
                  'id': item.id,
                  'openFeedbackId': item.openFeedbackId,
                  'label': item.label,
                  'description': item.description,
                  'start_date': _dateTimeConverter.encode(item.startDate),
                  'end_date': _dateTimeConverter.encode(item.endDate),
                  'room': item.room,
                  'category': item.category,
                  'type': item.type,
                  'language': item.language,
                  'isFavorite': item.isFavorite ? 1 : 0
                },
            changeListener),
        _eventDboUpdateAdapter = UpdateAdapter(
            database,
            'event',
            ['id'],
            (EventDbo item) => <String, Object?>{
                  'id': item.id,
                  'openFeedbackId': item.openFeedbackId,
                  'label': item.label,
                  'description': item.description,
                  'start_date': _dateTimeConverter.encode(item.startDate),
                  'end_date': _dateTimeConverter.encode(item.endDate),
                  'room': item.room,
                  'category': item.category,
                  'type': item.type,
                  'language': item.language,
                  'isFavorite': item.isFavorite ? 1 : 0
                },
            changeListener);

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<EventDbo> _eventDboInsertionAdapter;

  final UpdateAdapter<EventDbo> _eventDboUpdateAdapter;

  @override
  Future<List<EventDbo>> getAllEventsOnce() async {
    return _queryAdapter.queryList('SELECT * FROM event',
        mapper: (Map<String, Object?> row) => EventDbo(
            row['id'] as String,
            row['openFeedbackId'] as String,
            row['label'] as String,
            row['description'] as String,
            _dateTimeConverter.decode(row['start_date'] as int),
            _dateTimeConverter.decode(row['end_date'] as int),
            row['room'] as String,
            row['category'] as String,
            row['type'] as String,
            row['language'] as String,
            (row['isFavorite'] as int) != 0));
  }

  @override
  Stream<List<EventDbo>> getAllEvents() {
    return _queryAdapter.queryListStream('SELECT * FROM event',
        mapper: (Map<String, Object?> row) => EventDbo(
            row['id'] as String,
            row['openFeedbackId'] as String,
            row['label'] as String,
            row['description'] as String,
            _dateTimeConverter.decode(row['start_date'] as int),
            _dateTimeConverter.decode(row['end_date'] as int),
            row['room'] as String,
            row['category'] as String,
            row['type'] as String,
            row['language'] as String,
            (row['isFavorite'] as int) != 0),
        queryableName: 'event',
        isView: false);
  }

  @override
  Stream<List<EventDbo>> getAllFavoriteEvents() {
    return _queryAdapter.queryListStream(
        'SELECT * FROM event where isFavorite = true order by start_date',
        mapper: (Map<String, Object?> row) => EventDbo(
            row['id'] as String,
            row['openFeedbackId'] as String,
            row['label'] as String,
            row['description'] as String,
            _dateTimeConverter.decode(row['start_date'] as int),
            _dateTimeConverter.decode(row['end_date'] as int),
            row['room'] as String,
            row['category'] as String,
            row['type'] as String,
            row['language'] as String,
            (row['isFavorite'] as int) != 0),
        queryableName: 'event',
        isView: false);
  }

  @override
  Future<EventDbo?> getEventById(String id) async {
    return _queryAdapter.query('SELECT * FROM event WHERE id = ?1',
        mapper: (Map<String, Object?> row) => EventDbo(
            row['id'] as String,
            row['openFeedbackId'] as String,
            row['label'] as String,
            row['description'] as String,
            _dateTimeConverter.decode(row['start_date'] as int),
            _dateTimeConverter.decode(row['end_date'] as int),
            row['room'] as String,
            row['category'] as String,
            row['type'] as String,
            row['language'] as String,
            (row['isFavorite'] as int) != 0),
        arguments: [id]);
  }

  @override
  Future<List<EventDbo>> getEventsByIds(List<String> ids) async {
    const offset = 1;
    final _sqliteVariablesForIds =
        Iterable<String>.generate(ids.length, (i) => '?${i + offset}')
            .join(',');
    return _queryAdapter.queryList(
        'SELECT * FROM event WHERE id IN (' + _sqliteVariablesForIds + ')',
        mapper: (Map<String, Object?> row) => EventDbo(
            row['id'] as String,
            row['openFeedbackId'] as String,
            row['label'] as String,
            row['description'] as String,
            _dateTimeConverter.decode(row['start_date'] as int),
            _dateTimeConverter.decode(row['end_date'] as int),
            row['room'] as String,
            row['category'] as String,
            row['type'] as String,
            row['language'] as String,
            (row['isFavorite'] as int) != 0),
        arguments: [...ids]);
  }

  @override
  Future<void> deleteEvents() async {
    await _queryAdapter.queryNoReturn('DELETE FROM event');
  }

  @override
  Future<void> insertEvent(EventDbo event) async {
    await _eventDboInsertionAdapter.insert(event, OnConflictStrategy.abort);
  }

  @override
  Future<void> updateEvent(EventDbo event) async {
    await _eventDboUpdateAdapter.update(event, OnConflictStrategy.abort);
  }
}

class _$SpeakerDao extends SpeakerDao {
  _$SpeakerDao(
    this.database,
    this.changeListener,
  )   : _queryAdapter = QueryAdapter(database),
        _speakerDboInsertionAdapter = InsertionAdapter(
            database,
            'speaker',
            (SpeakerDbo item) => <String, Object?>{
                  'id': item.id,
                  'name': item.name,
                  'bio': item.bio,
                  'picture_url': item.pictureUrl,
                  'company': item.company,
                  'twitter_url': item.twitterUrl,
                  'github_url': item.githubUrl
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<SpeakerDbo> _speakerDboInsertionAdapter;

  @override
  Future<List<SpeakerDbo>> getAllSpeakers() async {
    return _queryAdapter.queryList('SELECT * FROM speaker',
        mapper: (Map<String, Object?> row) => SpeakerDbo(
            row['id'] as int,
            row['name'] as String,
            row['bio'] as String,
            row['picture_url'] as String,
            row['company'] as String,
            row['twitter_url'] as String,
            row['github_url'] as String));
  }

  @override
  Future<SpeakerDbo?> getSpeakerById(int id) async {
    return _queryAdapter.query('SELECT * FROM speaker WHERE id = ?1',
        mapper: (Map<String, Object?> row) => SpeakerDbo(
            row['id'] as int,
            row['name'] as String,
            row['bio'] as String,
            row['picture_url'] as String,
            row['company'] as String,
            row['twitter_url'] as String,
            row['github_url'] as String),
        arguments: [id]);
  }

  @override
  Future<List<SpeakerDbo>> getSpeakersByIds(List<int> ids) async {
    const offset = 1;
    final _sqliteVariablesForIds =
        Iterable<String>.generate(ids.length, (i) => '?${i + offset}')
            .join(',');
    return _queryAdapter.queryList(
        'SELECT * FROM speaker WHERE id IN (' + _sqliteVariablesForIds + ')',
        mapper: (Map<String, Object?> row) => SpeakerDbo(
            row['id'] as int,
            row['name'] as String,
            row['bio'] as String,
            row['picture_url'] as String,
            row['company'] as String,
            row['twitter_url'] as String,
            row['github_url'] as String),
        arguments: [...ids]);
  }

  @override
  Future<void> deleteSpeakers() async {
    await _queryAdapter.queryNoReturn('DELETE FROM speaker');
  }

  @override
  Future<void> insertSpeaker(SpeakerDbo speaker) async {
    await _speakerDboInsertionAdapter.insert(speaker, OnConflictStrategy.abort);
  }
}

class _$EventSpeakerDao extends EventSpeakerDao {
  _$EventSpeakerDao(
    this.database,
    this.changeListener,
  )   : _queryAdapter = QueryAdapter(database),
        _eventSpeakerDboInsertionAdapter = InsertionAdapter(
            database,
            'event_speaker',
            (EventSpeakerDbo item) => <String, Object?>{
                  'id': item.id,
                  'speaker_id': item.speakerId,
                  'event_id': item.eventId
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<EventSpeakerDbo> _eventSpeakerDboInsertionAdapter;

  @override
  Future<List<EventSpeakerDbo>> getAllEventSpeakers() async {
    return _queryAdapter.queryList('SELECT * FROM event_speaker',
        mapper: (Map<String, Object?> row) => EventSpeakerDbo(row['id'] as int,
            row['speaker_id'] as int, row['event_id'] as String));
  }

  @override
  Future<List<EventSpeakerDbo>> getEventsSpeakersByEventId(String id) async {
    return _queryAdapter.queryList(
        'SELECT * FROM event_speaker WHERE event_id = ?1',
        mapper: (Map<String, Object?> row) => EventSpeakerDbo(row['id'] as int,
            row['speaker_id'] as int, row['event_id'] as String),
        arguments: [id]);
  }

  @override
  Future<List<EventSpeakerDbo>> getEventsSpeakersBySpeakerId(int id) async {
    return _queryAdapter.queryList(
        'SELECT * FROM event_speaker WHERE speaker_id = ?1',
        mapper: (Map<String, Object?> row) => EventSpeakerDbo(row['id'] as int,
            row['speaker_id'] as int, row['event_id'] as String),
        arguments: [id]);
  }

  @override
  Future<void> deleteEventsSpeakers() async {
    await _queryAdapter.queryNoReturn('DELETE FROM event_speaker');
  }

  @override
  Future<void> insertEventSpeaker(EventSpeakerDbo event) async {
    await _eventSpeakerDboInsertionAdapter.insert(
        event, OnConflictStrategy.abort);
  }
}

class _$SponsorDao extends SponsorDao {
  _$SponsorDao(
    this.database,
    this.changeListener,
  )   : _queryAdapter = QueryAdapter(database, changeListener),
        _sponsorDboInsertionAdapter = InsertionAdapter(
            database,
            'sponsor',
            (SponsorDbo item) => <String, Object?>{
                  'sponsor_key': item.sponsorKey,
                  'name': item.name,
                  'description_fr': item.descriptionFr,
                  'description_en': item.descriptionEn,
                  'logo': item.logo,
                  'level': item.level,
                  'url': item.url
                },
            changeListener),
        _sponsorDboUpdateAdapter = UpdateAdapter(
            database,
            'sponsor',
            ['sponsor_key'],
            (SponsorDbo item) => <String, Object?>{
                  'sponsor_key': item.sponsorKey,
                  'name': item.name,
                  'description_fr': item.descriptionFr,
                  'description_en': item.descriptionEn,
                  'logo': item.logo,
                  'level': item.level,
                  'url': item.url
                },
            changeListener);

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<SponsorDbo> _sponsorDboInsertionAdapter;

  final UpdateAdapter<SponsorDbo> _sponsorDboUpdateAdapter;

  @override
  Stream<List<SponsorDbo>> getAllSponsors() {
    return _queryAdapter.queryListStream('SELECT * FROM sponsor',
        mapper: (Map<String, Object?> row) => SponsorDbo(
            row['sponsor_key'] as String,
            row['name'] as String,
            row['description_fr'] as String,
            row['description_en'] as String,
            row['logo'] as String,
            row['level'] as String,
            row['url'] as String),
        queryableName: 'sponsor',
        isView: false);
  }

  @override
  Future<SponsorDbo?> getSponsorByKey(String key) async {
    return _queryAdapter.query('SELECT * FROM sponsor WHERE sponsor_key = ?1',
        mapper: (Map<String, Object?> row) => SponsorDbo(
            row['sponsor_key'] as String,
            row['name'] as String,
            row['description_fr'] as String,
            row['description_en'] as String,
            row['logo'] as String,
            row['level'] as String,
            row['url'] as String),
        arguments: [key]);
  }

  @override
  Future<void> deleteSponsors() async {
    await _queryAdapter.queryNoReturn('DELETE FROM sponsor');
  }

  @override
  Future<void> insertSponsor(SponsorDbo sponsorDbo) async {
    await _sponsorDboInsertionAdapter.insert(
        sponsorDbo, OnConflictStrategy.replace);
  }

  @override
  Future<void> updateSponsor(SponsorDbo sponsorDbo) async {
    await _sponsorDboUpdateAdapter.update(sponsorDbo, OnConflictStrategy.abort);
  }
}

class _$JobOfferDao extends JobOfferDao {
  _$JobOfferDao(
    this.database,
    this.changeListener,
  )   : _queryAdapter = QueryAdapter(database),
        _jobOfferDboInsertionAdapter = InsertionAdapter(
            database,
            'sponsor_job_offer',
            (JobOfferDbo item) => <String, Object?>{
                  'id': item.id,
                  'title': item.title,
                  'link': item.link,
                  'sponsor_key': item.sponsorKey
                }),
        _jobOfferDboUpdateAdapter = UpdateAdapter(
            database,
            'sponsor_job_offer',
            ['id'],
            (JobOfferDbo item) => <String, Object?>{
                  'id': item.id,
                  'title': item.title,
                  'link': item.link,
                  'sponsor_key': item.sponsorKey
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<JobOfferDbo> _jobOfferDboInsertionAdapter;

  final UpdateAdapter<JobOfferDbo> _jobOfferDboUpdateAdapter;

  @override
  Future<List<JobOfferDbo>> getAllJobOffersForSponsor(String key) async {
    return _queryAdapter.queryList(
        'SELECT * FROM sponsor_job_offer WHERE sponsor_key = ?1',
        mapper: (Map<String, Object?> row) => JobOfferDbo(
            row['id'] as int,
            row['title'] as String,
            row['link'] as String,
            row['sponsor_key'] as String),
        arguments: [key]);
  }

  @override
  Future<void> deleteJobOffers() async {
    await _queryAdapter.queryNoReturn('DELETE FROM sponsor_job_offer');
  }

  @override
  Future<void> insertJobOffer(JobOfferDbo jobOffer) async {
    await _jobOfferDboInsertionAdapter.insert(
        jobOffer, OnConflictStrategy.replace);
  }

  @override
  Future<void> updateJobOffer(JobOfferDbo jobOffer) async {
    await _jobOfferDboUpdateAdapter.update(jobOffer, OnConflictStrategy.abort);
  }
}

// ignore_for_file: unused_element
final _dateTimeConverter = DateTimeConverter();
