import 'package:floor/floor.dart';

final migration1to2 = Migration(1, 2, (database) async {
  await database.execute('''
  CREATE TABLE sponsor_job_offer (
    id INTEGER PRIMARY KEY,
    title TEXT,
    link TEXT,
    sponsor_key TEXT,
    FOREIGN KEY (sponsor_key)
       REFERENCES sponsor (sponsor_key) 
  )
  ''');
  await database.execute('''
  CREATE TABLE sponsor (
    sponsor_key TEXT PRIMARY KEY,
    name TEXT,
    description_fr TEXT,
    description_en TEXT,
    logo TEXT,
    level TEXT,
    url TEXT
  )
  ''');
});
