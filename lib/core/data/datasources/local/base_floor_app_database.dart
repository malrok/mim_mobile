import 'package:mim_mobile/core/data/datasources/local/app_database.dart';

abstract class BaseFloorAppDatabase {
  Future<AppDatabase> getDatabase();
}
