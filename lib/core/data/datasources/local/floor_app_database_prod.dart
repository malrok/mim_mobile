import 'package:injectable/injectable.dart';
import 'package:mim_mobile/core/data/datasources/local/app_database.dart';
import 'package:mim_mobile/core/data/datasources/local/base_floor_app_database.dart';
import 'package:mim_mobile/core/data/datasources/local/migration_scripts/v2.script.dart';

@Environment('prod')
@Injectable(as: BaseFloorAppDatabase)
class FloorAppDatabase extends $FloorAppDatabase implements BaseFloorAppDatabase {
  @override
  Future<AppDatabase> getDatabase() =>
      $FloorAppDatabase.databaseBuilder('app_database.db').addMigrations([migration1to2]).build();
}
