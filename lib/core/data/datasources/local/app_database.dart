import 'dart:async';

import 'package:floor/floor.dart';
import 'package:mim_mobile/core/data/datasources/local/datatime.converter.dart';
import 'package:mim_mobile/features/events/data/datasources/local/event.dao.dart';
import 'package:mim_mobile/features/events/data/datasources/local/event.dbo.dart';
import 'package:mim_mobile/features/events/data/datasources/local/event_speaker.dao.dart';
import 'package:mim_mobile/features/events/data/datasources/local/event_speaker.dbo.dart';
import 'package:mim_mobile/features/events/data/datasources/local/speaker.dao.dart';
import 'package:mim_mobile/features/events/data/datasources/local/speaker.dbo.dart';
import 'package:mim_mobile/features/sponsors/data/datasources/local/job_offer.dao.dart';
import 'package:mim_mobile/features/sponsors/data/datasources/local/job_offer.dbo.dart';
import 'package:mim_mobile/features/sponsors/data/datasources/local/sponsor.dao.dart';
import 'package:mim_mobile/features/sponsors/data/datasources/local/sponsor.dbo.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

part 'app_database.g.dart';

@TypeConverters([DateTimeConverter])
@Database(version: 2, entities: [EventDbo, SpeakerDbo, EventSpeakerDbo, SponsorDbo, JobOfferDbo])
abstract class AppDatabase extends FloorDatabase {
  EventDao get eventDao;

  SpeakerDao get speakerDao;

  EventSpeakerDao get eventSpeakerDao;

  SponsorDao get sponsorDao;

  JobOfferDao get jobOfferDao;
}
