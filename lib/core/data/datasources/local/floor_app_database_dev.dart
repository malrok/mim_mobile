import 'package:injectable/injectable.dart';
import 'package:mim_mobile/core/data/datasources/local/app_database.dart';
import 'package:mim_mobile/core/data/datasources/local/base_floor_app_database.dart';

@Environment('dev')
@Injectable(as: BaseFloorAppDatabase)
class FloorAppDatabase extends $FloorAppDatabase implements BaseFloorAppDatabase {
  @override
  Future<AppDatabase> getDatabase() => $FloorAppDatabase.inMemoryDatabaseBuilder().build();
}
