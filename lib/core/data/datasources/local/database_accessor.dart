import 'dart:async';

import 'package:injectable/injectable.dart';
import 'package:mim_mobile/core/data/datasources/local/app_database.dart';
import 'package:mim_mobile/core/data/datasources/local/base_floor_app_database.dart';
import 'package:mim_mobile/features/events/data/datasources/local/event.dao.dart';
import 'package:mim_mobile/features/events/data/datasources/local/event.dbo.dart';
import 'package:mim_mobile/features/events/data/datasources/local/event_speaker.dao.dart';
import 'package:mim_mobile/features/events/data/datasources/local/event_speaker.dbo.dart';
import 'package:mim_mobile/features/events/data/datasources/local/events_and_speakers.model.dart';
import 'package:mim_mobile/features/events/data/datasources/local/speaker.dao.dart';
import 'package:mim_mobile/features/events/data/datasources/local/speaker.dbo.dart';
import 'package:mim_mobile/features/sponsors/data/datasources/local/job_offer.dao.dart';
import 'package:mim_mobile/features/sponsors/data/datasources/local/sponsor.dao.dart';
import 'package:mim_mobile/features/sponsors/data/datasources/local/sponsor.dbo.dart';

@singleton
class DatabaseAccessor {
  late AppDatabase _database;

  late EventDao _eventDao;
  late SpeakerDao _speakerDao;
  late EventSpeakerDao _eventSpeakerDao;
  late SponsorDao _sponsorDao;
  late JobOfferDao _jobOfferDao;

  @factoryMethod
  static Future<DatabaseAccessor> create(BaseFloorAppDatabase database) async {
    final databaseAccessor = DatabaseAccessor();
    await databaseAccessor._init(database);
    return databaseAccessor;
  }

  Future<void> _init(BaseFloorAppDatabase database) async {
    _database = await database.getDatabase();
    _eventDao = _database.eventDao;
    _speakerDao = _database.speakerDao;
    _eventSpeakerDao = _database.eventSpeakerDao;
    _sponsorDao = _database.sponsorDao;
    _jobOfferDao = _database.jobOfferDao;
  }

  Future<List<EventDbo>> getEventsWithSpeakersFuture() async {
    final dboEvents = await _eventDao.getAllEventsOnce();
    final dboSpeakers = await _speakerDao.getAllSpeakers();
    final dboEventsSpeakers = await _eventSpeakerDao.getAllEventSpeakers();

    for (final dboEvent in dboEvents) {
      final dboES = dboEventsSpeakers.where((element) => element.eventId == dboEvent.id);
      for (final dboEventSpeaker in dboES) {
        dboEvent.speakers.add(dboSpeakers.firstWhere((element) => element.id == dboEventSpeaker.speakerId));
      }
    }
    return dboEvents;
  }

  Stream<List<EventDbo>> getEventsWithSpeakers() {
    late StreamController<List<EventDbo>> controller;

    StreamSubscription? subscription;

    void startListener() {
      subscription = _eventDao.getAllEvents().listen((dboEvents) async {
        final dboSpeakers = await _speakerDao.getAllSpeakers();
        final dboEventsSpeakers = await _eventSpeakerDao.getAllEventSpeakers();

        for (final dboEvent in dboEvents) {
          final dboES = dboEventsSpeakers.where((element) => element.eventId == dboEvent.id);
          for (final dboEventSpeaker in dboES) {
            dboEvent.speakers.add(dboSpeakers.firstWhere((element) => element.id == dboEventSpeaker.speakerId));
          }
        }

        controller.add(dboEvents);
      });
    }

    void stopListener() {
      subscription?.cancel();
      subscription = null;
    }

    controller = StreamController(
      onListen: startListener,
      onPause: stopListener,
      onResume: startListener,
      onCancel: stopListener,
    );

    return controller.stream;
  }

  Stream<List<EventDbo>> getFavoriteEventsWithSpeakers() {
    late StreamController<List<EventDbo>> controller;

    StreamSubscription? subscription;

    void startListener() {
      subscription = _eventDao.getAllFavoriteEvents().listen((dboEvents) async {
        final dboSpeakers = await _speakerDao.getAllSpeakers();
        final dboEventsSpeakers = await _eventSpeakerDao.getAllEventSpeakers();

        for (final dboEvent in dboEvents) {
          final dboES = dboEventsSpeakers.where((element) => element.eventId == dboEvent.id);
          for (final dboEventSpeaker in dboES) {
            dboEvent.speakers.add(dboSpeakers.firstWhere((element) => element.id == dboEventSpeaker.speakerId));
          }
        }

        controller.add(dboEvents);
      });
    }

    void stopListener() {
      subscription?.cancel();
      subscription = null;
    }

    controller = StreamController(
      onListen: startListener,
      onPause: stopListener,
      onResume: startListener,
      onCancel: stopListener,
    );

    return controller.stream;
  }

  Future<List<SpeakerDbo>> getSpeakers() => _speakerDao.getAllSpeakers();

  Future<EventDbo?> getEventWithSpeakersById(String id) async {
    final EventDbo? dbEvent = await _eventDao.getEventById(id);

    if (dbEvent != null) {
      final List<EventSpeakerDbo> dboEventSpeakers = await _eventSpeakerDao.getEventsSpeakersByEventId(id);
      final List<SpeakerDbo> dbSpeakers =
          await _speakerDao.getSpeakersByIds(dboEventSpeakers.map((e) => e.speakerId).toList());

      dbEvent.speakers.addAll(dbSpeakers);

      return dbEvent;
    }

    return null;
  }

  Future<SpeakerDbo?> getSpeakerWithEventsById(int id) async {
    final SpeakerDbo? speakerDbo = await _speakerDao.getSpeakerById(id);
    if (speakerDbo != null) {
      final List<EventSpeakerDbo> dboEventSpeakers = await _eventSpeakerDao.getEventsSpeakersBySpeakerId(id);
      final List<EventDbo> dbEvents = await _eventDao.getEventsByIds(dboEventSpeakers.map((e) => e.eventId).toList());

      speakerDbo.events.addAll(dbEvents);

      return speakerDbo;
    }
    return null;
  }

  Future<EventDbo> updateEvent(EventDbo dbo) async {
    await _eventDao.updateEvent(dbo);
    return dbo;
  }

  Future<void> insertEventsAndSpeakers(EventsAndSpeakers eventsAndSpeakers) async {
    for (var event in eventsAndSpeakers.events) {
      await _eventDao.insertEvent(event);
    }
    for (var speaker in eventsAndSpeakers.speakers) {
      await _speakerDao.insertSpeaker(speaker);
    }
    for (var eventSpeaker in eventsAndSpeakers.eventsSpeakers) {
      await _eventSpeakerDao.insertEventSpeaker(eventSpeaker);
    }
  }

  Stream<List<SponsorDbo>> getSponsors() => _sponsorDao.getAllSponsors();

  Future<SponsorDbo?> getSponsorWithJobs(String key) async {
    final sponsor = await _sponsorDao.getSponsorByKey(key);

    if (sponsor != null) {
      final jobs = await _jobOfferDao.getAllJobOffersForSponsor(key);

      sponsor.jobOffers = jobs;
    }

    return sponsor;
  }

  Future<void> insertAllSponsors(List<SponsorDbo> sponsorsDbo) async {
    /// we delete all
    await _jobOfferDao.deleteJobOffers();
    await _sponsorDao.deleteSponsors();

    int id = 0;

    /// we insert all
    for (final sponsorDbo in sponsorsDbo) {
      await _sponsorDao.insertSponsor(sponsorDbo);
      for (final jobOffer in sponsorDbo.jobOffers) {
        jobOffer.id = id++;
        await _jobOfferDao.insertJobOffer(jobOffer);
      }
    }
  }

  Future<void> dropData() async {
    await _eventSpeakerDao.deleteEventsSpeakers();
    await _speakerDao.deleteSpeakers();
    await _eventDao.deleteEvents();
    await _jobOfferDao.deleteJobOffers();
    await _sponsorDao.deleteSponsors();
  }
}
