import 'package:flutter/material.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:mim_mobile/config/di/dependency_injection.dart';
import 'package:mim_mobile/mim_app.widget.dart';

Future<void> main() async {
  configureDependencies('prod');
  WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
  runApp(const MimApp());
}
