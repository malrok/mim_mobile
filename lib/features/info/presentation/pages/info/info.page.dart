import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:mim_mobile/config/constants/colors.constants.dart';
import 'package:mim_mobile/config/constants/icons.constants.dart';
import 'package:mim_mobile/config/constants/images.constants.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';
import 'package:mim_mobile/core/domain/helpers/mailto.helper.dart';
import 'package:mim_mobile/core/domain/helpers/url.helper.dart';
import 'package:mim_mobile/core/presentation/primary_button.widget.dart';
import 'package:mim_mobile/core/presentation/secondary_button.widget.dart';
import 'package:mim_mobile/features/info/presentation/pages/info/widgets/location_display.widget.dart';
import 'package:mim_mobile/features/params/presentation/blocs/get_params.cubit.dart';
import 'package:mim_mobile/features/params/presentation/blocs/get_params.states.dart';
import 'package:mim_mobile/features/sponsors/presentation/pages/sponsors_list/sponsors_list_container.widget.dart';

class InfoPage extends StatelessWidget {
  const InfoPage({super.key});

  @override
  Widget build(BuildContext context) {
    final l10n = AppLocalizations.of(context)!;
    final textTheme = Theme.of(context).textTheme;

    return BlocBuilder<GetParamsCubit, GetParamsStates>(
      bloc: GetParamsCubit()..load(),
      builder: (context, state) {
        return SingleChildScrollView(
          child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Image.asset(MimImagePaths.illustration, width: double.infinity),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: MimSizes.medium,
                vertical: MimSizes.large,
              ),
              child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Text(l10n.infoPageTitle, style: textTheme.headlineLarge),
                Text(l10n.infoPageSubtitle, style: textTheme.titleMedium),
                const SizedBox(height: MimSizes.medium),
                _InfoDescription(
                  title: l10n.infoPageSection1Title,
                  description: l10n.infoPageSection1Subtitle,
                ),
                const SizedBox(height: MimSizes.medium),
                _InfoDescription(
                  title: l10n.infoPageSection2Title,
                  description: l10n.infoPageSection2Subtitle,
                ),
                const SizedBox(height: MimSizes.medium),
                Wrap(
                  runSpacing: MimSizes.small,
                  children: [
                    PrimaryButton(
                      label: l10n.infoPageBuyYourTicket,
                      icon: MimSvg.arrowOut,
                      onPressed: state is GetParamsStatesData && state.data.ticketShopUrl.isNotEmpty
                          ? () => openUrl(state.data.ticketShopUrl)
                          : null,
                    ),
                    const SizedBox(width: MimSizes.small),
                    SecondaryButton(
                      label: l10n.infoPageContactUs,
                      icon: MimSvg.contact,
                      onPressed: state is GetParamsStatesData && state.data.contactEmail.isNotEmpty
                          ? () => mailto(state.data.contactEmail, l10n.contactUsSubject)
                          : null,
                    )
                  ],
                ),
                const SizedBox(height: MimSizes.medium),
                const SponsorsListContainer(),
                const SizedBox(height: MimSizes.medium),
                LocationDisplay(params: state is GetParamsStatesData ? state.data : null)
              ]),
            )
          ]),
        );
      },
    );
  }
}

class _InfoDescription extends StatelessWidget {
  final String title;
  final String description;

  const _InfoDescription({required this.title, required this.description});

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return RichText(
      text: TextSpan(style: textTheme.bodyMedium!.copyWith(color: MimColors.greyDark), children: [
        TextSpan(
          text: '👉 $title',
          style: TextStyle(
            fontWeight: FontWeight.w600,
            color: MimColors.primary,
          ),
        ),
        TextSpan(text: description)
      ]),
    );
  }
}
