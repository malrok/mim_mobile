import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:maps_launcher/maps_launcher.dart';
import 'package:mim_mobile/config/constants/colors.constants.dart';
import 'package:mim_mobile/config/constants/images.constants.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';
import 'package:mim_mobile/core/presentation/loading_display.widget.dart';
import 'package:mim_mobile/core/presentation/white_card.widget.dart';
import 'package:mim_mobile/features/params/domain/entities/params.entity.dart';

class LocationDisplay extends StatelessWidget {
  final Params? params;

  const LocationDisplay({super.key, this.params});

  @override
  Widget build(BuildContext context) {
    final l10n = AppLocalizations.of(context)!;
    final textTheme = Theme.of(context).textTheme;
    final defaultText = textTheme.bodyMedium!.copyWith(color: MimColors.greyDark);

    if (params == null) {
      return const LoadingDisplay();
    }

    if (params!.locationLatitude == 0 && params!.locationLongitude == 0) {
      return const SizedBox();
    }

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: MimSizes.medium),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(l10n.infoLocationTitle, style: textTheme.headlineLarge),
          const SizedBox(height: MimSizes.medium),
          WhiteCard(
            child: SizedBox(width: double.infinity, child: MimImages.localization),
            onTap: () async {
              /// on verifie que le device sait ouvrir des coordonnées dans une app
              MapsLauncher.launchCoordinates(
                params!.locationLatitude,
                params!.locationLongitude,
                params!.locationName,
              );
            },
          ),
          const SizedBox(height: MimSizes.medium),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(params!.locationName,
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: MimColors.primary,
                  )),
              Text(params!.locationAddress1, style: defaultText),
              Text(params!.locationAddress2, style: defaultText)
            ],
          ),
        ],
      ),
    );
  }
}
