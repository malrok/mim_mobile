import 'package:injectable/injectable.dart';
import 'package:mim_mobile/core/data/datasources/local/database_accessor.dart';
import 'package:mim_mobile/features/events/data/datasources/local/speaker.dbo.dart';
import 'package:mim_mobile/features/speakers/domain/repositories/ispeakers.repository.dart';

@Injectable(as: ISpeakersRepository)
class SpeakersRepository implements ISpeakersRepository {
  final DatabaseAccessor _dbAccessor;

  SpeakersRepository(this._dbAccessor);

  @override
  Future<List<SpeakerDbo>> getSpeakers() => _dbAccessor.getSpeakers();

  @override
  Future<SpeakerDbo?> getSpeakerWithEventsById(int id) => _dbAccessor.getSpeakerWithEventsById(id);
}
