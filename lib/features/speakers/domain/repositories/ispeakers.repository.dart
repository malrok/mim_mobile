import 'package:mim_mobile/features/events/data/datasources/local/speaker.dbo.dart';

abstract class ISpeakersRepository {
  Future<List<SpeakerDbo>> getSpeakers();

  Future<SpeakerDbo?> getSpeakerWithEventsById(int id);
}
