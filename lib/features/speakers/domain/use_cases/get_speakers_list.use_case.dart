import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/core/domain/exceptions/no_speakers.exception.dart';
import 'package:mim_mobile/features/events/data/datasources/local/speaker.dbo.dart';
import 'package:mim_mobile/features/events/domain/entities/speaker.entity.dart';
import 'package:mim_mobile/features/events/domain/transformers/speaker.transformer.dart';
import 'package:mim_mobile/features/speakers/domain/repositories/ispeakers.repository.dart';

@injectable
class SpeakersListUseCase {
  ISpeakersRepository repository;

  SpeakersListUseCase(this.repository);

  Future<Either<AppError, List<Speaker>>> execute() async {
    try {
      List<SpeakerDbo> result = await repository.getSpeakers();

      if (result.isEmpty) {
        return Left(AppError(NoSpeakersException()));
      }

      return Right(result.map((SpeakerDbo db) => speakerFromSpeakerDbo(db)).toList());
    } catch (error) {
      return Left(AppError(error));
    }
  }
}
