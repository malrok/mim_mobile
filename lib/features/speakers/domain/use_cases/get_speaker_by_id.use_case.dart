import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/features/events/data/datasources/local/speaker.dbo.dart';
import 'package:mim_mobile/features/events/domain/entities/speaker.entity.dart';
import 'package:mim_mobile/features/events/domain/transformers/speaker.transformer.dart';
import 'package:mim_mobile/features/speakers/domain/repositories/ispeakers.repository.dart';

@injectable
class SpeakerUseCase {
  ISpeakersRepository repository;

  SpeakerUseCase(this.repository);

  Future<Either<AppError, Speaker?>> execute(int id) async {
    try {
      SpeakerDbo? result = await repository.getSpeakerWithEventsById(id);

      return Right(result == null ? null : speakerFromSpeakerDbo(result));
    } catch (error) {
      return Left(AppError(error));
    }
  }
}
