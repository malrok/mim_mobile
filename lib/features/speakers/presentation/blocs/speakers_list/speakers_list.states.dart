import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/features/events/domain/entities/speaker.entity.dart';

sealed class SpeakersListStates {}

final class SpeakersListStatesData extends SpeakersListStates {
  final List<Speaker> data;

  SpeakersListStatesData(this.data);
}

final class SpeakersListStatesLoading extends SpeakersListStates {}

final class SpeakersListStatesError extends SpeakersListStates {
  final AppError error;

  SpeakersListStatesError(this.error);
}
