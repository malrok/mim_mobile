import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mim_mobile/config/di/dependency_injection.dart';
import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/features/events/domain/entities/speaker.entity.dart';
import 'package:mim_mobile/features/speakers/domain/use_cases/get_speakers_list.use_case.dart';
import 'package:mim_mobile/features/speakers/presentation/blocs/speakers_list/speakers_list.states.dart';

class SpeakersListCubit extends Cubit<SpeakersListStates> {
  late SpeakersListUseCase _useCase;

  final Completer<void> _isReady = Completer();

  SpeakersListCubit() : super(SpeakersListStatesLoading()) {
    getIt.getAsync<SpeakersListUseCase>().then((useCase) {
      _useCase = useCase;
      _isReady.complete();
    });
  }

  Future<void> load() async {
    await _isReady.future;
    emit(SpeakersListStatesLoading());
    (await _useCase.execute()).fold(
      (AppError appError) => emit(SpeakersListStatesError(appError)),
      (List<Speaker> speakers) => emit(SpeakersListStatesData(speakers)),
    );
  }
}
