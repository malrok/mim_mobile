import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/features/events/domain/entities/speaker.entity.dart';

sealed class SpeakerStates {}

final class SpeakerStatesData extends SpeakerStates {
  final Speaker data;

  SpeakerStatesData(this.data);
}

final class SpeakerStatesLoading extends SpeakerStates {}

final class SpeakerStatesError extends SpeakerStates {
  final AppError error;

  SpeakerStatesError(this.error);
}
