import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mim_mobile/config/di/dependency_injection.dart';
import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/core/domain/exceptions/no_speakers.exception.dart';
import 'package:mim_mobile/features/events/domain/entities/speaker.entity.dart';
import 'package:mim_mobile/features/speakers/domain/use_cases/get_speaker_by_id.use_case.dart';
import 'package:mim_mobile/features/speakers/presentation/blocs/speaker/speaker.states.dart';

class SpeakerCubit extends Cubit<SpeakerStates> {
  late SpeakerUseCase _useCase;

  final Completer<void> _isReady = Completer();

  SpeakerCubit() : super(SpeakerStatesLoading()) {
    getIt.getAsync<SpeakerUseCase>().then((useCase) {
      _useCase = useCase;
      _isReady.complete();
    });
  }

  Future<void> load(int id) async {
    await _isReady.future;
    emit(SpeakerStatesLoading());
    (await _useCase.execute(id)).fold(
      (AppError appError) => emit(SpeakerStatesError(appError)),
      (Speaker? speaker) {
        if (speaker == null) {
          emit(SpeakerStatesError(AppError(NoSpeakersException())));
        } else {
          emit(SpeakerStatesData(speaker));
        }
      },
    );
  }
}
