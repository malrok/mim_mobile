import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';
import 'package:mim_mobile/config/constants/colors.constants.dart';
import 'package:mim_mobile/config/constants/decorations.constants.dart';
import 'package:mim_mobile/config/constants/icons.constants.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';
import 'package:mim_mobile/core/presentation/app_text_field.widget.dart';
import 'package:mim_mobile/core/presentation/empty_list.widget.dart';
import 'package:mim_mobile/core/presentation/error_display.widget.dart';
import 'package:mim_mobile/core/presentation/loading_display.widget.dart';
import 'package:mim_mobile/core/presentation/speaker_card.widget.dart';
import 'package:mim_mobile/features/events/domain/entities/speaker.entity.dart';
import 'package:mim_mobile/features/speakers/presentation/blocs/speakers_list/speakers_list.cubit.dart';
import 'package:mim_mobile/features/speakers/presentation/blocs/speakers_list/speakers_list.states.dart';

class SpeakersList extends StatelessWidget {
  const SpeakersList({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SpeakersListCubit, SpeakersListStates>(
      builder: (context, state) => switch (state) {
        SpeakersListStatesData() => _SearchableList(speakers: state.data),
        SpeakersListStatesLoading() => const LoadingDisplay(),
        SpeakersListStatesError() => ErrorDisplay(
            error: state.error,
            onRetry: () => context.read<SpeakersListCubit>().load(),
          ),
      },
    );
  }
}

class _SearchableList extends StatefulWidget {
  final List<Speaker> speakers;

  const _SearchableList({
    required this.speakers,
  });

  @override
  State<_SearchableList> createState() => _SearchableListState();
}

class _SearchableListState extends State<_SearchableList> {
  String _filter = '';

  @override
  Widget build(BuildContext context) {
    final l10n = AppLocalizations.of(context)!;
    return SingleChildScrollView(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(MimSizes.medium),
            child: AppTextField(
              hintText: l10n.speakerSearchHint,
              onTextChanged: (str) => setState(() => _filter = str),
              suffixIcon: SvgPicture.asset(
                MimSvg.search,
                colorFilter: ColorFilter.mode(MimColors.black, BlendMode.srcIn),
              ),
            ),
          ),
          _ListOfSpeakers(speakers: widget.speakers.where((element) => element.name.toLowerCase().contains(_filter))),
        ],
      ),
    );
  }
}

class _ListOfSpeakers extends StatelessWidget {
  final Iterable<Speaker> speakers;

  const _ListOfSpeakers({
    required this.speakers,
  });

  @override
  Widget build(BuildContext context) {
    final l10n = AppLocalizations.of(context)!;
    if (speakers.isEmpty) {
      return EmptyList(text: l10n.noSpeakersForQuery);
    } else {
      return Column(
        children: [
          ...speakers.map((speaker) => Padding(
                padding: const EdgeInsets.symmetric(horizontal: MimSizes.medium),
                child: Container(
                  padding: const EdgeInsets.symmetric(vertical: MimSizes.small),
                  decoration: MimDecorations.dottedBottomLine,
                  child: SpeakerCard(
                    speaker: speaker,
                    hasArrow: true,
                    onTap: () => context.push('/speaker_detail', extra: speaker.id),
                  ),
                ),
              ))
        ],
      );
    }
  }
}
