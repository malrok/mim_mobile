import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mim_mobile/features/speakers/presentation/blocs/speakers_list/speakers_list.cubit.dart';
import 'package:mim_mobile/features/speakers/presentation/pages/speakers_list/widgets/speakers_list.widget.dart';

class SpeakersListPage extends StatelessWidget {
  const SpeakersListPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => SpeakersListCubit()..load(),
      child: const SpeakersList(),
    );
  }
}
