import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mim_mobile/core/presentation/loading_display.widget.dart';
import 'package:mim_mobile/features/speakers/presentation/blocs/speaker/speaker.cubit.dart';
import 'package:mim_mobile/features/speakers/presentation/pages/speaker_detail/widgets/speaker_detail.widget.dart';

class SpeakerDetailPage extends StatefulWidget {
  final int? id;

  const SpeakerDetailPage({super.key, this.id});

  @override
  State<SpeakerDetailPage> createState() => _SpeakerDetailPageState();
}

class _SpeakerDetailPageState extends State<SpeakerDetailPage> {
  @override
  Widget build(BuildContext context) {
    if (widget.id == null) {
      return const LoadingDisplay();
    } else {
      return BlocProvider(
        create: (_) => SpeakerCubit()..load(widget.id!),
        child: SpeakerDetail(id: widget.id!),
      );
    }
  }
}
