import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mim_mobile/config/constants/decorations.constants.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';
import 'package:mim_mobile/core/presentation/error_display.widget.dart';
import 'package:mim_mobile/core/presentation/formatted_text.widget.dart';
import 'package:mim_mobile/core/presentation/loading_display.widget.dart';
import 'package:mim_mobile/core/presentation/mim_app_bar.widget.dart';
import 'package:mim_mobile/core/presentation/speaker_avatar.widget.dart';
import 'package:mim_mobile/core/presentation/speaker_infos.widget.dart';
import 'package:mim_mobile/features/events/domain/entities/speaker.entity.dart';
import 'package:mim_mobile/features/speakers/presentation/blocs/speaker/speaker.cubit.dart';
import 'package:mim_mobile/features/speakers/presentation/blocs/speaker/speaker.states.dart';
import 'package:mim_mobile/features/speakers/presentation/pages/speaker_detail/widgets/speaker_event.widget.dart';
import 'package:mim_mobile/features/speakers/presentation/pages/speaker_detail/widgets/speaker_networks.widget.dart';

class SpeakerDetail extends StatelessWidget {
  final int id;

  const SpeakerDetail({super.key, required this.id});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SpeakerCubit, SpeakerStates>(
      builder: (context, state) => switch (state) {
        SpeakerStatesData() => _DetailOfSpeaker(speaker: state.data),
        SpeakerStatesLoading() => const LoadingDisplay(),
        SpeakerStatesError() => ErrorDisplay(
            error: state.error,
            onRetry: () => context.read<SpeakerCubit>().load(id),
          ),
      },
    );
  }
}

class _DetailOfSpeaker extends StatelessWidget {
  final Speaker speaker;

  const _DetailOfSpeaker({required this.speaker});

  @override
  Widget build(BuildContext context) {
    const mediumSize = MimSizes.medium;

    return Scaffold(
      appBar: const MimAppBar(isMainNav: false),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: MimSizes.medium,
            vertical: MimSizes.large,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SpeakerAvatar(
                speaker: speaker,
                size: SpeakerAvatarSize.large,
              ),
              const SizedBox(height: MimSizes.regular),
              SpeakerInfo(
                speaker: speaker,
                size: SpeakerInfoSize.big,
              ),
              const SizedBox(height: mediumSize),
              if (speaker.bio.content.isNotEmpty) ...[
                FormattedText(value: speaker.bio),
                const SizedBox(height: mediumSize),
              ],
              SpeakerNetworks(
                githubUrl: speaker.githubUrl,
                twitterUrl: speaker.twitterUrl,
              ),
              const SizedBox(height: mediumSize),
              Container(width: double.infinity, decoration: MimDecorations.dottedTopLine),
              ...speaker.events!.map((event) => SpeakerEvent(event: event)),
            ],
          ),
        ),
      ),
    );
  }
}
