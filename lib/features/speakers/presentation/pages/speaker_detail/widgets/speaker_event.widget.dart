import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:mim_mobile/config/constants/colors.constants.dart';
import 'package:mim_mobile/config/constants/decorations.constants.dart';
import 'package:mim_mobile/config/constants/icons.constants.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';
import 'package:mim_mobile/config/constants/theme.constants.dart';
import 'package:mim_mobile/core/presentation/app_tag.widget.dart';
import 'package:mim_mobile/core/presentation/event_duration.widget.dart';
import 'package:mim_mobile/core/presentation/favorite_toggle.widget.dart';
import 'package:mim_mobile/core/presentation/white_card.widget.dart';
import 'package:mim_mobile/features/events/domain/entities/event.entity.dart';
import 'package:mim_mobile/features/events/domain/value_objects/talk_type.value_object.dart';
import 'package:mim_mobile/features/events/presentation/blocs/favorite/favorite.cubit.dart';

class SpeakerEvent extends StatelessWidget {
  final Event event;

  const SpeakerEvent({
    super.key,
    required this.event,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: MimSizes.medium),
      child: BlocProvider<FavoriteCubit>(
        create: (_) => FavoriteCubit()..load(event.id),
        child: InkWell(
          onTap: () {
            if (event.type == TalkType.talk) {
              context.push('/event_detail', extra: event.id);
            }
          },
          child: WhiteCard(
            onTap: null,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: MimSizes.xsmall),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Text(
                        event.label,
                        softWrap: true,
                        style: textTheme.headlineMedium!.copyWith(color: MimColors.black),
                      ),
                    ),
                    FavoriteToggle(event: event),
                  ],
                ),
                const SizedBox(height: MimSizes.regular),
                FittedBox(
                    child: AppTag(
                  label: event.category.desc,
                  icon: event.category.icon,
                  size: AppTagSize.big,
                )),
                const SizedBox(height: MimSizes.regular),
                Text(
                  event.description.content,
                  softWrap: true,
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                  style: textTheme.titleMedium!.copyWith(color: MimColors.greyDark),
                ),
                const SizedBox(height: MimSizes.regular),
                Container(width: double.infinity, decoration: MimDecorations.dottedTopLine),
                const SizedBox(height: MimSizes.regular),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  textBaseline: TextBaseline.alphabetic,
                  children: [
                    const Center(
                      child: Image(
                        image: MimIcons.location,
                        width: MimSizes.medium,
                        height: MimSizes.medium,
                      ),
                    ),
                    const SizedBox(width: MimSizes.xxsmall),
                    Text(
                      event.track,
                      style: textTheme.labelMedium!.copyWith(color: MimColors.primary),
                    ),
                    const Spacer(),
                    EventDuration(
                      startDate: event.startDate,
                      endDate: event.endDate,
                      direction: EventDurationDirection.horizontal,
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
