import 'package:flutter/material.dart';
import 'package:mim_mobile/config/constants/icons.constants.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';
import 'package:mim_mobile/core/domain/helpers/url.helper.dart';
import 'package:mim_mobile/core/presentation/secondary_icon_button.widget.dart';
import 'package:url_launcher/url_launcher_string.dart';

class SpeakerNetworks extends StatelessWidget {
  final String githubUrl;
  final String twitterUrl;

  const SpeakerNetworks({
    super.key,
    required this.githubUrl,
    required this.twitterUrl,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        if (githubUrl.isNotEmpty)
          _SocialNetworkBadge(
            svg: MimSvg.github,
            url: getGithubUrl(githubUrl),
          ),
        if (githubUrl.isNotEmpty && twitterUrl.isNotEmpty) const SizedBox(width: MimSizes.xsmall),
        if (twitterUrl.isNotEmpty)
          _SocialNetworkBadge(
            svg: MimSvg.twitter,
            url: getTwitterUrl(twitterUrl),
          )
      ],
    );
  }
}

class _SocialNetworkBadge extends StatelessWidget {
  final String url;
  final String svg;

  const _SocialNetworkBadge({
    required this.url,
    required this.svg,
  });

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<bool>(
      future: canLaunchUrlString(url),
      builder: (context, snapshot) {
        if (snapshot.hasData && snapshot.data == true) {
          return SecondaryIconButton(
            onPressed: () => launchUrlString(url),
            svg: svg,
          );
        }
        return const SizedBox();
      },
    );
  }
}
