import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mim_mobile/config/di/dependency_injection.dart';
import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/core/domain/exceptions/no_events.exception.dart';
import 'package:mim_mobile/features/sponsors/domain/entities/sponsor.entity.dart';
import 'package:mim_mobile/features/sponsors/domain/use_cases/get_sponsor_details.use_case.dart';
import 'package:mim_mobile/features/sponsors/presentation/blocs/get_sponsor_details.states.dart';

class GetSponsorDetailsCubit extends Cubit<GetSponsorDetailsStates> {
  late GetSponsorDetailsUseCase _getSponsorDetailUseCase;

  final Completer<void> _isReady = Completer();

  GetSponsorDetailsCubit() : super(GetSponsorDetailsStatesLoading()) {
    getIt.getAsync<GetSponsorDetailsUseCase>().then((useCase) {
      _getSponsorDetailUseCase = useCase;
      _isReady.complete();
    });
  }

  void load(String id) async {
    await _isReady.future;
    emit(GetSponsorDetailsStatesLoading());
    (await _getSponsorDetailUseCase.execute(id)).fold(
      (AppError appError) => emit(GetSponsorDetailsStatesError(appError)),
      (Sponsor? sponsor) {
        if (sponsor == null) {
          emit(GetSponsorDetailsStatesError(AppError(NoEventsException())));
        } else {
          emit(GetSponsorDetailsStatesData(sponsor));
        }
      },
    );
  }
}
