import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mim_mobile/config/di/dependency_injection.dart';
import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/features/sponsors/domain/entities/sponsor.entity.dart';
import 'package:mim_mobile/features/sponsors/domain/use_cases/get_sponsors_list.use_case.dart';
import 'package:mim_mobile/features/sponsors/presentation/blocs/get_sponsors_list.states.dart';

class GetSponsorsListCubit extends Cubit<GetSponsorsListStates> {
  late GetSponsorsListUseCase _getSponsorsListUseCase;

  final Completer<void> _isReady = Completer();

  StreamSubscription? _subscription;

  GetSponsorsListCubit() : super(GetSponsorsListStatesLoading()) {
    getIt.getAsync<GetSponsorsListUseCase>().then((useCase) {
      _getSponsorsListUseCase = useCase;
      _isReady.complete();
    });
  }

  @override
  Future<void> close() {
    _subscription?.cancel();
    _subscription = null;
    return super.close();
  }

  void load() async {
    await _isReady.future;
    if (isClosed) return;
    emit(GetSponsorsListStatesLoading());
    _subscription = _getSponsorsListUseCase.execute().listen((result) => result.fold(
          (AppError appError) => emit(GetSponsorsListStatesError(appError)),
          (List<Sponsor> sponsors) => emit(GetSponsorsListStatesData(sponsors)),
        ));
  }
}
