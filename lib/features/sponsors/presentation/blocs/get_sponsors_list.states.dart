import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/features/sponsors/domain/entities/sponsor.entity.dart';

sealed class GetSponsorsListStates {}

final class GetSponsorsListStatesData extends GetSponsorsListStates {
  final List<Sponsor> data;

  GetSponsorsListStatesData(this.data);
}

final class GetSponsorsListStatesLoading extends GetSponsorsListStates {}

final class GetSponsorsListStatesError extends GetSponsorsListStates {
  final AppError error;

  GetSponsorsListStatesError(this.error);
}
