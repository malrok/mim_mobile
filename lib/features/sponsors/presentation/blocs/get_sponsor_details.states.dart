import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/features/sponsors/domain/entities/sponsor.entity.dart';

sealed class GetSponsorDetailsStates {}

final class GetSponsorDetailsStatesData extends GetSponsorDetailsStates {
  final Sponsor data;

  GetSponsorDetailsStatesData(this.data);
}

final class GetSponsorDetailsStatesLoading extends GetSponsorDetailsStates {}

final class GetSponsorDetailsStatesError extends GetSponsorDetailsStates {
  final AppError error;

  GetSponsorDetailsStatesError(this.error);
}
