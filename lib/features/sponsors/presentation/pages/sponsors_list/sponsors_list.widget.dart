import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:go_router/go_router.dart';
import 'package:mim_mobile/config/constants/colors.constants.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';
import 'package:mim_mobile/config/constants/theme.constants.dart';
import 'package:mim_mobile/core/presentation/white_card.widget.dart';
import 'package:mim_mobile/features/sponsors/domain/entities/sponsor.entity.dart';
import 'package:mim_mobile/features/sponsors/domain/value_objects/sponsor_level.value_object.dart';

class SponsorsList extends StatefulWidget {
  final List<Sponsor> sponsors;

  const SponsorsList({super.key, required this.sponsors});

  @override
  State<SponsorsList> createState() => _SponsorsListState();
}

class _SponsorsListState extends State<SponsorsList> {
  late List<Sponsor> platinium;
  late List<Sponsor> gold;
  late List<Sponsor> silver;

  @override
  void didChangeDependencies() {
    platinium = widget.sponsors.where((sponsor) => sponsor.level == SponsorLevel.platinium).toList();
    gold = widget.sponsors.where((sponsor) => sponsor.level == SponsorLevel.gold).toList();
    silver = widget.sponsors.where((sponsor) => sponsor.level == SponsorLevel.silver).toList();
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final l10n = AppLocalizations.of(context)!;

    if (platinium.isEmpty && gold.isEmpty && silver.isEmpty) {
      return const SizedBox();
    }

    return Padding(
      padding: const EdgeInsets.only(top: MimSizes.medium),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(l10n.sponsors, style: textTheme.headlineLarge),
          const SizedBox(height: MimSizes.small),
          if (platinium.isNotEmpty) ...[
            Text(l10n.platinium,
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  color: MimColors.primary,
                )),
            const SizedBox(height: MimSizes.small),
            ...platinium.map((p) => _Sponsor(sponsor: p)),
            const SizedBox(height: MimSizes.small),
          ],
          if (gold.isNotEmpty) ...[
            Text(l10n.gold,
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  color: MimColors.primary,
                )),
            const SizedBox(height: MimSizes.small),
            ...gold.map((p) => _Sponsor(sponsor: p)),
            const SizedBox(height: MimSizes.small),
          ],
          if (silver.isNotEmpty) ...[
            Text(l10n.silver,
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  color: MimColors.primary,
                )),
            const SizedBox(height: MimSizes.small),
            ...silver.map((p) => _Sponsor(sponsor: p)),
          ]
        ],
      ),
    );
  }
}

class _Sponsor extends StatelessWidget {
  final Sponsor sponsor;

  const _Sponsor({required this.sponsor});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: MimSizes.xsmall),
      child: WhiteCard(
        onTap: () => context.push('/sponsor_detail', extra: sponsor.key),
        child: SizedBox(
          width: double.infinity,
          height: MimSizes.hugeLarge,
          child: sponsor.logo.isNotEmpty
              ? Image.network(sponsor.logo, height: MimSizes.hugeLarge)
              : Center(child: Text(sponsor.name)),
        ),
      ),
    );
  }
}
