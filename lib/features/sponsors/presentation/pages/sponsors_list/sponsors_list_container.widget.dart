import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mim_mobile/core/presentation/error_display.widget.dart';
import 'package:mim_mobile/core/presentation/loading_display.widget.dart';
import 'package:mim_mobile/features/sponsors/presentation/blocs/get_sponsors_list.cubit.dart';
import 'package:mim_mobile/features/sponsors/presentation/blocs/get_sponsors_list.states.dart';
import 'package:mim_mobile/features/sponsors/presentation/pages/sponsors_list/sponsors_list.widget.dart';

class SponsorsListContainer extends StatelessWidget {
  const SponsorsListContainer({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GetSponsorsListCubit, GetSponsorsListStates>(
      bloc: GetSponsorsListCubit()..load(),
      builder: (context, state) => switch (state) {
        GetSponsorsListStatesData() => SponsorsList(sponsors: state.data),
        GetSponsorsListStatesLoading() => const LoadingDisplay(),
        GetSponsorsListStatesError() => ErrorDisplay(error: state.error),
      },
    );
  }
}
