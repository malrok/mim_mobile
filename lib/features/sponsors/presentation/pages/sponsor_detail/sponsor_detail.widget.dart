import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:mim_mobile/config/constants/decorations.constants.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';
import 'package:mim_mobile/core/domain/helpers/url.helper.dart';
import 'package:mim_mobile/core/presentation/secondary_button.widget.dart';
import 'package:mim_mobile/core/presentation/secondary_icon_button.widget.dart';
import 'package:mim_mobile/features/sponsors/domain/entities/sponsor.entity.dart';

class SponsorDetail extends StatelessWidget {
  final Sponsor sponsor;

  const SponsorDetail({super.key, required this.sponsor});

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final l10n = AppLocalizations.of(context)!;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          width: double.infinity,
          height: MimSizes.hugeLarge,
          child: sponsor.logo.isNotEmpty
              ? Image.network(sponsor.logo, height: MimSizes.hugeLarge)
              : Center(
                  child: Text(sponsor.name),
                ),
        ),
        SizedBox(height: MimSizes.regular),
        Text(AppLocalizations.of(context)!.localeName == 'fr' ? sponsor.descriptionFr : sponsor.descriptionEn,
            style: textTheme.bodyLarge),
        SizedBox(height: MimSizes.regular),
        SecondaryIconButton(
          onPressed: () => openUrl(sponsor.url),
          icon: Icons.language,
        ),
        sponsor.jobOffers.isNotEmpty
            ? Column(
                children: [
                  SizedBox(height: MimSizes.regular),
                  Container(width: double.infinity, decoration: MimDecorations.dottedTopLine),
                  SizedBox(height: MimSizes.regular),
                  Text(l10n.jobOffers, style: textTheme.headlineLarge),
                  SizedBox(height: MimSizes.regular),
                  ...sponsor.jobOffers.map((jobOffer) {
                    return Column(
                      children: [
                        SecondaryButton(
                          label: jobOffer.title,
                          onPressed: () => openUrl(jobOffer.link),
                        ),
                        SizedBox(height: MimSizes.regular),
                      ],
                    );
                  })
                ],
              )
            : const SizedBox(),
      ],
    );
  }
}
