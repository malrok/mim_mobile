import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';
import 'package:mim_mobile/core/presentation/error_display.widget.dart';
import 'package:mim_mobile/core/presentation/loading_display.widget.dart';
import 'package:mim_mobile/core/presentation/mim_app_bar.widget.dart';
import 'package:mim_mobile/features/sponsors/presentation/blocs/get_sponsor_details.cubit.dart';
import 'package:mim_mobile/features/sponsors/presentation/blocs/get_sponsor_details.states.dart';
import 'package:mim_mobile/features/sponsors/presentation/pages/sponsor_detail/sponsor_detail.widget.dart';

class SponsorDetailPage extends StatefulWidget {
  final String sponsorKey;

  const SponsorDetailPage({super.key, required this.sponsorKey});

  @override
  State<SponsorDetailPage> createState() => _SponsorDetailPageState();
}

class _SponsorDetailPageState extends State<SponsorDetailPage> {
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: const MimAppBar(isMainNav: false),
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: MimSizes.medium,
              vertical: MimSizes.large,
            ),
            child: BlocBuilder<GetSponsorDetailsCubit, GetSponsorDetailsStates>(
              bloc: GetSponsorDetailsCubit()..load(widget.sponsorKey),
              builder: (context, state) => switch (state) {
                GetSponsorDetailsStatesData() => SponsorDetail(sponsor: state.data),
                GetSponsorDetailsStatesLoading() => const LoadingDisplay(),
                GetSponsorDetailsStatesError() => ErrorDisplay(error: state.error),
              },
            ),
          ),
        ),
      );
}
