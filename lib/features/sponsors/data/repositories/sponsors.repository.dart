import 'dart:async';

import 'package:injectable/injectable.dart';
import 'package:mim_mobile/core/data/datasources/local/database_accessor.dart';
import 'package:mim_mobile/features/sponsors/data/datasources/local/sponsor.dbo.dart';
import 'package:mim_mobile/features/sponsors/data/datasources/remote/sponsors.remote_datasource.dart';
import 'package:mim_mobile/features/sponsors/data/datasources/transformers/sponsor.transformer.dart';
import 'package:mim_mobile/features/sponsors/domain/repositories/isponsors.repository.dart';

@Singleton(as: ISponsorsRepository)
class SponsorsRepository implements ISponsorsRepository {
  final DatabaseAccessor _dbAccessor;
  final SponsorsRemoteDatasource _datasource;

  SponsorsRepository(this._datasource, this._dbAccessor);

  @override
  Stream<List<SponsorDbo>> getSponsors() => _dbAccessor.getSponsors();

  @override
  Future<SponsorDbo?> getSponsorByKey(String key) async => _dbAccessor.getSponsorWithJobs(key);

  @override
  Future<void> refreshLocalWithRemote() async {
    final sponsorsDto = await _datasource.query();

    final sponsorsDbo = sponsorsDto.map((dto) => sponsorDboFromSponsorDto(dto)).toList();

    await _dbAccessor.insertAllSponsors(sponsorsDbo);
  }
}
