import 'package:floor/floor.dart';
import 'package:mim_mobile/features/sponsors/data/datasources/local/job_offer.dbo.dart';

@Entity(
  tableName: 'sponsor',
)
class SponsorDbo {
  /// database fields
  @primaryKey
  @ColumnInfo(name: 'sponsor_key')
  final String sponsorKey;
  final String name;
  @ColumnInfo(name: 'description_fr')
  final String descriptionFr;
  @ColumnInfo(name: 'description_en')
  final String descriptionEn;
  final String logo;
  final String level;
  final String url;

  /// ignored fields
  @ignore
  List<JobOfferDbo> jobOffers = [];

  SponsorDbo(
    this.sponsorKey,
    this.name,
    this.descriptionFr,
    this.descriptionEn,
    this.logo,
    this.level,
    this.url,
  );
}
