import 'package:floor/floor.dart';
import 'package:mim_mobile/features/sponsors/data/datasources/local/sponsor.dbo.dart';

@Entity(
  tableName: 'sponsor_job_offer',
  foreignKeys: [
    ForeignKey(
      childColumns: ['sponsor_key'],
      parentColumns: ['sponsor_key'],
      entity: SponsorDbo,
    ),
  ],
)
class JobOfferDbo {
  @primaryKey
  int id;
  String title;
  String link;
  @ColumnInfo(name: 'sponsor_key')
  String sponsorKey;

  JobOfferDbo(
    this.id,
    this.title,
    this.link,
    this.sponsorKey,
  );
}
