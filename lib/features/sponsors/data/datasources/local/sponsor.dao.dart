import 'package:floor/floor.dart';
import 'package:mim_mobile/features/sponsors/data/datasources/local/sponsor.dbo.dart';

@dao
abstract class SponsorDao {
  @Query('SELECT * FROM sponsor')
  Stream<List<SponsorDbo>> getAllSponsors();

  @Query('SELECT * FROM sponsor WHERE sponsor_key = :key')
  Future<SponsorDbo?> getSponsorByKey(String key);

  @update
  Future<void> updateSponsor(SponsorDbo sponsorDbo);

  @Insert(onConflict: OnConflictStrategy.replace)
  Future<void> insertSponsor(SponsorDbo sponsorDbo);

  @Query('DELETE FROM sponsor')
  Future<void> deleteSponsors();
}
