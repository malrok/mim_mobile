import 'package:floor/floor.dart';
import 'package:mim_mobile/features/sponsors/data/datasources/local/job_offer.dbo.dart';

@dao
abstract class JobOfferDao {
  @Query('SELECT * FROM sponsor_job_offer WHERE sponsor_key = :key')
  Future<List<JobOfferDbo>> getAllJobOffersForSponsor(String key);

  @update
  Future<void> updateJobOffer(JobOfferDbo jobOffer);

  @Insert(onConflict: OnConflictStrategy.replace)
  Future<void> insertJobOffer(JobOfferDbo jobOffer);

  @Query('DELETE FROM sponsor_job_offer')
  Future<void> deleteJobOffers();
}
