// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'job_offer.dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JobOfferDto _$JobOfferDtoFromJson(Map<String, dynamic> json) => JobOfferDto(
      title: json['title'] as String?,
      link: json['link'] as String?,
    );

Map<String, dynamic> _$JobOfferDtoToJson(JobOfferDto instance) =>
    <String, dynamic>{
      'title': instance.title,
      'link': instance.link,
    };
