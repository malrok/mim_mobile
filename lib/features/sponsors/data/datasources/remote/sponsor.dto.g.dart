// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sponsor.dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SponsorDto _$SponsorDtoFromJson(Map<String, dynamic> json) => SponsorDto(
      key: json['key'] as String?,
      name: json['name'] as String?,
      descriptionFr: json['description_fr'] as String?,
      descriptionEn: json['description_en'] as String?,
      logo: json['logo'] as String?,
      level: json['level'] as String?,
      url: json['url'] as String?,
      jobOffers: (json['jobOffers'] as List<dynamic>?)
          ?.map((e) => JobOfferDto.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$SponsorDtoToJson(SponsorDto instance) =>
    <String, dynamic>{
      'key': instance.key,
      'name': instance.name,
      'description_fr': instance.descriptionFr,
      'description_en': instance.descriptionEn,
      'logo': instance.logo,
      'level': instance.level,
      'url': instance.url,
      'jobOffers': instance.jobOffers,
    };
