import 'package:injectable/injectable.dart';
import 'package:mim_mobile/core/data/datasources/remote/base_remote.datasource.dart';
import 'package:mim_mobile/features/sponsors/data/datasources/remote/base_sponsors_client.dart';
import 'package:mim_mobile/features/sponsors/data/datasources/remote/sponsor.dto.dart';

@injectable
class SponsorsRemoteDatasource extends BaseRemoteDatasource<List<SponsorDto>> {
  final BaseSponsorsClient _client;

  SponsorsRemoteDatasource(this._client);

  Future<List<SponsorDto>> query() => super.execute(_getRemoteSponsors());

  Future<List<SponsorDto>> _getRemoteSponsors() => _client.getSponsors();
}
