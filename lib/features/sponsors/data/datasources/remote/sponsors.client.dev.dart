import 'package:injectable/injectable.dart';
import 'package:mim_mobile/features/sponsors/data/datasources/remote/base_sponsors_client.dart';
import 'package:mocktail/mocktail.dart';

@Singleton(as: BaseSponsorsClient)
@Environment('dev')
class MockSponsorsClient extends Mock implements BaseSponsorsClient {
  @factoryMethod
  static Future<MockSponsorsClient> instantiate() async => MockSponsorsClient();
}
