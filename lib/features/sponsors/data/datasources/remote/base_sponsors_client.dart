import 'package:mim_mobile/core/data/datasources/remote/base_client.dart';
import 'package:mim_mobile/features/sponsors/data/datasources/remote/sponsor.dto.dart';

abstract class BaseSponsorsClient extends BaseClient<List<dynamic>> {
  BaseSponsorsClient(super.dio);

  Future<List<SponsorDto>> getSponsors();
}
