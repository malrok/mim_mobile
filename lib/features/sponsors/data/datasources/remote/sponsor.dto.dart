import 'package:json_annotation/json_annotation.dart';
import 'package:mim_mobile/features/sponsors/data/datasources/remote/job_offer.dto.dart';

part 'sponsor.dto.g.dart';

@JsonSerializable()
class SponsorDto {
  String? key;
  String? name;
  @JsonKey(name: "description_fr")
  String? descriptionFr;
  @JsonKey(name: "description_en")
  String? descriptionEn;
  String? logo;
  String? level;
  String? url;
  List<JobOfferDto>? jobOffers;

  SponsorDto({
    this.key,
    this.name,
    this.descriptionFr,
    this.descriptionEn,
    this.logo,
    this.level,
    this.url,
    this.jobOffers,
  });

  factory SponsorDto.fromJson(Map<String, dynamic> json) => _$SponsorDtoFromJson(json);

  Map<String, dynamic> toJson() => _$SponsorDtoToJson(this);
}
