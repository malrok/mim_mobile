import 'package:json_annotation/json_annotation.dart';

part 'job_offer.dto.g.dart';

@JsonSerializable()
class JobOfferDto {
  String? title;
  String? link;

  JobOfferDto({
    this.title,
    this.link,
  });

  factory JobOfferDto.fromJson(Map<String, dynamic> json) => _$JobOfferDtoFromJson(json);

  Map<String, dynamic> toJson() => _$JobOfferDtoToJson(this);
}
