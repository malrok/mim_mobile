import 'package:injectable/injectable.dart';
import 'package:mim_mobile/core/data/datasources/remote/http.client.dart';
import 'package:mim_mobile/features/params/data/datasources/local/base_params_dao.dart';
import 'package:mim_mobile/features/sponsors/data/datasources/remote/base_sponsors_client.dart';
import 'package:mim_mobile/features/sponsors/data/datasources/remote/sponsor.dto.dart';

@Singleton(as: BaseSponsorsClient)
@Environment('prod')
class SponsorsClient extends BaseSponsorsClient {
  late BaseParamsDao _paramsDao;

  @factoryMethod
  static Future<SponsorsClient> instantiate(BaseParamsDao paramsDao) async => SponsorsClient._(
        httpClient,
      ).._paramsDao = paramsDao;

  SponsorsClient._(super.dio);

  @override
  Future<List<SponsorDto>> getSponsors() async => (await super.execute(_paramsDao.getParamsSync().sponsorsFileName))
      .map((data) => SponsorDto.fromJson(data))
      .toList();
}
