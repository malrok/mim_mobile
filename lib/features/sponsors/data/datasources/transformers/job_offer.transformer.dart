import 'package:mim_mobile/features/sponsors/data/datasources/local/job_offer.dbo.dart';
import 'package:mim_mobile/features/sponsors/data/datasources/remote/job_offer.dto.dart';

JobOfferDbo jobOfferDboFromJobOfferDto(String foreignKey, JobOfferDto jobOfferDto) => JobOfferDbo(
      -1,
      jobOfferDto.title ?? '',
      jobOfferDto.link ?? '',
      foreignKey,
    );
