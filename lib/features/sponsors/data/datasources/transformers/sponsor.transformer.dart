import 'package:mim_mobile/features/sponsors/data/datasources/local/sponsor.dbo.dart';
import 'package:mim_mobile/features/sponsors/data/datasources/remote/sponsor.dto.dart';
import 'package:mim_mobile/features/sponsors/data/datasources/transformers/job_offer.transformer.dart';

SponsorDbo sponsorDboFromSponsorDto(SponsorDto sponsorDto) => SponsorDbo(
      sponsorDto.key ?? '',
      sponsorDto.name ?? '',
      sponsorDto.descriptionFr ?? '',
      sponsorDto.descriptionEn ?? '',
      sponsorDto.logo ?? '',
      sponsorDto.level ?? '',
      sponsorDto.url ?? '',
    )..jobOffers =
        sponsorDto.jobOffers?.map((dto) => jobOfferDboFromJobOfferDto(sponsorDto.key ?? '', dto)).toList() ?? [];
