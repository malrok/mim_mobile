import 'package:mim_mobile/features/sponsors/data/datasources/local/sponsor.dbo.dart';
import 'package:mim_mobile/features/sponsors/domain/entities/sponsor.entity.dart';
import 'package:mim_mobile/features/sponsors/domain/transformers/job_offer.transformer.dart';
import 'package:mim_mobile/features/sponsors/domain/value_objects/sponsor_level.value_object.dart';

Sponsor sponsorFromSponsorDbo(SponsorDbo dbo) => Sponsor(
      key: dbo.sponsorKey,
      name: dbo.name,
      descriptionFr: dbo.descriptionFr,
      descriptionEn: dbo.descriptionEn,
      logo: dbo.logo.isNotEmpty ? 'https://thomas-boutin.github.io/${dbo.logo}' : '',
      level: SponsorLevel.fromString(dbo.level),
      url: dbo.url,
      jobOffers: dbo.jobOffers.map((j) => jobOfferFromJobOfferDbo(j)).toList(),
    );
