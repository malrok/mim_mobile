import 'package:mim_mobile/features/sponsors/data/datasources/local/job_offer.dbo.dart';
import 'package:mim_mobile/features/sponsors/domain/entities/job_offer.entity.dart';

JobOffer jobOfferFromJobOfferDbo(JobOfferDbo dbo) => JobOffer(
      dbo.title,
      dbo.link,
    );
