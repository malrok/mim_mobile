import 'package:mim_mobile/features/sponsors/data/datasources/local/sponsor.dbo.dart';

abstract class ISponsorsRepository {
  Stream<List<SponsorDbo>> getSponsors();

  Future<SponsorDbo?> getSponsorByKey(String key);

  Future<void> refreshLocalWithRemote();
}