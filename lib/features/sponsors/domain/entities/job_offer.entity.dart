class JobOffer {
  final String title;
  final String link;

  JobOffer(this.title, this.link);
}