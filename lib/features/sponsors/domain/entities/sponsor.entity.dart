import 'package:mim_mobile/features/sponsors/domain/entities/job_offer.entity.dart';
import 'package:mim_mobile/features/sponsors/domain/value_objects/sponsor_level.value_object.dart';

class Sponsor {
  final String key;
  final String name;
  final String descriptionFr;
  final String descriptionEn;
  final String logo;
  final SponsorLevel level;
  final String url;
  final List<JobOffer> jobOffers;

  Sponsor({
    required this.key,
    required this.name,
    required this.descriptionFr,
    required this.descriptionEn,
    required this.logo,
    required this.level,
    required this.url,
    required this.jobOffers,
  });
}
