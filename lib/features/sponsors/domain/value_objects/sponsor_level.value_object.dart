enum SponsorLevel {
  platinium('platinium'),
  gold('gold'),
  silver('silver');

  final String desc;

  const SponsorLevel(this.desc);

  factory SponsorLevel.fromString(String? value) => SponsorLevel.values.firstWhere((e) => e.desc == value);
}
