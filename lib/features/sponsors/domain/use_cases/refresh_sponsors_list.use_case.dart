import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/features/sponsors/domain/repositories/isponsors.repository.dart';

@injectable
class RefreshSponsorsListUseCase {
  ISponsorsRepository repository;

  RefreshSponsorsListUseCase(this.repository);

  Future<Either<AppError, void>> execute() async {
    try {
      await repository.refreshLocalWithRemote();
      return const Right(null);
    } catch (error) {
      return Left(AppError(error));
    }
  }
}
