import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/features/sponsors/data/datasources/local/sponsor.dbo.dart';
import 'package:mim_mobile/features/sponsors/domain/entities/sponsor.entity.dart';
import 'package:mim_mobile/features/sponsors/domain/repositories/isponsors.repository.dart';
import 'package:mim_mobile/features/sponsors/domain/transformers/sponsor.transformer.dart';

@injectable
class GetSponsorDetailsUseCase {
  ISponsorsRepository repository;

  GetSponsorDetailsUseCase(this.repository);

  Future<Either<AppError, Sponsor?>> execute(String key) async {
    try {
      SponsorDbo? result = await repository.getSponsorByKey(key);

      return Right(result == null ? null : sponsorFromSponsorDbo(result));
    } catch (error) {
      return Left(AppError(error));
    }
  }
}
