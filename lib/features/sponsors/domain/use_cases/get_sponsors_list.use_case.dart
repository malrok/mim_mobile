import 'dart:async';

import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/features/sponsors/domain/entities/sponsor.entity.dart';
import 'package:mim_mobile/features/sponsors/domain/repositories/isponsors.repository.dart';
import 'package:mim_mobile/features/sponsors/domain/transformers/sponsor.transformer.dart';

@injectable
class GetSponsorsListUseCase {
  ISponsorsRepository repository;

  GetSponsorsListUseCase(this.repository);

  Stream<Either<AppError, List<Sponsor>>> execute() {
    StreamController<Either<AppError, List<Sponsor>>> controller = StreamController();

    runZonedGuarded(
      () => repository
          .getSponsors()
          .listen((sponsors) => controller.add(Right(sponsors.map((dbo) => sponsorFromSponsorDbo(dbo)).toList()))),
      (error, stack) => controller.add(Left(AppError(error))),
    );

    return controller.stream;
  }
}
