import 'package:floor/floor.dart';
import 'package:mim_mobile/features/events/data/datasources/local/event.dbo.dart';

@Entity(
  tableName: 'speaker',
)
class SpeakerDbo {
  @primaryKey
  int id;
  String name;
  String bio;
  @ColumnInfo(name: 'picture_url')
  String pictureUrl;
  String company;
  @ColumnInfo(name: 'twitter_url')
  String twitterUrl;
  @ColumnInfo(name: 'github_url')
  String githubUrl;

  /// ignored fields
  @ignore
  List<EventDbo> events = [];

  SpeakerDbo(
    this.id,
    this.name,
    this.bio,
    this.pictureUrl,
    this.company,
    this.twitterUrl,
    this.githubUrl,
  );
}
