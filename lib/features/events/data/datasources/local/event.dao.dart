import 'package:floor/floor.dart';
import 'package:mim_mobile/features/events/data/datasources/local/event.dbo.dart';

@dao
abstract class EventDao {
  @Query('SELECT * FROM event')
  Future<List<EventDbo>> getAllEventsOnce();

  @Query('SELECT * FROM event')
  Stream<List<EventDbo>> getAllEvents();

  @Query('SELECT * FROM event where isFavorite = true order by start_date')
  Stream<List<EventDbo>> getAllFavoriteEvents();

  @Query('SELECT * FROM event WHERE id = :id')
  Future<EventDbo?> getEventById(String id);

  @Query('SELECT * FROM event WHERE id IN (:ids)')
  Future<List<EventDbo>> getEventsByIds(List<String> ids);

  @update
  Future<void> updateEvent(EventDbo event);

  @insert
  Future<void> insertEvent(EventDbo event);

  @Query('DELETE FROM event')
  Future<void> deleteEvents();
}
