import 'package:floor/floor.dart';
import 'package:mim_mobile/features/events/data/datasources/local/speaker.dbo.dart';

@dao
abstract class SpeakerDao {
  @Query('SELECT * FROM speaker')
  Future<List<SpeakerDbo>> getAllSpeakers();

  @Query('SELECT * FROM speaker WHERE id = :id')
  Future<SpeakerDbo?> getSpeakerById(int id);

  @Query('SELECT * FROM speaker WHERE id IN (:ids)')
  Future<List<SpeakerDbo>> getSpeakersByIds(List<int> ids);

  @insert
  Future<void> insertSpeaker(SpeakerDbo speaker);

  @Query('DELETE FROM speaker')
  Future<void> deleteSpeakers();
}
