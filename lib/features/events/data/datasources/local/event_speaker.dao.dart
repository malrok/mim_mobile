import 'package:floor/floor.dart';
import 'package:mim_mobile/features/events/data/datasources/local/event_speaker.dbo.dart';

@dao
abstract class EventSpeakerDao {
  @Query('SELECT * FROM event_speaker')
  Future<List<EventSpeakerDbo>> getAllEventSpeakers();

  @Query('SELECT * FROM event_speaker WHERE event_id = :id')
  Future<List<EventSpeakerDbo>> getEventsSpeakersByEventId(String id);

  @Query('SELECT * FROM event_speaker WHERE speaker_id = :id')
  Future<List<EventSpeakerDbo>> getEventsSpeakersBySpeakerId(int id);

  @insert
  Future<void> insertEventSpeaker(EventSpeakerDbo event);

  @Query('DELETE FROM event_speaker')
  Future<void> deleteEventsSpeakers();
}
