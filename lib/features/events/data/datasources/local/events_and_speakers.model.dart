import 'package:mim_mobile/features/events/data/datasources/local/event.dbo.dart';
import 'package:mim_mobile/features/events/data/datasources/local/event_speaker.dbo.dart';
import 'package:mim_mobile/features/events/data/datasources/local/speaker.dbo.dart';

class EventsAndSpeakers {
  List<EventDbo> events = [];
  List<SpeakerDbo> speakers = [];
  List<EventSpeakerDbo> eventsSpeakers = [];
}
