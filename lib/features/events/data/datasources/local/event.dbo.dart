import 'package:floor/floor.dart';
import 'package:mim_mobile/features/events/data/datasources/local/speaker.dbo.dart';

@Entity(
  tableName: 'event',
)
class EventDbo {
  /// database fields
  @primaryKey
  String id;
  String openFeedbackId;
  String label;
  String description;
  @ColumnInfo(name: 'start_date')
  DateTime startDate;
  @ColumnInfo(name: 'end_date')
  DateTime endDate;
  String room;
  String category;
  String type;
  String language;
  bool isFavorite;

  /// ignored fields
  @ignore
  List<SpeakerDbo> speakers = [];

  EventDbo(
    this.id,
    this.openFeedbackId,
    this.label,
    this.description,
    this.startDate,
    this.endDate,
    this.room,
    this.category,
    this.type,
    this.language,
    this.isFavorite,
  );
}
