import 'package:floor/floor.dart';
import 'package:mim_mobile/features/events/data/datasources/local/event.dbo.dart';
import 'package:mim_mobile/features/events/data/datasources/local/speaker.dbo.dart';

@Entity(
  tableName: 'event_speaker',
  foreignKeys: [
    ForeignKey(
      childColumns: ['speaker_id'],
      parentColumns: ['id'],
      entity: SpeakerDbo,
    ),
    ForeignKey(
      childColumns: ['event_id'],
      parentColumns: ['id'],
      entity: EventDbo,
    ),
  ],
)
class EventSpeakerDbo {
  @primaryKey
  int id;
  @ColumnInfo(name: 'speaker_id')
  int speakerId;
  @ColumnInfo(name: 'event_id')
  String eventId;

  EventSpeakerDbo(
    this.id,
    this.speakerId,
    this.eventId,
  );
}
