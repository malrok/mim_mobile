import 'package:mim_mobile/features/events/data/datasources/local/event.dbo.dart';
import 'package:mim_mobile/features/events/data/datasources/local/event_speaker.dbo.dart';
import 'package:mim_mobile/features/events/data/datasources/local/events_and_speakers.model.dart';
import 'package:mim_mobile/features/events/data/datasources/local/speaker.dbo.dart';
import 'package:mim_mobile/features/events/data/datasources/remote/event.dto.dart';

class EventTransformer {
  final DateTime _eventDate;

  int _eventId = 0;
  int _speakerId = 0;
  int _eventAndSpeakerId = 0;

  EventTransformer(this._eventDate);

  EventsAndSpeakers eventsAndSpeakersFromEventDto(List<EventDto> dtoEvents) {
    final eventAndSpeakers = EventsAndSpeakers();

    for (var dto in dtoEvents) {
      /// we prepare the events
      final dboEvent = _dboEventFromEventDto(dto);
      if (dboEvent.id.isEmpty) dboEvent.id = '${_eventId++}';
      eventAndSpeakers.events.add(dboEvent);

      /// we prepare the speakers
      final dboSpeakers = _dboSpeakersFromEventDto(dto);
      for (var dboSpeaker in dboSpeakers) {
        try {
          final speaker = eventAndSpeakers.speakers.firstWhere((element) => element.name == dboSpeaker.name);
          dboSpeaker.id = speaker.id;
        } on StateError {
          dboSpeaker.id = _speakerId++;
          eventAndSpeakers.speakers.add(dboSpeaker);
        }
        dboEvent.speakers.add(dboSpeaker);
        eventAndSpeakers.eventsSpeakers.add(EventSpeakerDbo(_eventAndSpeakerId++, dboSpeaker.id, dboEvent.id));
      }
    }

    return eventAndSpeakers;
  }

  EventDbo _dboEventFromEventDto(EventDto dto) {
    return EventDbo(
      dto.id!,
      dto.openFeedbackId ?? '',
      dto.title ?? '',
      dto.abstract?.replaceAll('\\\\n', '\n') ?? '',
      startDateFromSchedule(dto.schedule),
      endDateFromSchedule(dto.schedule),
      dto.track ?? '',
      dto.categorie ?? '',
      dto.type ?? '',
      dto.language ?? '',
      false,
    );
  }

  List<SpeakerDbo> _dboSpeakersFromEventDto(EventDto dto) {
    final dboSpeakers = <SpeakerDbo>[];
    if (_isValidName(dto.speaker1FullName)) {
      dboSpeakers.add(SpeakerDbo(
        -1,
        dto.speaker1FullName!,
        dto.speaker1Bio ?? '',
        _getPictureUrl(dto.speaker1PictureURL),
        dto.speaker1Company ?? '',
        dto.speaker1TwitterURL ?? '',
        dto.speaker1GitHubURL ?? '',
      ));
    }
    if (_isValidName(dto.speaker2FullName)) {
      dboSpeakers.add(SpeakerDbo(
        -1,
        dto.speaker2FullName!,
        dto.speaker2Bio ?? '',
        _getPictureUrl(dto.speaker2PictureURL),
        dto.speaker2Company ?? '',
        dto.speaker2TwitterURL ?? '',
        dto.speaker2GitHubURL ?? '',
      ));
    }
    if (_isValidName(dto.speaker3FullName)) {
      dboSpeakers.add(SpeakerDbo(
        -1,
        dto.speaker3FullName!,
        dto.speaker3Bio ?? '',
        _getPictureUrl(dto.speaker3PictureURL),
        dto.speaker3Company ?? '',
        dto.speaker3TwitterURL ?? '',
        dto.speaker3GitHubURL ?? '',
      ));
    }
    if (_isValidName(dto.speaker4FullName)) {
      dboSpeakers.add(SpeakerDbo(
        -1,
        dto.speaker4FullName!,
        dto.speaker4Bio ?? '',
        _getPictureUrl(dto.speaker4PictureURL),
        dto.speaker4Company ?? '',
        dto.speaker4TwitterURL ?? '',
        dto.speaker4GitHubURL ?? '',
      ));
    }
    if (_isValidName(dto.speaker5FullName)) {
      dboSpeakers.add(SpeakerDbo(
        -1,
        dto.speaker5FullName!,
        dto.speaker5Bio ?? '',
        _getPictureUrl(dto.speaker5PictureURL),
        dto.speaker5Company ?? '',
        dto.speaker5TwitterURL ?? '',
        dto.speaker5GitHubURL ?? '',
      ));
    }
    return dboSpeakers;
  }

  DateTime startDateFromSchedule(String? schedule) {
    return _getDateTimeFromTime(schedule?.split('-')[0]);
  }

  DateTime endDateFromSchedule(String? schedule) {
    return _getDateTimeFromTime(schedule?.split('-')[1]);
  }

  DateTime _getDateTimeFromTime(String? time) {
    if (time == null) return DateTime.now();

    final elements = time.split('h');
    final hour = elements[0];
    final minutes = elements[1].isNotEmpty ? elements[1] : '0';
    return _eventDate.copyWith(hour: int.parse(hour), minute: int.parse(minutes));
  }

  bool _isValidName(String? name) {
    if (name == null) return false;
    if (name.isEmpty) return false;
    if (name == '#') return false;
    if (name == '#N/A') return false;
    return true;
  }

  String _getPictureUrl(String? url) {
    if (url == null) return '';
    if (Uri.parse(url).host.isNotEmpty) return url;
    return '';
  }
}
