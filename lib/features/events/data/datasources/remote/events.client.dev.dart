import 'package:injectable/injectable.dart';
import 'package:mim_mobile/features/events/data/datasources/remote/base_events_client.dart';
import 'package:mim_mobile/features/params/data/datasources/local/base_params_dao.dart';
import 'package:mocktail/mocktail.dart';

@Singleton(as: BaseEventsClient)
@Environment('dev')
class MockEventsClient extends Mock implements BaseEventsClient {
  @factoryMethod
  static Future<MockEventsClient> instantiate(BaseParamsDao paramsDao) async => MockEventsClient();
}
