import 'package:json_annotation/json_annotation.dart';

part 'event.dto.g.dart';

@JsonSerializable()
class EventDto {
  String? track;
  String? type;
  String? schedule;
  String? id;
  String? openFeedbackId;
  String? title;
  String? format;
  String? categorie;
  String? abstract;
  String? level;
  String? language;
  String? speaker1FullName;
  String? speaker1Company;
  String? speaker1Bio;
  String? speaker1PictureURL;
  String? speaker1TwitterURL;
  String? speaker1GitHubURL;
  String? speaker2FullName;
  String? speaker2Company;
  String? speaker2Bio;
  String? speaker2PictureURL;
  String? speaker2TwitterURL;
  String? speaker2GitHubURL;
  String? speaker3FullName;
  String? speaker3Company;
  String? speaker3Bio;
  String? speaker3PictureURL;
  String? speaker3TwitterURL;
  String? speaker3GitHubURL;
  String? speaker4FullName;
  String? speaker4Company;
  String? speaker4Bio;
  String? speaker4PictureURL;
  String? speaker4TwitterURL;
  String? speaker4GitHubURL;
  String? speaker5FullName;
  String? speaker5Company;
  String? speaker5Bio;
  String? speaker5PictureURL;
  String? speaker5TwitterURL;
  String? speaker5GitHubURL;

  EventDto({
    this.track,
    this.type,
    this.schedule,
    this.id,
    this.openFeedbackId,
    this.title,
    this.format,
    this.categorie,
    this.abstract,
    this.level,
    this.language,
    this.speaker1FullName,
    this.speaker1Company,
    this.speaker1Bio,
    this.speaker1PictureURL,
    this.speaker1TwitterURL,
    this.speaker1GitHubURL,
    this.speaker2FullName,
    this.speaker2Company,
    this.speaker2Bio,
    this.speaker2PictureURL,
    this.speaker2TwitterURL,
    this.speaker2GitHubURL,
    this.speaker3FullName,
    this.speaker3Company,
    this.speaker3Bio,
    this.speaker3PictureURL,
    this.speaker3TwitterURL,
    this.speaker3GitHubURL,
    this.speaker4FullName,
    this.speaker4Company,
    this.speaker4Bio,
    this.speaker4PictureURL,
    this.speaker4TwitterURL,
    this.speaker4GitHubURL,
    this.speaker5FullName,
    this.speaker5Company,
    this.speaker5Bio,
    this.speaker5PictureURL,
    this.speaker5TwitterURL,
    this.speaker5GitHubURL,
  });

  factory EventDto.fromJson(Map<String, dynamic> json) => _$EventDtoFromJson(json);

  Map<String, dynamic> toJson() => _$EventDtoToJson(this);
}
