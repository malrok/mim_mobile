import 'package:mim_mobile/core/data/datasources/remote/base_client.dart';
import 'package:mim_mobile/features/events/data/datasources/remote/event.dto.dart';

abstract class BaseEventsClient extends BaseClient<List<dynamic>> {
  BaseEventsClient(super.dio);

  Future<List<EventDto>> getEvents();
}
