import 'package:injectable/injectable.dart';
import 'package:mim_mobile/core/data/datasources/remote/base_remote.datasource.dart';
import 'package:mim_mobile/features/events/data/datasources/remote/base_events_client.dart';
import 'package:mim_mobile/features/events/data/datasources/remote/event.dto.dart';

@injectable
class EventsRemoteDatasource extends BaseRemoteDatasource<List<EventDto>> {
  final BaseEventsClient _client;

  EventsRemoteDatasource(this._client);

  Future<List<EventDto>> query() => super.execute(_getRemoteEvents());

  Future<List<EventDto>> _getRemoteEvents() => _client.getEvents();
}
