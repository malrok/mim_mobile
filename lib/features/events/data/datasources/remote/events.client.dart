import 'package:injectable/injectable.dart';
import 'package:mim_mobile/core/data/datasources/remote/http.client.dart';
import 'package:mim_mobile/features/events/data/datasources/remote/base_events_client.dart';
import 'package:mim_mobile/features/events/data/datasources/remote/event.dto.dart';
import 'package:mim_mobile/features/params/data/datasources/local/base_params_dao.dart';

@Singleton(as: BaseEventsClient)
@Environment('prod')
class EventsClient extends BaseEventsClient {
  late BaseParamsDao _paramsDao;

  @factoryMethod
  static Future<EventsClient> instantiate(BaseParamsDao paramsDao) async => EventsClient._(
        httpClient,
      ).._paramsDao = paramsDao;

  EventsClient._(super.dio);

  @override
  Future<List<EventDto>> getEvents() async =>
      (await super.execute(_paramsDao.getParamsSync().eventsFileName)).map((data) => EventDto.fromJson(data)).toList();
}
