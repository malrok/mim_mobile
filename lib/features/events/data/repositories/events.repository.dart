import 'dart:async';

import 'package:injectable/injectable.dart';
import 'package:mim_mobile/config/di/dependency_injection.dart';
import 'package:mim_mobile/core/data/datasources/local/database_accessor.dart';
import 'package:mim_mobile/features/events/data/datasources/local/event.dbo.dart';
import 'package:mim_mobile/features/events/data/datasources/remote/events.remote_datasource.dart';
import 'package:mim_mobile/features/events/data/datasources/transformers/event.transformer.dart';
import 'package:mim_mobile/features/events/domain/repositories/ievents.repository.dart';
import 'package:mim_mobile/features/params/data/datasources/local/base_params_dao.dart';

@Singleton(as: IEventsRepository)
class EventsRepository implements IEventsRepository {
  late DatabaseAccessor _dbAccessor;
  late EventsRemoteDatasource _datasource;
  late EventTransformer _eventTransformer;

  @factoryMethod
  static Future<EventsRepository> instantiate(DatabaseAccessor dbAccessor, EventsRemoteDatasource datasource) async {
    final repository = EventsRepository._();

    final dao = await getIt.getAsync<BaseParamsDao>();

    final params = dao.getParamsSync();

    repository._dbAccessor = dbAccessor;
    repository._datasource = datasource;
    repository._eventTransformer = EventTransformer(params.eventDate);

    return repository;
  }

  EventsRepository._();

  bool _refreshingData = false;

  @override
  Stream<List<EventDbo>> getEvents() {
    late StreamController<List<EventDbo>> controller;

    StreamSubscription? subscription;

    void startListener() {
      subscription = _dbAccessor.getEventsWithSpeakers().listen((dbEvents) async {
        if (dbEvents.isEmpty) {
          if (!_refreshingData) {
            final eventsDto = await _datasource.query();
            final eventsAndSpeakers = _eventTransformer.eventsAndSpeakersFromEventDto(eventsDto);

            _dbAccessor.insertEventsAndSpeakers(eventsAndSpeakers);

            controller.add(eventsAndSpeakers.events);
          }
        } else {
          controller.add(dbEvents);
        }
      });
    }

    void stopListener() {
      subscription?.cancel();
      subscription = null;
    }

    controller = StreamController(
      onListen: startListener,
      onPause: stopListener,
      onResume: startListener,
      onCancel: stopListener,
    );

    return controller.stream;
  }

  @override
  Stream<List<EventDbo>> getFavoriteEvents() {
    late StreamController<List<EventDbo>> controller;

    StreamSubscription? subscription;

    void startListener() =>
        subscription = _dbAccessor.getFavoriteEventsWithSpeakers().listen((dbEvents) async => controller.add(dbEvents));

    void stopListener() {
      subscription?.cancel();
      subscription = null;
    }

    controller = StreamController(
      onListen: startListener,
      onPause: stopListener,
      onResume: startListener,
      onCancel: stopListener,
    );

    return controller.stream;
  }

  @override
  Future<EventDbo?> getEventById(String id) => _dbAccessor.getEventWithSpeakersById(id);

  @override
  Future<EventDbo> updateEvent(EventDbo dbo) => _dbAccessor.updateEvent(dbo);

  @override
  Future<void> refreshLocalWithRemote() async {
    _refreshingData = true;

    List<EventDbo> dbFavoriteEvents =
        (await _dbAccessor.getEventsWithSpeakersFuture()).where((element) => element.isFavorite == true).toList();

    try {
      final eventsDto = await _datasource.query();

      if (eventsDto.isNotEmpty) {
        await _dbAccessor.dropData();
        final eventsAndSpeakers = _eventTransformer.eventsAndSpeakersFromEventDto(eventsDto);

        for (var dbFavoriteEvent in dbFavoriteEvents) {
          eventsAndSpeakers.events.firstWhere((element) => element.label == dbFavoriteEvent.label).isFavorite = true;
        }

        await _dbAccessor.insertEventsAndSpeakers(eventsAndSpeakers);
      }
    } catch (e) {
      print('DEBUG --- error : $e');
      rethrow;
    }

    _refreshingData = false;
  }
}
