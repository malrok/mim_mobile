import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mim_mobile/core/presentation/loading_display.widget.dart';
import 'package:mim_mobile/features/events/presentation/blocs/event/event.cubit.dart';
import 'package:mim_mobile/features/events/presentation/pages/event_detail/widgets/event_detail_loader.widget.dart';

class EventDetailPage extends StatefulWidget {
  final String? id;

  const EventDetailPage({super.key, this.id});

  @override
  State<EventDetailPage> createState() => _EventDetailPageState();
}

class _EventDetailPageState extends State<EventDetailPage> {
  @override
  Widget build(BuildContext context) {
    if (widget.id == null) {
      return const LoadingDisplay();
    } else {
      return BlocProvider(
        create: (_) => EventCubit()..load(widget.id!),
        child: EventDetailLoader(id: widget.id!),
      );
    }
  }
}
