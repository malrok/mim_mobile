import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mim_mobile/core/presentation/error_display.widget.dart';
import 'package:mim_mobile/core/presentation/loading_display.widget.dart';
import 'package:mim_mobile/features/events/presentation/blocs/event/event.cubit.dart';
import 'package:mim_mobile/features/events/presentation/blocs/event/event.states.dart';
import 'package:mim_mobile/features/events/presentation/blocs/favorite/favorite.cubit.dart';
import 'package:mim_mobile/features/events/presentation/pages/event_detail/widgets/event_detail.widget.dart';

class EventDetailLoader extends StatefulWidget {
  final String id;

  const EventDetailLoader({
    super.key,
    required this.id,
  });

  @override
  State<EventDetailLoader> createState() => _EventDetailLoaderState();
}

class _EventDetailLoaderState extends State<EventDetailLoader> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EventCubit, EventStates>(
      builder: (context, state) => switch (state) {
        EventStatesData() => BlocProvider<FavoriteCubit>(
            create: (_) => FavoriteCubit()..load(state.data.id),
            child: EventDetail(event: state.data),
          ),
        EventStatesLoading() => const LoadingDisplay(),
        EventStatesError() => ErrorDisplay(
            error: state.error,
            onRetry: () => context.read<EventCubit>().load(widget.id),
          ),
      },
    );
  }
}
