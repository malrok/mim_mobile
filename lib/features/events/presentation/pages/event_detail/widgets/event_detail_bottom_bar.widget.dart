import 'package:flutter/material.dart';
import 'package:mim_mobile/config/constants/colors.constants.dart';
import 'package:mim_mobile/config/constants/icons.constants.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';
import 'package:mim_mobile/config/constants/theme.constants.dart';
import 'package:mim_mobile/core/presentation/favorite_toggle.widget.dart';
import 'package:mim_mobile/features/events/domain/entities/event.entity.dart';

class EventDetailBottomBar extends StatelessWidget {
  final Event event;

  const EventDetailBottomBar({
    super.key,
    required this.event,
  });

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      height: MimSizes.xxxlarge,
      padding: const EdgeInsets.all(MimSizes.regular),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        textBaseline: TextBaseline.alphabetic,
        children: [
          const Center(
            child: Image(
              image: MimIcons.location,
              width: MimSizes.medium,
              height: MimSizes.medium,
            ),
          ),
          const SizedBox(width: MimSizes.xxsmall),
          Text(
            event.track,
            style: textTheme.labelMedium!.copyWith(color: MimColors.primary),
          ),
          const SizedBox(width: MimSizes.small),
          VerticalDivider(
            color: MimColors.divider,
            thickness: MimSizes.xxxsmall,
            indent: MimSizes.xsmall,
            endIndent: MimSizes.xsmall,
          ),
          const SizedBox(width: MimSizes.small),
          Text(event.language.icon),
          const SizedBox(width: MimSizes.xxsmall),
          Text(
            event.language.desc,
            style: textTheme.labelMedium!.copyWith(color: MimColors.greyDark),
          ),
          const Spacer(),
          FavoriteToggle(event: event, size: MimSizes.large),
        ],
      ),
    );
  }
}
