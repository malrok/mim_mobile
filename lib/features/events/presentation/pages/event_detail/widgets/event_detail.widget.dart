import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:go_router/go_router.dart';
import 'package:mim_mobile/config/constants/decorations.constants.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';
import 'package:mim_mobile/core/presentation/app_tag.widget.dart';
import 'package:mim_mobile/core/presentation/formatted_text.widget.dart';
import 'package:mim_mobile/core/presentation/mim_app_bar.widget.dart';
import 'package:mim_mobile/core/presentation/primary_button.widget.dart';
import 'package:mim_mobile/core/presentation/speaker_card.widget.dart';
import 'package:mim_mobile/core/presentation/speaker_card_small.widget.dart';
import 'package:mim_mobile/core/presentation/speaker_description.widget.dart';
import 'package:mim_mobile/features/events/domain/entities/event.entity.dart';
import 'package:mim_mobile/features/events/domain/entities/speaker.entity.dart';
import 'package:mim_mobile/features/events/presentation/pages/event_detail/widgets/event_detail_bottom_bar.widget.dart';
import 'package:mim_mobile/features/params/presentation/blocs/get_params.cubit.dart';
import 'package:mim_mobile/features/params/presentation/blocs/get_params.states.dart';

class EventDetail extends StatefulWidget {
  final Event event;

  const EventDetail({
    super.key,
    required this.event,
  });

  @override
  State<EventDetail> createState() => _EventDetailState();
}

class _EventDetailState extends State<EventDetail> {
  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final l10n = AppLocalizations.of(context)!;

    return Scaffold(
      appBar: MimAppBar(
        isMainNav: false,
        trailing: AppTag(label: widget.event.category.desc, icon: widget.event.category.icon, size: AppTagSize.big),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: MimSizes.medium,
            vertical: MimSizes.large,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(widget.event.label, style: textTheme.headlineLarge),
              const SizedBox(height: MimSizes.regular),
              Wrap(
                spacing: MimSizes.xsmall,
                runSpacing: MimSizes.xsmall,
                children: [
                  ...widget.event.speakers.map(
                    (Speaker speaker) => SpeakerCardSmall(speaker: speaker),
                  ),
                ],
              ),
              const SizedBox(height: MimSizes.medium),
              FormattedText(value: widget.event.description),
              BlocBuilder<GetParamsCubit, GetParamsStates>(
                bloc: GetParamsCubit()..load(),
                builder: (context, state) {
                  return Column(
                    children: [
                      const SizedBox(height: MimSizes.medium),
                      PrimaryButton(
                        label: l10n.giveFeedback,
                        onPressed: state is GetParamsStatesData && state.data.openFeedbackUrl.isNotEmpty
                            ? () => context.push(
                                  '/feedback',
                                  extra: '${state.data.openFeedbackUrl}${widget.event.openFeedbackId}',
                                )
                            : null,
                      ),
                    ],
                  );
                },
              ),
              const SizedBox(height: MimSizes.medium),
              ...widget.event.speakers.map(
                (Speaker s) => Container(
                  decoration: MimDecorations.dottedTopLine,
                  padding: const EdgeInsets.symmetric(vertical: MimSizes.regular),
                  child: Column(
                    children: [
                      SpeakerCard(
                        speaker: s,
                        onTap: () => context.push('/speaker_detail', extra: s.id),
                      ),
                      if (s.bio.isNotEmpty) const SizedBox(height: MimSizes.medium),
                      SpeakerDescription(speaker: s),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: EventDetailBottomBar(event: widget.event),
    );
  }
}
