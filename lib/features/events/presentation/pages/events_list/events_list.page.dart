import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mim_mobile/features/events/presentation/blocs/events_list/get_events_list.cubit.dart';
import 'package:mim_mobile/features/events/presentation/pages/events_list/widgets/tracks_container.widget.dart';

class EventsListPage extends StatelessWidget {
  const EventsListPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => GetEventsListCubit()..load(),
      child: const TracksContainer(),
    );
  }
}
