import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mim_mobile/config/constants/colors.constants.dart';
import 'package:mim_mobile/config/constants/icons.constants.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';
import 'package:mim_mobile/config/constants/theme.constants.dart';
import 'package:mim_mobile/features/events/domain/entities/track.entity.dart';
import 'package:mim_mobile/features/events/presentation/pages/events_list/widgets/events_list.widget.dart';
import 'package:mim_mobile/features/events/presentation/pages/favorites_list/favorites_list.page.dart';

class TracksTabs extends StatefulWidget {
  final List<Track> tracks;

  const TracksTabs({
    super.key,
    required this.tracks,
  });

  @override
  State<TracksTabs> createState() => _TracksTabsState();
}

class _TracksTabsState extends State<TracksTabs> {
  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;

    return DefaultTabController(
      length: widget.tracks.length + 1,
      initialIndex: 1,
      child: Column(
        children: [
          _TracksTabBar(
            tabs: [
              SizedBox(
                width: width * 0.16,
                child: Tab(
                  icon: SvgPicture.asset(
                    MimSvg.bookmarkOn,
                    // width: MimSizes.medium,
                    height: MimSizes.medium,
                    colorFilter: ColorFilter.mode(MimColors.white, BlendMode.srcIn),
                  ),
                ),
              ),
              ...widget.tracks.map(
                (track) => SizedBox(
                  width: width * 0.28,
                  child: Tab(text: track.name),
                ),
              ),
            ],
          ),
          Expanded(
            child: TabBarView(
              children: [
                const FavoritesList(),
                ...widget.tracks.map(
                  (track) => EventsList(
                    track: track.name,
                    events: track.events,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class _TracksTabBar extends StatelessWidget {
  final List<Widget> tabs;

  const _TracksTabBar({
    required this.tabs,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      color: MimColors.primary,
      width: double.infinity,
      child: TabBar(
        indicatorSize: TabBarIndicatorSize.label,
        isScrollable: true,
        indicator: BoxDecoration(
          color: MimColors.black12,
          border: Border(
            top: BorderSide(color: MimColors.black12, width: 2),
          ),
        ),
        labelColor: MimColors.background,
        labelStyle: textTheme.labelMedium,
        tabAlignment: TabAlignment.start,
        unselectedLabelColor: MimColors.divider,
        unselectedLabelStyle: textTheme.labelMedium,
        tabs: tabs,
      ),
    );
  }
}
