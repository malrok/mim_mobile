import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mim_mobile/core/presentation/error_display.widget.dart';
import 'package:mim_mobile/core/presentation/loading_display.widget.dart';
import 'package:mim_mobile/features/events/presentation/blocs/events_list/get_events_list.cubit.dart';
import 'package:mim_mobile/features/events/presentation/blocs/events_list/get_events_list.states.dart';
import 'package:mim_mobile/features/events/presentation/pages/events_list/widgets/tracks_tabs.widget.dart';

class TracksContainer extends StatelessWidget {
  const TracksContainer({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GetEventsListCubit, GetEventsListStates>(
      builder: (context, state) => switch (state) {
        GetEventsListStatesData() => TracksTabs(tracks: state.data),
        GetEventsListStatesLoading() => const LoadingDisplay(),
        GetEventsListStatesError() => ErrorDisplay(
            error: state.error,
            onRetry: () => context.read<GetEventsListCubit>().load(),
          ),
      },
    );
  }
}
