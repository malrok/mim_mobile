import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:go_router/go_router.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';
import 'package:mim_mobile/core/presentation/empty_list.widget.dart';
import 'package:mim_mobile/core/presentation/event_card.widget.dart';
import 'package:mim_mobile/features/events/domain/entities/event.entity.dart';
import 'package:mim_mobile/features/events/domain/value_objects/talk_type.value_object.dart';
import 'package:mim_mobile/features/events/presentation/blocs/events_list/refresh_events_list.cubit.dart';
import 'package:mim_mobile/features/events/presentation/blocs/events_list/refresh_events_list.states.dart';

class EventsList extends StatefulWidget {
  final List<Event> events;
  final String track;

  const EventsList({
    super.key,
    required this.track,
    required this.events,
  });

  @override
  State<EventsList> createState() => _EventsListState();
}

class _EventsListState extends State<EventsList> {
  final RefreshEventsListCubit _refreshEventsListCubit = RefreshEventsListCubit();

  @override
  Widget build(BuildContext context) {
    final l10n = AppLocalizations.of(context)!;
    final list = widget.events;

    return BlocListener<RefreshEventsListCubit, RefreshEventsListStates>(
      bloc: _refreshEventsListCubit,
      listener: (context, state) {
        if (state is RefreshEventsListStatesRefreshingError) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(state.error.getMessage(l10n))));
        }
      },
      child: RefreshIndicator(
        onRefresh: () async => _refreshEventsListCubit.refresh(),
        child: SingleChildScrollView(
          key: PageStorageKey(widget.track),
          padding: const EdgeInsets.symmetric(vertical: MimSizes.regular),
          child: (list.isEmpty)
              ? EmptyList(text: l10n.noEventsForQuery)
              : Column(
                  children: [
                    ...list.map(
                      (event) => Padding(
                        padding: const EdgeInsets.symmetric(horizontal: MimSizes.medium),
                        child: PlanningElement(
                          start: event.startDate,
                          end: event.endDate,
                          events: [event],
                          onTap: (_) {
                            if (event.type == TalkType.talk) {
                              context.push('/event_detail', extra: event.id);
                            }
                          },
                        ),
                      ),
                    )
                  ],
                ),
        ),
      ),
    );
  }
}
