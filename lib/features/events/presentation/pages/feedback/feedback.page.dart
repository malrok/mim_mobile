import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:webview_flutter/webview_flutter.dart';

class FeedbackPage extends StatefulWidget {
  final String openFeedbackUrl;

  const FeedbackPage({super.key, required this.openFeedbackUrl});

  @override
  State<FeedbackPage> createState() => _FeedbackPageState();
}

class _FeedbackPageState extends State<FeedbackPage> {
  late WebViewController _controller;

  @override
  void initState() {
    final delegate = NavigationDelegate(onPageFinished: (String url) => _controller.runJavaScript('''
          const sheet = new CSSStyleSheet();
          sheet.replaceSync('header {display: none}');
          document.adoptedStyleSheets = [sheet];
          '''));
    _controller = WebViewController()
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setNavigationDelegate(delegate);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _controller.loadRequest(Uri.parse(widget.openFeedbackUrl));
  }

  @override
  Widget build(BuildContext context) {
    final l10n = AppLocalizations.of(context)!;
    return Scaffold(
      appBar: AppBar(title: Text(l10n.giveFeedback)),
      body: WebViewWidget(controller: _controller),
    );
  }
}
