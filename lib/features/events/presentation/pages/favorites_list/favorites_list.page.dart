import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:go_router/go_router.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';
import 'package:mim_mobile/core/presentation/empty_list.widget.dart';
import 'package:mim_mobile/core/presentation/error_display.widget.dart';
import 'package:mim_mobile/core/presentation/event_card.widget.dart';
import 'package:mim_mobile/core/presentation/loading_display.widget.dart';
import 'package:mim_mobile/features/events/domain/entities/event.entity.dart';
import 'package:mim_mobile/features/events/domain/value_objects/talk_type.value_object.dart';
import 'package:mim_mobile/features/events/presentation/blocs/favorites_list/favorites_list.cubit.dart';
import 'package:mim_mobile/features/events/presentation/blocs/favorites_list/favorites_list.states.dart';

class FavoritesList extends StatelessWidget {
  const FavoritesList({super.key});

  @override
  Widget build(BuildContext context) {
    final l10n = AppLocalizations.of(context)!;

    return BlocBuilder<FavoritesListCubit, FavoritesListStates>(
      bloc: FavoritesListCubit()..load(),
      builder: (context, state) {
        return switch (state) {
          FavoritesListStatesData() => SingleChildScrollView(
              padding: const EdgeInsets.symmetric(vertical: MimSizes.regular),
              child: (state.data.isEmpty)
                  ? EmptyList(text: l10n.noFavoritesSet)
                  : Column(
                      children: [
                        ...state.data.map(
                          (planning) => Padding(
                            padding: const EdgeInsets.symmetric(horizontal: MimSizes.medium),
                            child: PlanningElement(
                              start: planning.start,
                              end: planning.end,
                              events: planning.events,
                              onTap: (Event event) {
                                if (event.type == TalkType.talk) {
                                  context.push('/event_detail', extra: event.id);
                                }
                              },
                            ),
                          ),
                        )
                      ],
                    ),
            ),
          FavoritesListStatesLoading() => const LoadingDisplay(),
          FavoritesListStatesError() => ErrorDisplay(error: state.error),
        };
      },
    );
  }
}
