import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';

sealed class RefreshEventsListStates {}

final class RefreshEventsListStatesRefreshing extends RefreshEventsListStates {}

final class RefreshEventsListStatesRefreshingError extends RefreshEventsListStates {
  final AppError error;

  RefreshEventsListStatesRefreshingError(this.error);
}
