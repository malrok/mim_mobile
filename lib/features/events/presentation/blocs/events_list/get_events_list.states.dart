import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/features/events/domain/entities/track.entity.dart';

sealed class GetEventsListStates {}

final class GetEventsListStatesData extends GetEventsListStates {
  final List<Track> data;

  GetEventsListStatesData(this.data);
}

final class GetEventsListStatesLoading extends GetEventsListStates {}

final class GetEventsListStatesError extends GetEventsListStates {
  final AppError error;

  GetEventsListStatesError(this.error);
}
