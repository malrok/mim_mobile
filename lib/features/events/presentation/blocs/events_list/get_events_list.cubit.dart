import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mim_mobile/config/di/dependency_injection.dart';
import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/features/events/domain/entities/track.entity.dart';
import 'package:mim_mobile/features/events/domain/use_cases/get_events_list.use_case.dart';
import 'package:mim_mobile/features/events/presentation/blocs/events_list/get_events_list.states.dart';

class GetEventsListCubit extends Cubit<GetEventsListStates> {
  late GetEventsListUseCase _getEventsListUseCase;

  final Completer<void> _isReady = Completer();

  StreamSubscription? _subscription;

  GetEventsListCubit() : super(GetEventsListStatesLoading()) {
    Future.wait([
      getIt.getAsync<GetEventsListUseCase>().then((useCase) => _getEventsListUseCase = useCase),
    ]).then((_) => _isReady.complete());
  }

  @override
  Future<void> close() {
    _subscription?.cancel();
    _subscription = null;
    return super.close();
  }

  void load() async {
    await _isReady.future;
    if (isClosed) return;
    emit(GetEventsListStatesLoading());
    _subscription = _getEventsListUseCase.execute().listen((result) => result.fold(
          (AppError appError) => emit(GetEventsListStatesError(appError)),
          (List<Track> events) => emit(GetEventsListStatesData(events)),
        ));
  }
}
