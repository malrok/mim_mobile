import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mim_mobile/config/di/dependency_injection.dart';
import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/features/events/domain/use_cases/refresh_events.use_case.dart';
import 'package:mim_mobile/features/events/presentation/blocs/events_list/refresh_events_list.states.dart';

class RefreshEventsListCubit extends Cubit<RefreshEventsListStates> {
  late RefreshEventsUseCase _refreshEventsUseCase;

  final Completer<void> _isReady = Completer();

  StreamSubscription? _subscription;

  RefreshEventsListCubit() : super(RefreshEventsListStatesRefreshing()) {
    Future.wait([
      getIt.getAsync<RefreshEventsUseCase>().then((useCase) => _refreshEventsUseCase = useCase),
    ]).then((_) => _isReady.complete());
  }

  @override
  Future<void> close() {
    _subscription?.cancel();
    _subscription = null;
    return super.close();
  }

  void refresh() async {
    await _isReady.future;
    if (isClosed) return;
    emit(RefreshEventsListStatesRefreshing());
    (await _refreshEventsUseCase.execute()).fold(
      (AppError appError) => emit(RefreshEventsListStatesRefreshingError(appError)),
      (_) {},
    );
  }
}
