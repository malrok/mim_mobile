import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';

sealed class FavoriteStates {}

final class FavoriteStatesData extends FavoriteStates {
  final bool data;

  FavoriteStatesData(this.data);
}

final class FavoriteStatesLoading extends FavoriteStates {}

final class FavoriteStatesError extends FavoriteStates {
  final AppError error;

  FavoriteStatesError(this.error);
}
