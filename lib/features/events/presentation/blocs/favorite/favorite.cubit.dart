import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mim_mobile/config/di/dependency_injection.dart';
import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/core/domain/exceptions/no_events.exception.dart';
import 'package:mim_mobile/features/events/domain/entities/event.entity.dart';
import 'package:mim_mobile/features/events/domain/use_cases/get_event_by_id.use_case.dart';
import 'package:mim_mobile/features/events/domain/use_cases/update_event_favorite.use_case.dart';
import 'package:mim_mobile/features/events/presentation/blocs/favorite/favorite.states.dart';

class FavoriteCubit extends Cubit<FavoriteStates> {
  late GetEventByIdUseCase _getEventByIdUseCase;
  late UpdateEventFavoriteUseCase _updateEventFavoriteUseCase;

  final Completer<void> _isReady = Completer();

  FavoriteCubit() : super(FavoriteStatesLoading()) {
    Future.wait([
      getIt.getAsync<GetEventByIdUseCase>().then((useCase) => _getEventByIdUseCase = useCase),
      getIt.getAsync<UpdateEventFavoriteUseCase>().then((useCase) => _updateEventFavoriteUseCase = useCase),
    ]).then((_) => _isReady.complete());
  }

  void load(String id) async {
    await _isReady.future;
    if (isClosed) return;
    emit(FavoriteStatesLoading());
    (await _getEventByIdUseCase.execute(id)).fold(
      (AppError appError) => emit(FavoriteStatesError(appError)),
      (Event? event) {
        if (event == null) {
          if (isClosed) return;
          emit(FavoriteStatesError(AppError(NoEventsException())));
        } else {
          if (isClosed) return;
          emit(FavoriteStatesData(event.isFavorite));
        }
      },
    );
  }

  void updateFavorite(Event event) async {
    await _isReady.future;
    emit(FavoriteStatesLoading());
    (await _updateEventFavoriteUseCase.execute(event)).fold(
      (AppError appError) => emit(FavoriteStatesError(appError)),
      (Event? event) {
        if (event == null) {
          if (isClosed) return;
          emit(FavoriteStatesError(AppError(NoEventsException())));
        } else {
          if (isClosed) return;
          emit(FavoriteStatesData(event.isFavorite));
        }
      },
    );
  }
}
