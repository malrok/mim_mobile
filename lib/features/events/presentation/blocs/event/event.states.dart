import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/features/events/domain/entities/event.entity.dart';

sealed class EventStates {}

final class EventStatesData extends EventStates {
  final Event data;

  EventStatesData(this.data);
}

final class EventStatesLoading extends EventStates {}

final class EventStatesError extends EventStates {
  final AppError error;

  EventStatesError(this.error);
}
