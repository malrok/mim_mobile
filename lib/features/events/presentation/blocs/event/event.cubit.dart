import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mim_mobile/config/di/dependency_injection.dart';
import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/core/domain/exceptions/no_events.exception.dart';
import 'package:mim_mobile/features/events/domain/entities/event.entity.dart';
import 'package:mim_mobile/features/events/domain/use_cases/get_event_by_id.use_case.dart';
import 'package:mim_mobile/features/events/presentation/blocs/event/event.states.dart';

class EventCubit extends Cubit<EventStates> {
  late GetEventByIdUseCase _getEventByIdUseCase;

  final Completer<void> _isReady = Completer();

  EventCubit() : super(EventStatesLoading()) {
    getIt.getAsync<GetEventByIdUseCase>().then((useCase) {
      _getEventByIdUseCase = useCase;
      _isReady.complete();
    });
  }

  void load(String id) async {
    await _isReady.future;
    emit(EventStatesLoading());
    (await _getEventByIdUseCase.execute(id)).fold(
      (AppError appError) => emit(EventStatesError(appError)),
      (Event? event) {
        if (event == null) {
          emit(EventStatesError(AppError(NoEventsException())));
        } else {
          emit(EventStatesData(event));
        }
      },
    );
  }
}
