import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/features/events/domain/entities/planning.entity.dart';

sealed class FavoritesListStates {}

final class FavoritesListStatesData extends FavoritesListStates {
  final List<Planning> data;

  FavoritesListStatesData(this.data);
}

final class FavoritesListStatesLoading extends FavoritesListStates {}

final class FavoritesListStatesError extends FavoritesListStates {
  final AppError error;

  FavoritesListStatesError(this.error);
}
