import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mim_mobile/config/di/dependency_injection.dart';
import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/features/events/domain/entities/planning.entity.dart';
import 'package:mim_mobile/features/events/domain/use_cases/get_favorite_events_list.use_case.dart';
import 'package:mim_mobile/features/events/domain/use_cases/refresh_events.use_case.dart';
import 'package:mim_mobile/features/events/presentation/blocs/favorites_list/favorites_list.states.dart';

class FavoritesListCubit extends Cubit<FavoritesListStates> {
  late GetFavoriteEventsListUseCase _getFavoritesListUseCase;
  late RefreshEventsUseCase _refreshEventsUseCase;

  final Completer<void> _isReady = Completer();

  StreamSubscription? _subscription;

  FavoritesListCubit() : super(FavoritesListStatesLoading()) {
    Future.wait([
      getIt.getAsync<GetFavoriteEventsListUseCase>().then((useCase) => _getFavoritesListUseCase = useCase),
      getIt.getAsync<RefreshEventsUseCase>().then((useCase) => _refreshEventsUseCase = useCase),
    ]).then((_) => _isReady.complete());
  }

  @override
  Future<void> close() {
    _subscription?.cancel();
    _subscription = null;
    return super.close();
  }

  void load() async {
    await _isReady.future;
    if (isClosed) return;
    emit(FavoritesListStatesLoading());
    _subscription = _getFavoritesListUseCase.execute().listen((result) => result.fold(
          (AppError appError) => emit(FavoritesListStatesError(appError)),
          (List<Planning> plannings) => emit(FavoritesListStatesData(plannings)),
        ));
  }

  void refresh() async {
    await _isReady.future;
    if (isClosed) return;
    emit(FavoritesListStatesLoading());
    (await _refreshEventsUseCase.execute()).fold(
      (AppError appError) => emit(FavoritesListStatesError(appError)),
      (_) {},
    );
  }
}
