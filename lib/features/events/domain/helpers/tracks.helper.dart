import 'package:mim_mobile/features/events/domain/entities/event.entity.dart';
import 'package:mim_mobile/features/events/domain/entities/track.entity.dart';
import 'package:mim_mobile/features/events/domain/value_objects/talk_type.value_object.dart';

List<Track> getTracksFromEvents(List<Event> events) {
  List<String> trackNames = events.map((event) => event.track).toSet().toList();

  List<Track> tracks = trackNames
      .map((track) => Track(
          name: track,
          events: events
              .where(
                (event) => event.track == track && event.type != TalkType.pause,
              )
              .toList()))
      .toList();

  return tracks;
}
