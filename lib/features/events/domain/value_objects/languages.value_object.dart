enum Languages {
  french('French', '🇫🇷'),
  english('English', '🇺🇸'),
  not_set('', '🏴‍☠️');

  final String desc;
  final String icon;

  const Languages(this.desc, this.icon);

  factory Languages.fromString(String? value) => Languages.values.firstWhere((e) => e.desc == value);
}
