import 'package:mim_mobile/config/constants/icons.constants.dart';

enum TalkCategories {
  mobileForGood('Numérique Mobile Responsable / Mobile For Green / Mobile For Good', MimCategoryIcons.mobileForGood),
  iotData('IoT / Data', MimCategoryIcons.iot),
  pwaAndNoCode('PWA, No Code et Solutions Alternatives', MimCategoryIcons.pwa),
  nativeFrameworks('Framework Natifs', MimCategoryIcons.nativeFrameworks),
  crossPlatformFrameworks('Frameworks Multiplateformes', MimCategoryIcons.multipleFrameworks),
  product('Product', MimCategoryIcons.product),
  mobileCiCd('CI/CD Mobile', MimCategoryIcons.ci),
  mobileUxUi('UX / UI Mobile', MimCategoryIcons.design),
  rex('ReX : Histoires d\'Échecs et de Succès', MimCategoryIcons.successAndFailure),
  gaming('Gaming', MimCategoryIcons.gaming),
  arVrXr('AR / VR / XR', MimCategoryIcons.gaming),
  methodology('Méthodologie', MimCategoryIcons.methodology),
  securityAndPrivacy('Security & Privacy', MimCategoryIcons.security),
  other('Autre', MimCategoryIcons.others),
  na('', '');

  final String desc;
  final String icon;

  const TalkCategories(
    this.desc,
    this.icon,
  );

  factory TalkCategories.fromString(String? value) =>
      TalkCategories.values.firstWhere((e) => e.desc == value, orElse: () => TalkCategories.na);
}
