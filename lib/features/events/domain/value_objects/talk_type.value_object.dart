enum TalkType {
  talk('talk'),
  pause('break'),
  section('section');

  final String desc;

  const TalkType(this.desc);

  factory TalkType.fromString(String? value) =>
      TalkType.values.firstWhere((e) => e.desc == value, orElse: () => TalkType.pause);
}
