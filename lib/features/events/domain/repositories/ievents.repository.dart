import 'package:mim_mobile/features/events/data/datasources/local/event.dbo.dart';

abstract class IEventsRepository {
  Stream<List<EventDbo>> getEvents();

  Stream<List<EventDbo>> getFavoriteEvents();

  Future<EventDbo?> getEventById(String id);

  Future<EventDbo> updateEvent(EventDbo dbo);

  Future<void> refreshLocalWithRemote();
}
