import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/features/events/data/datasources/local/event.dbo.dart';
import 'package:mim_mobile/features/events/domain/entities/event.entity.dart';
import 'package:mim_mobile/features/events/domain/repositories/ievents.repository.dart';
import 'package:mim_mobile/features/events/domain/transformers/event.transformer.dart';

@injectable
class GetEventByIdUseCase {
  IEventsRepository repository;

  GetEventByIdUseCase(this.repository);

  Future<Either<AppError, Event?>> execute(String id) async {
    try {
      EventDbo? result = await repository.getEventById(id);

      return Right(result == null ? null : eventFromEventDbo(result));
    } catch (error) {
      return Left(AppError(error));
    }
  }
}
