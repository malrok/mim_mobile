import 'dart:async';

import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/features/events/data/datasources/local/event.dbo.dart';
import 'package:mim_mobile/features/events/domain/entities/track.entity.dart';
import 'package:mim_mobile/features/events/domain/helpers/tracks.helper.dart';
import 'package:mim_mobile/features/events/domain/repositories/ievents.repository.dart';
import 'package:mim_mobile/features/events/domain/transformers/event.transformer.dart';

@injectable
class GetEventsListUseCase {
  IEventsRepository repository;

  GetEventsListUseCase(this.repository);

  Stream<Either<AppError, List<Track>>> execute() {
    StreamController<Either<AppError, List<Track>>> controller = StreamController();

    runZonedGuarded(
      () => repository.getEvents().listen((events) => controller.add(Right<AppError, List<Track>>(
          getTracksFromEvents(events.map((EventDbo db) => eventFromEventDbo(db)).toList())))),
      (error, stack) => controller.add(Left(AppError(error))),
    );

    return controller.stream;
  }
}
