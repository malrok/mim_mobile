import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/features/events/data/datasources/local/event.dbo.dart';
import 'package:mim_mobile/features/events/domain/entities/event.entity.dart';
import 'package:mim_mobile/features/events/domain/repositories/ievents.repository.dart';
import 'package:mim_mobile/features/events/domain/transformers/event.transformer.dart';

@injectable
class UpdateEventFavoriteUseCase {
  IEventsRepository repository;

  UpdateEventFavoriteUseCase(this.repository);

  Future<Either<AppError, Event?>> execute(Event event) async {
    try {
      event.isFavorite = !event.isFavorite;
      EventDbo result = await repository.updateEvent(eventDboFromEvent(event));

      return Right(eventFromEventDbo(result)..speakers.addAll(event.speakers));
    } catch (error) {
      return Left(AppError(error));
    }
  }
}
