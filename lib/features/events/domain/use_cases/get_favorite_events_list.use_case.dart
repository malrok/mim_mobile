import 'dart:async';

import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/features/events/data/datasources/local/event.dbo.dart';
import 'package:mim_mobile/features/events/domain/entities/event.entity.dart';
import 'package:mim_mobile/features/events/domain/entities/planning.entity.dart';
import 'package:mim_mobile/features/events/domain/repositories/ievents.repository.dart';
import 'package:mim_mobile/features/events/domain/transformers/event.transformer.dart';

@injectable
class GetFavoriteEventsListUseCase {
  IEventsRepository repository;

  GetFavoriteEventsListUseCase(this.repository);

  Stream<Either<AppError, List<Planning>>> execute() {
    StreamController<Either<AppError, List<Planning>>> controller = StreamController();

    runZonedGuarded(
      () => repository.getFavoriteEvents().listen((dboList) {
        List<Planning> plannings = [];
        List<Event> events = [];
        late Planning currentPlanning;

        DateTime? lastStartDate;

        startAnew(EventDbo dbo) {
          lastStartDate = dbo.startDate;
          events = [];
          events.add(eventFromEventDbo(dbo));
          currentPlanning = Planning(dbo.startDate.toUtc(), dbo.endDate.toUtc(), events);
          plannings.add(currentPlanning);
        }

        for (final dbo in dboList) {
          if (lastStartDate == null) {
            startAnew(dbo);
          } else if (lastStartDate!.isAtSameMomentAs(dbo.startDate)) {
            events.add(eventFromEventDbo(dbo));
          } else {
            startAnew(dbo);
          }
        }

        controller.add(Right(plannings));
      }),
      (error, stack) => controller.add(Left(AppError(error))),
    );

    return controller.stream;
  }
}
