import 'package:mim_mobile/features/events/domain/entities/formatted_text.entity.dart';
import 'package:mim_mobile/features/events/domain/entities/speaker.entity.dart';
import 'package:mim_mobile/features/events/domain/value_objects/languages.value_object.dart';
import 'package:mim_mobile/features/events/domain/value_objects/talk_categories.value_object.dart';
import 'package:mim_mobile/features/events/domain/value_objects/talk_type.value_object.dart';

class Event {
  String id;
  String openFeedbackId;
  String label;
  FormattedText description;
  DateTime startDate;
  DateTime endDate;
  String track;
  List<Speaker> speakers;
  TalkCategories category;
  TalkType type;
  Languages language;
  bool isFavorite;

  Event({
    required this.id,
    required this.openFeedbackId,
    required this.label,
    required this.description,
    required this.startDate,
    required this.endDate,
    required this.track,
    required this.speakers,
    required this.category,
    required this.type,
    required this.language,
    required this.isFavorite,
  });

  Duration get duration => endDate.difference(startDate);

  bool isClickable() => description.content.isNotEmpty;

  @override
  String toString() => '$id / $label / $description ${speakers.toString()}';
}
