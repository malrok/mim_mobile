import 'package:mim_mobile/features/events/domain/entities/event.entity.dart';

class Track {
  String name;
  List<Event> events;

  Track({
    required this.name,
    required this.events,
  });
}
