import 'package:mim_mobile/features/events/domain/entities/event.entity.dart';
import 'package:mim_mobile/features/events/domain/entities/formatted_text.entity.dart';

class Speaker {
  int id;
  String name;
  FormattedText bio;
  String pictureUrl;
  String company;
  String twitterUrl;
  String githubUrl;

  List<Event>? events;

  Speaker({
    required this.id,
    required this.name,
    required this.bio,
    required this.pictureUrl,
    required this.company,
    required this.twitterUrl,
    required this.githubUrl,
    this.events,
  });

  @override
  String toString() => '$id / $name / $bio';
}
