import 'package:mim_mobile/features/events/domain/entities/event.entity.dart';

class Planning {
  DateTime start;
  DateTime end;
  List<Event> events;

  Planning(this.start, this.end, this.events);
}