import 'package:mim_mobile/features/events/domain/value_objects/formatted_text_type.value_object.dart';

class FormattedText {
  final String content;
  final FormattedTextType type;

  FormattedText(this.content, this.type);

  bool get isNotEmpty => content.isNotEmpty;
}
