import 'package:mim_mobile/features/events/data/datasources/local/event.dbo.dart';
import 'package:mim_mobile/features/events/domain/entities/event.entity.dart';
import 'package:mim_mobile/features/events/domain/transformers/formatted_text.transformer.dart';
import 'package:mim_mobile/features/events/domain/transformers/speaker.transformer.dart';
import 'package:mim_mobile/features/events/domain/value_objects/languages.value_object.dart';
import 'package:mim_mobile/features/events/domain/value_objects/talk_categories.value_object.dart';
import 'package:mim_mobile/features/events/domain/value_objects/talk_type.value_object.dart';

Event eventFromEventDbo(EventDbo db) {
  return Event(
    id: db.id,
    openFeedbackId: db.openFeedbackId,
    label: db.label,
    description: formattedTextFromDto(db.description.replaceAll('\\n', '\n')),
    startDate: db.startDate.toUtc(),
    endDate: db.endDate.toUtc(),
    track: db.room,
    speakers: db.speakers.map((s) => speakerFromSpeakerDbo(s)).toList(),
    category: TalkCategories.fromString(db.category),
    type: TalkType.fromString(db.type),
    language: Languages.fromString(db.language),
    isFavorite: db.isFavorite,
  );
}

EventDbo eventDboFromEvent(Event event) {
  return EventDbo(
    event.id,
    event.openFeedbackId,
    event.label,
    event.description.content,
    event.startDate,
    event.endDate,
    event.track,
    event.category.desc,
    event.type.desc,
    event.language.desc,
    event.isFavorite,
  );
}
