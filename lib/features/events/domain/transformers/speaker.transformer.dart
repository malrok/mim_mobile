import 'package:mim_mobile/features/events/data/datasources/local/speaker.dbo.dart';
import 'package:mim_mobile/features/events/data/datasources/remote/event.dto.dart';
import 'package:mim_mobile/features/events/domain/entities/speaker.entity.dart';
import 'package:mim_mobile/features/events/domain/transformers/event.transformer.dart';
import 'package:mim_mobile/features/events/domain/transformers/formatted_text.transformer.dart';

List<Speaker> speakersFromEventDto(EventDto dto) {
  final speakers = <Speaker>[];
  if (dto.speaker1FullName != null) {
    speakers.add(Speaker(
      id: -1,
      name: dto.speaker1FullName!,
      bio: formattedTextFromDto(dto.speaker1Bio),
      pictureUrl: dto.speaker1PictureURL ?? '',
      company: dto.speaker1Company ?? '',
      twitterUrl: dto.speaker1TwitterURL ?? '',
      githubUrl: dto.speaker1GitHubURL ?? '',
    ));
  }
  if (dto.speaker2FullName != null) {
    speakers.add(Speaker(
      id: -1,
      name: dto.speaker2FullName!,
      bio: formattedTextFromDto(dto.speaker2Bio),
      pictureUrl: dto.speaker2PictureURL ?? '',
      company: dto.speaker2Company ?? '',
      twitterUrl: dto.speaker2TwitterURL ?? '',
      githubUrl: dto.speaker2GitHubURL ?? '',
    ));
  }
  return speakers;
}

Speaker speakerFromSpeakerDbo(SpeakerDbo dbo) {
  return Speaker(
    id: dbo.id,
    name: dbo.name,
    bio: formattedTextFromDto(dbo.bio.replaceAll('\\n', '\n')),
    pictureUrl: dbo.pictureUrl,
    company: dbo.company,
    twitterUrl: dbo.twitterUrl,
    githubUrl: dbo.githubUrl,
    events: dbo.events.map((event) => eventFromEventDbo(event)).toList(),
  );
}
