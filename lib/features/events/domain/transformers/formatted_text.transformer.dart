import 'package:mim_mobile/features/events/domain/entities/formatted_text.entity.dart';
import 'package:mim_mobile/features/events/domain/value_objects/formatted_text_type.value_object.dart';

FormattedText formattedTextFromDto(String? dto) {
  final str = dto ?? '';
  return FormattedText(str, _getStringType(str));
}

FormattedTextType _getStringType(String str) {
  if (RegExp(r'(__|\*\*|#)').hasMatch(str)) {
    return FormattedTextType.markdown;
  } else if (str.contains('>')) {
    return FormattedTextType.html;
  }
  return FormattedTextType.plain;
}
