import 'package:mim_mobile/features/events/data/datasources/local/event.dbo.dart';
import 'package:mim_mobile/features/events/domain/entities/planning.entity.dart';
import 'package:mim_mobile/features/events/domain/transformers/event.transformer.dart';

Planning planningFromEventDbo(EventDbo dbo) => Planning(
      dbo.startDate,
      dbo.endDate,
      [eventFromEventDbo(dbo)],
    );
