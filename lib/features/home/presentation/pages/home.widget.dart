import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:mim_mobile/core/presentation/app_navigation_bar.widget.dart';
import 'package:mim_mobile/core/presentation/error_display.widget.dart';
import 'package:mim_mobile/core/presentation/loading_display.widget.dart';
import 'package:mim_mobile/core/presentation/mim_app_bar.widget.dart';
import 'package:mim_mobile/features/events/presentation/pages/events_list/events_list.page.dart';
import 'package:mim_mobile/features/home/presentation/pages/widgets/no_event.widget.dart';
import 'package:mim_mobile/features/info/presentation/pages/info/info.page.dart';
import 'package:mim_mobile/features/map/presentation/pages/map.page.dart';
import 'package:mim_mobile/features/params/presentation/blocs/check_event_date_cubit.dart';
import 'package:mim_mobile/features/speakers/presentation/pages/speakers_list/speakers_list.page.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  final List<Widget> _pages = [
    const EventsListPage(),
    const SpeakersListPage(),
    const MapPage(),
    const InfoPage(),
  ];

  final _checkEventDateCubit = CheckEventDateCubit();

  @override
  void initState() {
    FlutterNativeSplash.remove();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: BlocBuilder<CheckEventDateCubit, CheckEventDateState>(
        bloc: _checkEventDateCubit..check(),
        builder: (context, state) {
          return switch (state) {
            CheckEventDateLoading() => _HomeContainer(child: const LoadingDisplay()),
            CheckEventDateHasEvent() => _HomeContainer(
                hasNavigationBar: true,
                child: TabBarView(
                  physics: const NeverScrollableScrollPhysics(),
                  children: [..._pages],
                ),
              ),
            CheckEventDateNoEvent() => _HomeContainer(child: const NoEvent()),
            CheckEventDateError() => _HomeContainer(
                child: SizedBox.expand(
                  child: Center(
                    child: ErrorDisplay(
                      error: state.error,
                      onRetry: () => _checkEventDateCubit.check(),
                    ),
                  ),
                ),
              ),
          };
        },
      ),
    );
  }
}

class _HomeContainer extends StatelessWidget {
  final Widget child;
  final bool hasNavigationBar;

  const _HomeContainer({required this.child, this.hasNavigationBar = false});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const MimAppBar(),
      body: child,
      bottomNavigationBar: hasNavigationBar ? const AppNavigationBar() : null,
    );
  }
}
