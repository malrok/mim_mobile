import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:mim_mobile/config/constants/colors.constants.dart';
import 'package:mim_mobile/config/constants/images.constants.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';
import 'package:mim_mobile/config/constants/theme.constants.dart';
import 'package:mim_mobile/core/domain/helpers/url.helper.dart';
import 'package:mim_mobile/core/presentation/primary_button.widget.dart';

class NoEvent extends StatelessWidget {
  const NoEvent({super.key});

  @override
  Widget build(BuildContext context) {
    final l10n = AppLocalizations.of(context)!;
    return SizedBox.expand(
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MimImages.noEvent,
            const SizedBox(height: MimSizes.medium),
            Text(
              l10n.noEventFound,
              style: textTheme.headlineMedium!.copyWith(color: MimColors.primary),
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: MimSizes.medium),
            PrimaryButton(
              onPressed: () => openUrl('https://mobilis-in-mobile.io'),
              label: l10n.webSiteAction,
            ),
          ],
        ),
      ),
    );
  }
}
