import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mim_mobile/config/di/dependency_injection.dart';
import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/features/home/presentation/blocs/refresh_sponsors_list.states.dart';
import 'package:mim_mobile/features/sponsors/domain/use_cases/refresh_sponsors_list.use_case.dart';

class RefreshSponsorsListCubit extends Cubit<RefreshSponsorsListStates> {
  late RefreshSponsorsListUseCase _refreshSponsorsListUseCase;

  final Completer<void> _isReady = Completer();

  RefreshSponsorsListCubit() : super(RefreshSponsorsListStatesLoading()) {
    getIt.getAsync<RefreshSponsorsListUseCase>().then((useCase) {
      _refreshSponsorsListUseCase = useCase;
      _isReady.complete();
    });
  }

  void load() async {
    await _isReady.future;
    if (isClosed) return;
    emit(RefreshSponsorsListStatesLoading());
    final result = await _refreshSponsorsListUseCase.execute();
    result.fold(
      (AppError appError) => emit(RefreshSponsorsListStatesError(appError)),
      (_) => emit(RefreshSponsorsListStatesData()),
    );
  }
}
