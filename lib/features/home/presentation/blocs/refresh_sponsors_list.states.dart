import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';

sealed class RefreshSponsorsListStates {}

final class RefreshSponsorsListStatesData extends RefreshSponsorsListStates {}

final class RefreshSponsorsListStatesLoading extends RefreshSponsorsListStates {}

final class RefreshSponsorsListStatesError extends RefreshSponsorsListStates {
  final AppError error;

  RefreshSponsorsListStatesError(this.error);
}
