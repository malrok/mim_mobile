import 'package:injectable/injectable.dart';
import 'package:mim_mobile/features/map/data/datasources/local/preferences.dao.dart';
import 'package:mim_mobile/features/map/domain/repositories/ipreferences.repository.dart';

@Singleton(as: IPreferencesRepository)
class PreferencesRepository implements IPreferencesRepository {
  final PreferencesDao _dao;

  PreferencesRepository(this._dao);

  @override
  Stream<bool> getThreeDimensionsMapPreference() => _dao.isThreeDimensionsMap();

  @override
  void setThreeDimensionsMapPreference(bool value) => _dao.setThreeDimensionsMap(value);
}
