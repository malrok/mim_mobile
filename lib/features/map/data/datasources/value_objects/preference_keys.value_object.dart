enum PreferenceKey {
  threeDimensionsMap('threeDimensionsMap');

  final String key;

  const PreferenceKey(this.key);
}
