import 'package:injectable/injectable.dart';
import 'package:mim_mobile/features/map/data/datasources/value_objects/preference_keys.value_object.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

@singleton
class PreferencesDao {
  late StreamingSharedPreferences _prefs;

  @factoryMethod
  static Future<PreferencesDao> create() async {
    final PreferencesDao preferencesDao = PreferencesDao();
    await preferencesDao._init();
    return preferencesDao;
  }

  Future<void> _init() async {
    _prefs = await StreamingSharedPreferences.instance;
  }

  Stream<bool> isThreeDimensionsMap() => _prefs.getBool(PreferenceKey.threeDimensionsMap.key, defaultValue: false);

  void setThreeDimensionsMap(bool value) => _prefs.setBool(PreferenceKey.threeDimensionsMap.key, value);
}
