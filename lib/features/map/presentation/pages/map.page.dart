import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mim_mobile/config/constants/colors.constants.dart';
import 'package:mim_mobile/config/constants/icons.constants.dart';
import 'package:mim_mobile/config/constants/images.constants.dart';
import 'package:mim_mobile/config/constants/sizes.constants.dart';
import 'package:mim_mobile/core/presentation/secondary_icon_button.widget.dart';
import 'package:mim_mobile/features/map/presentation/blocs/get_map_settings/get_map_settings.cubit.dart';
import 'package:mim_mobile/features/map/presentation/blocs/get_map_settings/get_map_settings.states.dart';
import 'package:mim_mobile/features/map/presentation/blocs/set_map_settings/set_map_settings.cubit.dart';
import 'package:model_viewer_plus/model_viewer_plus.dart';

class MapPage extends StatefulWidget {
  const MapPage({super.key});

  @override
  State<MapPage> createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  final SetMapSettingsCubit _setMapSettingsCubit = SetMapSettingsCubit();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GetMapSettingsCubit, GetMapSettingsStates>(
      bloc: GetMapSettingsCubit()..load(),
      builder: (context, state) {
        final is3d = state is GetMapSettingsStatesData ? state.value : false;
        return Stack(children: [
          const _ThreeDimensionsMap(),
          if (!is3d) const _TwoDimensionsMap(),
          Positioned(
            right: MimSizes.regular,
            top: MimSizes.regular,
            child: BlocProvider(
              create: (context) => _setMapSettingsCubit,
              child: SecondaryIconButton(
                icon: is3d ? Icons.threed_rotation_outlined : null,
                svg: !is3d ? MimSvg.twoD : null,
                onPressed: () {
                  _setMapSettingsCubit.update(!is3d);
                },
              ),
            ),
          ),
        ]);
      },
    );
  }
}

class _TwoDimensionsMap extends StatefulWidget {
  const _TwoDimensionsMap();

  @override
  State<_TwoDimensionsMap> createState() => _TwoDimensionsMapState();
}

class _TwoDimensionsMapState extends State<_TwoDimensionsMap> {
  final _transformationController = TransformationController();

  late TapDownDetails _doubleTapDetails;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: MimColors.mapBackground,
      child: Padding(
        padding: EdgeInsets.all(MimSizes.xsmall),
        child: GestureDetector(
          onDoubleTapDown: _handleDoubleTapDown,
          onDoubleTap: _handleDoubleTap,
          child: InteractiveViewer(
            transformationController: _transformationController,
            child: Container(
              alignment: Alignment.center,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(MimImagePaths.map),
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _handleDoubleTapDown(TapDownDetails details) {
    _doubleTapDetails = details;
  }

  void _handleDoubleTap() {
    if (_transformationController.value != Matrix4.identity()) {
      _transformationController.value = Matrix4.identity();
    } else {
      final position = _doubleTapDetails.localPosition;
      _transformationController.value = Matrix4.identity()
        ..translate(-position.dx, -position.dy)
        ..scale(2.0);
    }
  }
}

class _ThreeDimensionsMap extends StatelessWidget {
  const _ThreeDimensionsMap();

  @override
  Widget build(BuildContext context) {
    return ModelViewer(
      backgroundColor: MimColors.mapBackground,
      src: 'assets/3d/mim.glb',
      orientation: '0deg 30deg -30deg',
    );
  }
}
