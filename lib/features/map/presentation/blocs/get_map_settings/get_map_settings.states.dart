import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';

sealed class GetMapSettingsStates {}

final class GetMapSettingsStatesData extends GetMapSettingsStates {
  final bool value;

  GetMapSettingsStatesData(this.value);
}

final class GetMapSettingsStatesLoading extends GetMapSettingsStates {}

final class GetMapSettingsStatesError extends GetMapSettingsStates {
  final AppError error;

  GetMapSettingsStatesError(this.error);
}
