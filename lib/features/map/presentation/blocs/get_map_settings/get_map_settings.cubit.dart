import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mim_mobile/config/di/dependency_injection.dart';
import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/features/map/domain/use_cases/get_three_dimensions_map_setting.use_case.dart';
import 'package:mim_mobile/features/map/presentation/blocs/get_map_settings/get_map_settings.states.dart';

class GetMapSettingsCubit extends Cubit<GetMapSettingsStates> {
  late GetThreeDimensionsMapSettingUseCase _getThreeDimensionsMapSettingUseCase;

  final Completer<void> _isReady = Completer();

  StreamSubscription? _subscription;

  GetMapSettingsCubit() : super(GetMapSettingsStatesLoading()) {
    getIt.getAsync<GetThreeDimensionsMapSettingUseCase>().then((useCase) {
      _getThreeDimensionsMapSettingUseCase = useCase;
      _isReady.complete();
    });
  }

  @override
  Future<void> close() {
    _subscription?.cancel();
    _subscription = null;
    return super.close();
  }

  void load() async {
    await _isReady.future;
    if (isClosed) return;
    emit(GetMapSettingsStatesLoading());
    _subscription = _getThreeDimensionsMapSettingUseCase.execute().listen((result) => result.fold(
          (AppError appError) => emit(GetMapSettingsStatesError(appError)),
          (value) => emit(GetMapSettingsStatesData(value)),
        ));
  }
}
