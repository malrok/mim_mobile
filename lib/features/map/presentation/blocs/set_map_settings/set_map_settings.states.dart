import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';

sealed class SetMapSettingsStates {}

final class SetMapSettingsStatesData extends SetMapSettingsStates {}

final class SetMapSettingsStatesLoading extends SetMapSettingsStates {}

final class SetMapSettingsStatesError extends SetMapSettingsStates {
  final AppError error;

  SetMapSettingsStatesError(this.error);
}
