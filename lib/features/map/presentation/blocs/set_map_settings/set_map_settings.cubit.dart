import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mim_mobile/config/di/dependency_injection.dart';
import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/features/map/domain/use_cases/set_three_dimensions_map_setting.use_case.dart';
import 'package:mim_mobile/features/map/presentation/blocs/set_map_settings/set_map_settings.states.dart';

class SetMapSettingsCubit extends Cubit<SetMapSettingsStates> {
  late SetThreeDimensionsMapSettingUseCase _setThreeDimensionsMapSettingUseCase;

  final Completer<void> _isReady = Completer();

  SetMapSettingsCubit() : super(SetMapSettingsStatesLoading()) {
    getIt.getAsync<SetThreeDimensionsMapSettingUseCase>().then((useCase) {
      _setThreeDimensionsMapSettingUseCase = useCase;
      _isReady.complete();
    });
  }

  void update(bool value) async {
    await _isReady.future;
    emit(SetMapSettingsStatesLoading());
    (await _setThreeDimensionsMapSettingUseCase.execute(value)).fold(
      (AppError appError) => emit(SetMapSettingsStatesError(appError)),
      (_) => emit(SetMapSettingsStatesData()),
    );
  }
}
