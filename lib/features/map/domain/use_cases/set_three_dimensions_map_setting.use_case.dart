import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/features/map/domain/repositories/ipreferences.repository.dart';

@injectable
class SetThreeDimensionsMapSettingUseCase {
  IPreferencesRepository repository;

  SetThreeDimensionsMapSettingUseCase(this.repository);

  Future<Either<AppError, void>> execute(bool value) async {
    try {
      repository.setThreeDimensionsMapPreference(value);

      return const Right(null);
    } catch (error) {
      return Left(AppError(error));
    }
  }
}
