import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/features/map/domain/repositories/ipreferences.repository.dart';

@injectable
class GetThreeDimensionsMapSettingUseCase {
  IPreferencesRepository repository;

  GetThreeDimensionsMapSettingUseCase(this.repository);

  Stream<Either<AppError, bool>> execute() {
    return repository
        .getThreeDimensionsMapPreference()
        .handleError((error) => Left(AppError(error)))
        .map((value) => Right(value));
  }
}
