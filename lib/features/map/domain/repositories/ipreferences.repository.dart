abstract class IPreferencesRepository {
  Stream<bool> getThreeDimensionsMapPreference();

  void setThreeDimensionsMapPreference(bool value);
}