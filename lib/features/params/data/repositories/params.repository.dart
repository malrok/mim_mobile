import 'dart:async';

import 'package:injectable/injectable.dart';
import 'package:mim_mobile/features/params/data/datasources/local/base_params_dao.dart';
import 'package:mim_mobile/features/params/data/datasources/local/params.dbo.dart';
import 'package:mim_mobile/features/params/data/datasources/remote/params.remote_datasource.dart';
import 'package:mim_mobile/features/params/data/datasources/transformers/params.transformer.dart';
import 'package:mim_mobile/features/params/domain/repository/iparams.repository.dart';

@Singleton(as: IParamsRepository)
class ParamsRepository implements IParamsRepository {
  final BaseParamsDao _dao;
  final ParamsRemoteDatasource _datasource;

  ParamsRepository(this._datasource, this._dao);

  @override
  Stream<ParamsDbo> getParams() {
    StreamController<ParamsDbo> controller = StreamController();

    _dao.getParams().listen((params) {
      unawaited(refreshLocalWithRemote());
      if (params.contactEmail != 'default') {
        controller.add(params);
      }
    });

    return controller.stream;
  }

  @override
  Future<void> refreshLocalWithRemote() async {
    final paramsDto = await _datasource.query();

    final paramsDbo = paramsDboFromParamsDto(paramsDto);

    await _dao.updateParams(paramsDbo);
  }
}
