import 'package:mim_mobile/features/params/data/datasources/local/params.dbo.dart';
import 'package:mim_mobile/features/params/data/datasources/remote/params.dto.dart';

ParamsDbo paramsDboFromParamsDto(ParamsDto dto) => ParamsDbo(
      contactEmail: dto.contactEmail,
      locationAddress1: dto.locationAddress1,
      locationAddress2: dto.locationAddress2,
      locationLatitude: dto.locationLatitude,
      locationLongitude: dto.locationLongitude,
      locationName: dto.locationName,
      eventsFileName: dto.eventsFileName,
      sponsorsFileName: dto.sponsorsFileName,
      openFeedbackUrl: dto.openFeedbackUrl,
      ticketShopUrl: dto.ticketShopUrl,
      eventDate: DateTime.parse(dto.eventDate),
    );
