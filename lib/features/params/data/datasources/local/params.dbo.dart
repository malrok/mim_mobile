import 'package:json_annotation/json_annotation.dart';

part 'params.dbo.g.dart';

@JsonSerializable()
class ParamsDbo {
  String contactEmail;
  String locationAddress1;
  String locationAddress2;
  double locationLatitude;
  double locationLongitude;
  String locationName;
  @JsonKey(defaultValue: '')
  String eventsFileName;
  @JsonKey(defaultValue: '')
  String sponsorsFileName;
  String openFeedbackUrl;
  String ticketShopUrl;
  DateTime eventDate;

  ParamsDbo({
    required this.contactEmail,
    required this.locationAddress1,
    required this.locationAddress2,
    required this.locationLatitude,
    required this.locationLongitude,
    required this.locationName,
    required this.eventsFileName,
    required this.sponsorsFileName,
    required this.openFeedbackUrl,
    required this.ticketShopUrl,
    required this.eventDate,
  });

  factory ParamsDbo.fromJson(Map<String, dynamic> json) => _$ParamsDboFromJson(json);

  Map<String, dynamic> toJson() => _$ParamsDboToJson(this);
}
