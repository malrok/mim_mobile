import 'package:mim_mobile/features/params/data/datasources/local/params.dbo.dart';

abstract class BaseParamsDao {
  ParamsDbo getParamsSync();

  Stream<ParamsDbo> getParams();

  Future<bool> updateParams(ParamsDbo params);
}
