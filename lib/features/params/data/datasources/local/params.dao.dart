import 'dart:convert';

import 'package:injectable/injectable.dart';
import 'package:mim_mobile/features/params/data/datasources/local/base_params_dao.dart';
import 'package:mim_mobile/features/params/data/datasources/local/params.dbo.dart';
import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

const paramsKey = 'params';

const paramsDefaults = {
  'contactEmail': 'default',
  'locationAddress1': '',
  'locationAddress2': '',
  'locationLatitude': 0.0,
  'locationLongitude': 0.0,
  'locationName': '',
  'openFeedbackUrl': '',
  'ticketShopUrl': '',
  'eventDate': '1970-01-01T09:00:00.000Z',
};

@Singleton(as: BaseParamsDao)
@Environment('prod')
class ParamsDao implements BaseParamsDao {
  late StreamingSharedPreferences preferences;
  Preference<String>? paramsPref;

  @factoryMethod
  static Future<ParamsDao> instantiate() async {
    final preferences = await StreamingSharedPreferences.instance;

    final paramsDao = ParamsDao._();

    paramsDao.preferences = preferences;

    return paramsDao;
  }

  ParamsDao._();

  @override
  ParamsDbo getParamsSync() {
    paramsPref ??= preferences.getString(paramsKey, defaultValue: jsonEncode(paramsDefaults));
    return ParamsDbo.fromJson(jsonDecode(paramsPref!.getValue()));
  }

  @override
  Stream<ParamsDbo> getParams() {
    paramsPref ??= preferences.getString(paramsKey, defaultValue: jsonEncode(paramsDefaults));
    return paramsPref!.map((prefs) => ParamsDbo.fromJson(jsonDecode(prefs)));
  }

  @override
  Future<bool> updateParams(ParamsDbo params) async {
    paramsPref ??= preferences.getString(paramsKey, defaultValue: jsonEncode(paramsDefaults));
    return paramsPref!.setValue(jsonEncode(params.toJson()));
  }
}
