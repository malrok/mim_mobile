import 'package:injectable/injectable.dart';
import 'package:mim_mobile/features/params/data/datasources/local/base_params_dao.dart';
import 'package:mocktail/mocktail.dart';

@Singleton(as: BaseParamsDao)
@Environment('dev')
class MockParamsDao extends Mock implements BaseParamsDao {
  @factoryMethod
  static Future<MockParamsDao> instantiate() async => MockParamsDao._();

  MockParamsDao._();
}
