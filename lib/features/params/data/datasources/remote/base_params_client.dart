import 'package:mim_mobile/core/data/datasources/remote/base_client.dart';
import 'package:mim_mobile/features/params/data/datasources/remote/params.dto.dart';

abstract class BaseParamsClient extends BaseClient<Map<String, dynamic>> {
  BaseParamsClient(super.dio);

  Future<ParamsDto> getParams();
}
