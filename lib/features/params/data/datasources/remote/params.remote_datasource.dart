import 'package:injectable/injectable.dart';
import 'package:mim_mobile/core/data/datasources/remote/base_remote.datasource.dart';
import 'package:mim_mobile/features/params/data/datasources/remote/base_params_client.dart';
import 'package:mim_mobile/features/params/data/datasources/remote/params.dto.dart';

@injectable
class ParamsRemoteDatasource extends BaseRemoteDatasource<ParamsDto> {
  final BaseParamsClient _client;

  ParamsRemoteDatasource(this._client);

  Future<ParamsDto> query() => super.execute(_getRemoteParams());

  Future<ParamsDto> _getRemoteParams() => _client.getParams();
}
