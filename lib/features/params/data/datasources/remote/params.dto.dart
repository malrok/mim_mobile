import 'package:json_annotation/json_annotation.dart';

part 'params.dto.g.dart';

@JsonSerializable()
class ParamsDto {
  String contactEmail;
  String locationAddress1;
  String locationAddress2;
  double locationLatitude;
  double locationLongitude;
  String locationName;
  String eventsFileName;
  String sponsorsFileName;
  String openFeedbackUrl;
  String ticketShopUrl;
  String eventDate;

  ParamsDto({
    required this.contactEmail,
    required this.locationAddress1,
    required this.locationAddress2,
    required this.locationLatitude,
    required this.locationLongitude,
    required this.locationName,
    required this.eventsFileName,
    required this.sponsorsFileName,
    required this.openFeedbackUrl,
    required this.ticketShopUrl,
    required this.eventDate,
  });

  factory ParamsDto.fromJson(Map<String, dynamic> json) => _$ParamsDtoFromJson(json);

  Map<String, dynamic> toJson() => _$ParamsDtoToJson(this);
}
