import 'package:injectable/injectable.dart';
import 'package:mim_mobile/core/data/datasources/remote/http.client.dart';
import 'package:mim_mobile/features/params/data/datasources/remote/base_params_client.dart';
import 'package:mim_mobile/features/params/data/datasources/remote/params.dto.dart';

@Singleton(as: BaseParamsClient)
@Environment('prod')
class ParamsClient extends BaseParamsClient {
  @factoryMethod
  static Future<ParamsClient> instantiate() async => ParamsClient._(httpClient);

  ParamsClient._(super.dio);

  @override
  Future<ParamsDto> getParams() async => ParamsDto.fromJson(await super.execute('/app_params.json'));
}
