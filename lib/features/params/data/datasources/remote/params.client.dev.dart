import 'package:injectable/injectable.dart';
import 'package:mim_mobile/features/params/data/datasources/remote/base_params_client.dart';
import 'package:mocktail/mocktail.dart';

@Singleton(as: BaseParamsClient)
@Environment('dev')
class MockParamsClient extends Mock implements BaseParamsClient {
  @factoryMethod
  static Future<MockParamsClient> instantiate() async => MockParamsClient();
}
