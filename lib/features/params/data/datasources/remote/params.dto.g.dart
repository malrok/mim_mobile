// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'params.dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ParamsDto _$ParamsDtoFromJson(Map<String, dynamic> json) => ParamsDto(
      contactEmail: json['contactEmail'] as String,
      locationAddress1: json['locationAddress1'] as String,
      locationAddress2: json['locationAddress2'] as String,
      locationLatitude: (json['locationLatitude'] as num).toDouble(),
      locationLongitude: (json['locationLongitude'] as num).toDouble(),
      locationName: json['locationName'] as String,
      eventsFileName: json['eventsFileName'] as String,
      sponsorsFileName: json['sponsorsFileName'] as String,
      openFeedbackUrl: json['openFeedbackUrl'] as String,
      ticketShopUrl: json['ticketShopUrl'] as String,
      eventDate: json['eventDate'] as String,
    );

Map<String, dynamic> _$ParamsDtoToJson(ParamsDto instance) => <String, dynamic>{
      'contactEmail': instance.contactEmail,
      'locationAddress1': instance.locationAddress1,
      'locationAddress2': instance.locationAddress2,
      'locationLatitude': instance.locationLatitude,
      'locationLongitude': instance.locationLongitude,
      'locationName': instance.locationName,
      'eventsFileName': instance.eventsFileName,
      'sponsorsFileName': instance.sponsorsFileName,
      'openFeedbackUrl': instance.openFeedbackUrl,
      'ticketShopUrl': instance.ticketShopUrl,
      'eventDate': instance.eventDate,
    };
