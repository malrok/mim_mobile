import 'package:mim_mobile/features/params/data/datasources/local/params.dbo.dart';
import 'package:mim_mobile/features/params/domain/entities/params.entity.dart';

Params paramsEntityFromParamsDbo(ParamsDbo dbo) => Params(
      contactEmail: dbo.contactEmail,
      locationAddress1: dbo.locationAddress1,
      locationAddress2: dbo.locationAddress2,
      locationLatitude: dbo.locationLatitude,
      locationLongitude: dbo.locationLongitude,
      locationName: dbo.locationName,
      openFeedbackUrl: dbo.openFeedbackUrl,
      ticketShopUrl: dbo.ticketShopUrl,
      eventDate: dbo.eventDate,
    );
