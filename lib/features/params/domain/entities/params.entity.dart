class Params {
  final String contactEmail;
  final String locationAddress1;
  final String locationAddress2;
  final double locationLatitude;
  final double locationLongitude;
  final String locationName;
  final String openFeedbackUrl;
  final String ticketShopUrl;
  final DateTime eventDate;

  Params({
    required this.contactEmail,
    required this.locationAddress1,
    required this.locationAddress2,
    required this.locationLatitude,
    required this.locationLongitude,
    required this.locationName,
    required this.openFeedbackUrl,
    required this.ticketShopUrl,
    required this.eventDate,
  });
}
