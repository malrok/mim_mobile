import 'dart:developer';

import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/features/params/domain/repository/iparams.repository.dart';

@injectable
class RefreshParamsUseCase {
  late IParamsRepository repository;

  RefreshParamsUseCase(this.repository);

  Future<Either<AppError, void>> execute() async {
    try {
      await repository.refreshLocalWithRemote();
      return const Right(null);
    } catch (error) {
      log(error.toString());
      return Left(AppError(error));
    }
  }
}
