import 'dart:async';

import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/features/params/domain/repository/iparams.repository.dart';

@injectable
class CheckEventDateUseCase {
  IParamsRepository repository;

  CheckEventDateUseCase(this.repository);

  Stream<Either<AppError, bool>> execute() {
    StreamController<Either<AppError, bool>> controller = StreamController();

    runZonedGuarded(
      () => repository.getParams().listen((params) {
        final isEventPlanned = DateTime.now().isBefore(params.eventDate.add(Duration(days: 14)));
        controller.add(Right(isEventPlanned));
      }),
      (error, stack) => controller.add(Left(AppError(error))),
    );

    return controller.stream;
  }
}
