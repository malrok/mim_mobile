import 'dart:async';

import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/features/params/domain/entities/params.entity.dart';
import 'package:mim_mobile/features/params/domain/repository/iparams.repository.dart';
import 'package:mim_mobile/features/params/domain/transformers/params.transformer.dart';

@injectable
class GetParamsUseCase {
  IParamsRepository repository;

  GetParamsUseCase(this.repository);

  Stream<Either<AppError, Params>> execute() {
    StreamController<Either<AppError, Params>> controller = StreamController();

    runZonedGuarded(
      () => repository.getParams().listen((params) => controller.add(Right(paramsEntityFromParamsDbo(params)))),
      (error, stack) => controller.add(Left(AppError(error))),
    );

    return controller.stream;
  }
}
