import 'package:mim_mobile/features/params/data/datasources/local/params.dbo.dart';

abstract class IParamsRepository {
  Stream<ParamsDbo> getParams();

  Future<void> refreshLocalWithRemote();
}
