import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mim_mobile/config/di/dependency_injection.dart';
import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/features/params/domain/entities/params.entity.dart';
import 'package:mim_mobile/features/params/domain/use_cases/get_params.use_case.dart';
import 'package:mim_mobile/features/params/presentation/blocs/get_params.states.dart';

class GetParamsCubit extends Cubit<GetParamsStates> {
  late GetParamsUseCase _getParamsUseCase;

  final Completer<void> _isReady = Completer();

  StreamSubscription? _subscription;

  GetParamsCubit() : super(GetParamsStatesLoading()) {
    getIt.getAsync<GetParamsUseCase>().then((useCase) {
      _getParamsUseCase = useCase;
      _isReady.complete();
    });
  }

  @override
  Future<void> close() {
    _subscription?.cancel();
    _subscription = null;
    return super.close();
  }

  void load() async {
    await _isReady.future;
    if (isClosed) return;
    emit(GetParamsStatesLoading());
    _subscription = _getParamsUseCase.execute().listen((result) {
      result.fold(
        (AppError appError) => emit(GetParamsStatesError(appError)),
        (Params params) => emit(GetParamsStatesData(params)),
      );
    });
  }
}
