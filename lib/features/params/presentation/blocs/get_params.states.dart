import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/features/params/domain/entities/params.entity.dart';

sealed class GetParamsStates {}

final class GetParamsStatesData extends GetParamsStates {
  final Params data;

  GetParamsStatesData(this.data);
}

final class GetParamsStatesLoading extends GetParamsStates {}

final class GetParamsStatesError extends GetParamsStates {
  final AppError error;

  GetParamsStatesError(this.error);
}
