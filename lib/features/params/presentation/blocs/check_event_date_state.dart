part of 'check_event_date_cubit.dart';

sealed class CheckEventDateState {}

final class CheckEventDateLoading extends CheckEventDateState {}

final class CheckEventDateHasEvent extends CheckEventDateState {}

final class CheckEventDateNoEvent extends CheckEventDateState {}

final class CheckEventDateError extends CheckEventDateState {
  final AppError error;

  CheckEventDateError(this.error);
}
