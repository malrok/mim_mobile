import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mim_mobile/config/di/dependency_injection.dart';
import 'package:mim_mobile/core/domain/entities/app_error.entity.dart';
import 'package:mim_mobile/features/params/domain/use_cases/check_event_date.use_case.dart';

part 'check_event_date_state.dart';

class CheckEventDateCubit extends Cubit<CheckEventDateState> {
  late CheckEventDateUseCase _useCase;

  final Completer<void> _isReady = Completer();

  StreamSubscription? _subscription;

  CheckEventDateCubit() : super(CheckEventDateLoading()) {
    getIt.getAsync<CheckEventDateUseCase>().then((useCase) {
      _useCase = useCase;
      _isReady.complete();
    });
  }

  @override
  Future<void> close() {
    _subscription?.cancel();
    _subscription = null;
    return super.close();
  }

  void check() async {
    await _isReady.future;
    if (isClosed) return;
    emit(CheckEventDateLoading());
    _subscription = _useCase.execute().listen((result) {
      result.fold(
        (AppError appError) => emit(CheckEventDateError(appError)),
        (bool hasEvent) {
          if (hasEvent) {
            emit(CheckEventDateHasEvent());
          } else {
            emit(CheckEventDateNoEvent());
          }
        },
      );
    });
  }
}
