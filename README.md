# Mobilis in Mobile

Official companion application for the Mobilis in Mobile event.  

## translations

Translation files are stored in `assets/l10n`  
After adding a translation, simply run the `flutter pub get` command to generate a new version of the AppLocalizations class.

## Event set up

The event setup is described in a file hosted on the server (`https://thomas-boutin.github.io/app_params.json` at the time when this was added to this file)  

The file can be modified by accessing the repo at this address: [https://github.com/Thomas-Boutin/thomas-boutin.github.io](https://github.com/Thomas-Boutin/thomas-boutin.github.io)

The file describes the basic options of the event:  
- contactEmail: the email used to send a request from the user
- locationAddress1: the first line of the address of the place hosting the event (ie "41 Boulevard de la Prairie au Duc")
- locationAddress2: the second line of the address of the place hosting the event (ie "44200 Nantes, FRANCE")
- locationLatitude: the latitude of the place hosting the event (used to display the place on a map)
- locationLongitude: the longitude of the place hosting the event (used to display the place on a map)
- locationName: the name of the place hosting the event
- eventsFileName: the name of the file on the remote server where slots are defined (ie "slots2024.json")
  sponsorsFileName: the name of the file on the remote server where sponsors are defined (ie "sponsors2024.json"
- openFeedbackUrl: the URL of the page created on openfeedback for the current edition of the event
- ticketShopUrl: the URL of the page where tickets for the event can be bought
- eventDate: the date of the event (ie "2024-06-18T09:00:00.000Z")